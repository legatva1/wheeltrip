package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.inject.Inject;
import com.google.maps.android.SphericalUtil;
import com.squareup.otto.Subscribe;

import java.util.Date;

import cz.cvut.fel.dcgi.wheeltrip.BuildConfig;
import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.BearingEvent;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.LocationFoundEvent;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.TtsEvent;
import cz.cvut.fel.dcgi.wheeltrip.controller.DirectionsController;
import cz.cvut.fel.dcgi.wheeltrip.controller.MapController;
import cz.cvut.fel.dcgi.wheeltrip.controller.NotificationController;
import cz.cvut.fel.dcgi.wheeltrip.model.notification.Notification;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Difficulty;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Direction;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Step;
import cz.cvut.fel.dcgi.wheeltrip.service.compass.CompassService;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.ControllerActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.JoystickController;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.MapsActivity;
import cz.cvut.fel.dcgi.wheeltrip.util.IconUtils;
import cz.cvut.fel.dcgi.wheeltrip.util.MockLocationUtils;
import cz.cvut.fel.dcgi.wheeltrip.util.SettingsUtils;
import roboguice.inject.InjectView;
import roboguice.util.Ln;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 30. 12. 2015
 */
public class NavigationFragment extends BaseMapFragment {

    @Inject private MapController mMapController;
    @Inject private CompassService mCompassService;
    @Inject private DirectionsController mDirectionsController;
    @Inject private NotificationController mNotificationController;

    @InjectView(R.id.nextLocation) private Button nextLocation;
    @InjectView(R.id.direction_arrow) private ImageView directionArrow;
    @InjectView(R.id.instruction_message) private TextView instructionMessage;
    @InjectView(R.id.direction_arrow_card) private CardView directionCard;
    @InjectView(R.id.instruction_message_card) private CardView messageCard;
    @InjectView(R.id.cannot_finish_step) private AppCompatButton cannotFinishStep;
    @InjectView(R.id.emergency) private AppCompatButton emergency;
    @InjectView(R.id.end_navigation) private AppCompatButton endNavigation;
    @InjectView(R.id.previous_instruction) private AppCompatButton prevInstruction;
    @InjectView(R.id.next_instruction) private AppCompatButton nextInstruction;
    @InjectView(R.id.step_number) private TextView stepNumber;
    @InjectView(R.id.menuContainer) private CardView menu;
    @InjectView(R.id.notification) private LinearLayout notificationContainer;
    @InjectView(R.id.notification_message) private TextView notificationMessage;

    private Step currentInstruction;
    private int currentInstructionIndex;
    private boolean isMenuOpened;
    private boolean isMenuAnimationInProgress;
    private boolean isDialogOpened;
    private float bearing;
    private AlertDialog dialog;
    private ListView dialogListView;
    private boolean notificationShown;
    private long backPressed;
    private boolean toStart;

    /**
     * Controller callback.
     */
    private JoystickController controller = new JoystickController() {

        @Override
        public void onConfirmEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }

            if (notificationShown) {
                notificationContainer.setVisibility(View.GONE);
                notificationShown = false;
                if (ttsService != null) {
                    ttsService.stopPlaying();
                }

            }
        }

        @Override
        public void onBackEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }

            if (isDialogOpened) {
                dialog.dismiss();
                return;
            }

            getActivity().onBackPressed();
        }

        @Override
        public void onUpEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }

            if (isDialogOpened) {
                int pos = dialogListView.getCheckedItemPosition();
                if (pos - 1 < 0) {
                    return;
                } else {
                    dialogListView.setItemChecked(pos - 1, true);
                }
                return;
            }
        }

        @Override
        public void onDownEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }

            if (isDialogOpened) {
                int pos = dialogListView.getCheckedItemPosition();

                if (pos + 1 >= dialogListView.getCount()) {
                    return;
                } else {
                    dialogListView.setItemChecked(pos + 1, true);
                }
                return;
            }

            menu.performClick();
        }

        @Override
        public void onLeftEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }

            if (isMenuOpened) {
                cannotFinishStep.performClick();
            } else {
                prevInstruction.performClick();
            }
        }

        @Override
        public void onRightEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }

            if (isDialogOpened) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                return;
            }

            if (isMenuOpened) {
                endNavigation.performClick();
                return;
            } else {
                nextInstruction.performClick();
            }

        }
    };

    /**
     * Creates menu animation.
     *
     * @return animator object
     */
    private ObjectAnimator createMenuAnimation() {
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, r.getDisplayMetrics());
        ObjectAnimator menuAnimation = ObjectAnimator.ofFloat(
                menu,
                View.TRANSLATION_X,
                isMenuOpened ? 0f : -menu.getWidth() + px,
                isMenuOpened ? -menu.getWidth() + px : 0f);

        menuAnimation.setDuration(1000);
        menuAnimation.setInterpolator(new DecelerateInterpolator(1.5f));
        menuAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                isMenuAnimationInProgress = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {

                isMenuOpened = !isMenuOpened;
                isMenuAnimationInProgress = false;

            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        return menuAnimation;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        super.onCreateView(inflater, container, savedState);
        return inflater.inflate(R.layout.fragment_navigation, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((ControllerActivity) getActivity()).setCallback(controller);

        mRequestLocationUpdates = true;
        checkTTS();


        if (BuildConfig.DEBUG && SettingsUtils.isMockingEnabled(getActivity())) {

            nextLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ttsService != null) {
                        ttsService.stopPlaying();
                    }

                    if (MockLocationUtils.hasNext()) {

                        Location loc = MockLocationUtils.getMockLocation();
                        try {
                            LocationServices.FusedLocationApi.setMockLocation(mGoogleApiClient, loc);
                            Ln.d("Setting mock loc: %s", loc);
                            handleNewLocation(new LatLng(loc.getLatitude(), loc.getLongitude()));
                        } catch (SecurityException ex) {
                            Ln.d("Location permissions not granted: %s", ex.getMessage());
                        }

                    } else {
                        MockLocationUtils.fillMockPositions(sessionData.getSelectedRoute().getLatLngs());
                        Toast.makeText(getActivity(), "mock restart", Toast.LENGTH_SHORT).show();
                        currentInstructionIndex = 0;
                    }
                }
            });

            MockLocationUtils.fillMockPositions(sessionData.getSelectedRoute().getLatLngs());
        } else {
            nextLocation.setVisibility(View.GONE);
        }

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isMenuAnimationInProgress) {
                    createMenuAnimation().start();
                }
            }
        });

        isMenuOpened = true;

        cannotFinishStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDialogOpened = true;
                dialog = (AlertDialog) createDialog();
                dialogListView = dialog.getListView();
                dialog.show();

            }
        });

        endNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        prevInstruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ttsService != null) {
                    ttsService.stopPlaying();
                }

                if (currentInstructionIndex < 1) {
                    return;
                }
                currentInstructionIndex--;
                Ln.d("current instruction index %s", currentInstructionIndex);
                currentInstruction = sessionData.getSelectedRoute().getSteps().get(currentInstructionIndex);
                if (ttsService != null) {
                    ttsService.speakMessageWithNotification(currentInstruction.getInstruction());
                }

                stepNumber.setText(getResources().getString(R.string.step_number, currentInstructionIndex + 1));
                AnimatorSet s = new AnimatorSet();
                s.play(createDirectionArrowAnimation()).with(createInstructionMessageAnimation());
                s.start();
            }
        });

        nextInstruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ttsService != null) {
                    ttsService.stopPlaying();
                }
                if (currentInstructionIndex >= sessionData.getSelectedRoute().getSteps().size() - 1) {
                    return;
                }
                currentInstructionIndex++;
                Ln.d("current instruction index %s", currentInstructionIndex);
                currentInstruction = sessionData.getSelectedRoute().getSteps().get(currentInstructionIndex);
                if (ttsService != null) {
                    ttsService.speakMessageWithNotification(currentInstruction.getInstruction());
                }
                stepNumber.setText(getResources().getString(R.string.step_number, currentInstructionIndex + 1));
                AnimatorSet s = new AnimatorSet();
                s.play(createDirectionArrowAnimation()).with(createInstructionMessageAnimation());
                s.start();
            }
        });

        emergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.your_location_sent), Toast.LENGTH_SHORT).show();

            }
        });

        if(!sessionData.getSelectedRoute().getSteps().isEmpty()) {
            currentInstruction = sessionData.getSelectedRoute().getSteps().get(0);
            AnimatorSet s = new AnimatorSet();
            s.play(createDirectionArrowAnimation()).with(createInstructionMessageAnimation());
            s.start();


            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (ttsService != null) {
                        ttsService.speakMessageWithNotification(currentInstruction.getInstruction());
                    }
                }
            }, 3000);


            if (!sessionData.getSelectedRoute().getSteps().get(0).getBounds().contains(sessionData.getLastLocation())) {
                if (ttsService != null && !toStart) {
                    showNotification(Notification.GO_TO_START);
                    toStart = true;
                }
            }

        }

        if(sessionData.getSelectedRoute().getSteps().isEmpty()){
            directionCard.setVisibility(View.GONE);
            messageCard.setVisibility(View.GONE);
            Toast.makeText(getActivity(),getActivity().getResources().getString(R.string.use_naviterier),Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Creates direction arrow animation
     *
     * @return animator object
     */
    private ObjectAnimator createDirectionArrowAnimation() {

        ObjectAnimator menuAnimation = ObjectAnimator.ofFloat(
                directionCard,
                View.TRANSLATION_X, -directionCard.getWidth(), 0f);

        menuAnimation.setDuration(500);
        menuAnimation.setInterpolator(new DecelerateInterpolator(1.5f));
        menuAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                directionCard.setVisibility(View.VISIBLE);
                directionArrow.setImageDrawable(getActivity().getResources().getDrawable(IconUtils.getDirectionArrow(Direction.getByType(currentInstruction.getType()))));
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        return menuAnimation;
    }

    /**
     * Creates instruction message animation
     *
     * @return animator object
     */
    private ObjectAnimator createInstructionMessageAnimation() {

        ObjectAnimator menuAnimation = ObjectAnimator.ofFloat(
                messageCard,
                View.TRANSLATION_Y, -messageCard.getHeight(), 0f);

        menuAnimation.setDuration(500);
        menuAnimation.setInterpolator(new DecelerateInterpolator(1.5f));
        menuAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

                messageCard.setVisibility(View.VISIBLE);
                instructionMessage.setText(currentInstruction.getInstruction());
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        return menuAnimation;
    }


    public Dialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setSingleChoiceItems(R.array.obstacles, 0, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("Odeslat a přeplánovat trasu \u25B6", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        Ln.d("current instruction index %s", currentInstructionIndex);
        int index = currentInstructionIndex + 1;
        builder.setTitle("Nelze dokončit krok " + index + "/" + sessionData.getSelectedRoute().getSteps().size());
        AlertDialog dialog = builder.create();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                isDialogOpened = false;
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                isDialogOpened = false;
            }
        });
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                getActivity().dispatchKeyEvent(event);
                return true;
            }
        });

        return dialog;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        super.onMapReady(googleMap);
        mMapController.drawRoute(sessionData.getSelectedRoute(), mMap);
        //Uncomment this section to show checkpoints on the route
        // mMapController.drawCheckpoints(sessionData.getSelectedRoute().getSteps(), mMap, getActivity());
    }

    @Subscribe
    public void onLocationFound(LocationFoundEvent event) {
        Ln.d("Received LocationFoundEvent");
        if (!SettingsUtils.isMockingEnabled(getActivity())) {
            handleNewLocation(event.getLocation());
        }
    }

    private void handleNewLocation(LatLng position) {
        mMapController.animateCameraToPosition(position, bearing, mMap);

        for (Step step : sessionData.getSelectedRoute().getSteps()) {
            if (step.getBounds().contains(position)) {
                Ln.d("bounds for step %s contains %s", step, position);
                if (!step.isUsed()) {
                    Ln.d("step %s is not used", step);
                    //mAppBus.post(new TtsEvent(step.getInstruction()));
                    //step.setUsed(true);
                    //currentInstruction = step;
                    //currentInstructionIndex = sessionData.getSelectedRoute().getSteps().indexOf(step);
                    //createDirectionArrowAnimation().start();
                    //AnimatorSet s = new AnimatorSet();
                    //s.play(createDirectionArrowAnimation()).with(createInstructionMessageAnimation());
                    //s.start();
                    //if (sessionData.getSelectedRoute().getSteps().indexOf(step) == 0) {
                    //    notificationContainer.setVisibility(View.GONE);
                    //}
                }
            }

        }

        if (SphericalUtil.computeDistanceBetween(position, sessionData.getSelectedRoute().getLatLngs().get(sessionData.getSelectedRoute().getLatLngs().size() - 1)) < 30) {
            if (ttsService != null) {
                showNotification(Notification.IN_FINISH);
            }
        }

        for (Difficulty difficulty : sessionData.getRouteData().getRoutes().get(sessionData.getSelectedRouteIndex()).getDifficulties()) {
            if (difficulty.getBounds().contains(position)) {
                if (!difficulty.isUsed()) {
                    mAppBus.post(new TtsEvent("POZOR! " + difficulty.getDescription()));
                    difficulty.setUsed(true);
                }
            }

        }
    }

    /**
     * Shows notification.
     *
     * @param notification
     */
    private void showNotification(Notification notification) {
        ttsService.stopPlaying();
        notificationMessage.setText(notification.getMessage());
        mNotificationController.playNotification(ttsService, notification);
        notificationContainer.setVisibility(View.VISIBLE);
        notificationShown = true;

    }


    @Override
    public void onStart() {
        super.onStart();
        mCompassService.startCompass(getActivity());
    }

    @Override
    public void onStop() {
        mCompassService.stopCompass(getActivity());
        super.onStop();
    }

    @Subscribe
    public void onBearingEvent(BearingEvent event) {
        //Ln.d("onBearingEvent(%s)",event.getBearing());
        bearing = (float) event.getBearing();
    }

    @Subscribe
    public void onTtsEvent(TtsEvent event) {
        if (ttsService != null) {
            ttsService.speakMessageWithNotification(event.getMessage());
        }
    }

    @Override
    public boolean onBackPressed() {

        if (notificationShown) {
            notificationContainer.setVisibility(View.GONE);
            notificationShown = false;
            backPressed = new Date().getTime();
            if (ttsService != null) {
                ttsService.stopPlaying();
            }

            return true;
        }

        if (Math.abs(backPressed - new Date().getTime()) < 1000) {
            Ln.d("%s %s", backPressed, new Date().getTime());
            backPressed = new Date().getTime();
            return true;
        }

        backPressed = new Date().getTime();

        return super.onBackPressed();
    }
}
