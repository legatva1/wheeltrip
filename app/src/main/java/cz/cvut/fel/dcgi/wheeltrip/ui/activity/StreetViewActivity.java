package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import android.os.Bundle;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

import cz.cvut.fel.dcgi.wheeltrip.R;
import roboguice.inject.ContentView;
import roboguice.util.Ln;

/**
 * StreetView mode.
 *
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 2. 1. 2016
 */

@ContentView(R.layout.activity_streetview)
public class StreetViewActivity extends ControllerActivity
        implements OnStreetViewPanoramaReadyCallback {

    public static final String STREETVIEW_POSITION = "streetview_position";
    private LatLng position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        position = getIntent().getParcelableExtra(STREETVIEW_POSITION);
        Ln.d("street view position: %s", position);

        StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager()
                        .findFragmentById(R.id.streetviewpanorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);
    }


    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        streetViewPanorama.setPosition(position);
    }
}
