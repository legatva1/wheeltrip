package cz.cvut.fel.dcgi.wheeltrip.db.naviterier;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Contract for NaviTerier addresses.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 20. 12. 2015
 */
public final class NaviterierAddressContract {

    public static final String CONTENT_AUTHORITY = "cz.cvut.fel.dcgi.wheeltrip";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_ADDRESS = "naviterier_address";

    public static final class AddressEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ADDRESS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ADDRESS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ADDRESS;

        public static final String TABLE_NAME = "NAVITERIER_ADDRESS";

        public static final String ADDRESS_ID = "POI_ID";
        public static final String LAT = "LAT";
        public static final String LNG = "LNG";
        public static final String STREET = "STREET";
        public static final String HOUSE_NUMBER = "HOUSE_NUMBER";
        public static final String LAND_REGISTRY_NUMBER = "LAND_REGISTRY_NUMBER";
        public static final String CITY = "CITY";
        public static final String SEARCH_TITLE = "SEARCH_TITLE";


        public static final String [] ADDRESS_COLUMNS = {_ID,ADDRESS_ID,LAT,LNG,STREET,HOUSE_NUMBER,LAND_REGISTRY_NUMBER,CITY,SEARCH_TITLE};

        public static Uri buildPoiUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }


    public static class NaviterierAddressTable {
        public static final int _ID = 0;
        public static final int POI_ID = 1;
        public static final int LAT = 2;
        public static final int LNG = 3;
        public static final int STREET = 4;
        public static final int HOUSE_NUMBER = 5;
        public static final int LAND_REGISTRY_NUMBER = 6;
        public static final int CITY = 7;
        public static final int SEARCH_TITLE = 8;

    }
}
