package cz.cvut.fel.dcgi.wheeltrip.service.tts;

import android.content.Context;
import android.media.AudioManager;
import android.speech.tts.TextToSpeech;

import java.util.HashMap;
import java.util.Locale;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.util.SettingsUtils;
import roboguice.util.Ln;

/**
 * TTS service used to play inastructions, notifications and descriptions.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 24. 1. 2016
 */
public class TextToSpeechService implements TextToSpeech.OnInitListener {

    private TextToSpeech tts;

    private boolean ready = false;

    private static final String EARCON_NAME = "[tethys]";

    private Context context;

    public TextToSpeechService(Context context) {
        this.context = context;
        tts = new TextToSpeech(context, this);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            tts.setLanguage(Locale.getDefault());
            ready = true;
        } else {
            ready = false;
        }
    }

    /**
     * Performs TTS.
     * @param text text to transform
     */
    private void speak(String text) {
        HashMap<String, String> params = new HashMap<>();
        params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "UniqueID");
        params.put(TextToSpeech.Engine.KEY_PARAM_STREAM,
                String.valueOf(AudioManager.STREAM_ALARM));
        int result = tts.speak(text, TextToSpeech.QUEUE_ADD, params);
        Ln.d("Play speach result: %s", result);
    }

    /**
     * Flushes stream so currently playing message is stopped.
     */
    public void stopPlaying() {
        if (SettingsUtils.isTtsEnabled(context)) {
            HashMap<String, String> params = new HashMap<>();
            params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "UniqueID");
            params.put(TextToSpeech.Engine.KEY_PARAM_STREAM,
                    String.valueOf(AudioManager.STREAM_ALARM));
            int result = tts.speak("", TextToSpeech.QUEUE_FLUSH, params);
        }
    }


    /**
     * Plays earcon sound.
     */
    private void playNotificationSound() {
        tts.addEarcon(EARCON_NAME, "cz.cvut.fel.dcgi.wheeltrip", R.raw.tethys);
        int result = tts.playEarcon(EARCON_NAME, TextToSpeech.QUEUE_ADD, null);
        Ln.d("Play notification result: %s", result);
    }

    /**
     * Plays silence.
     * @param duration duration of silance
     */
    private void pause(int duration) {
        int result = tts.playSilence(duration, TextToSpeech.QUEUE_ADD, null);
        Ln.d("Play silence result: %s", result);
    }

    /**
     * Plays complete message.
     * @param message message to play
     * @param pauseDuration pause between earcon and message
     * @param playNotification true if earon should be played false otherwise.
     */
    private void speakAll(String message, int pauseDuration, boolean playNotification) {
        Ln.d("Speak: %s %s", message, ready);
        if (ready) {
            if (playNotification) {
                playNotificationSound();
                pause(pauseDuration);
            }
            speak(message);
        }
    }

    /**
     * Playes only message.
     * @param message message to play
     */
    public void speakMessage(String message) {
        if (SettingsUtils.isTtsEnabled(context)) {
            speakAll(message, 0, false);
        }

    }

    /**
     * Plays message with notification.
     * @param message message to play
     */
    public void speakMessageWithNotification(String message) {
        if (SettingsUtils.isTtsEnabled(context)) {
            speakAll(message, 500, true);
        }
    }

    /**
     * Plays message with notification and custom pause between them.
     * @param message message to play
     * @param pauseDuration pause between notification and message
     */
    public void speakMessageWithNotificationCustomPause(String message, int pauseDuration) {
        if (SettingsUtils.isTtsEnabled(context)) {
            speakAll(message, pauseDuration, true);
        }
    }

    /**
     * Destroys tts.
     */
    public void destroy() {
        tts.shutdown();
    }
}
