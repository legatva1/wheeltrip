package cz.cvut.fel.dcgi.wheeltrip.ui.activity;


import com.google.inject.Inject;

import cz.cvut.fel.dcgi.wheeltrip.bus.AppBus;

/**
 * Base activity that registers event bus.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 29. 1. 2016
 */
public class BaseActivity extends StateLoggingActivity {

    @Inject protected AppBus mAppBus;

    @Override
    protected void onStart() {
        super.onStart();
        mAppBus.register(this);
    }

    @Override
    protected void onStop() {
        mAppBus.unregister(this);
        super.onStop();
    }
}
