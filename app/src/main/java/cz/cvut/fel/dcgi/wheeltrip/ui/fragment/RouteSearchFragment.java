package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.inject.Inject;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.controller.DirectionsController;
import cz.cvut.fel.dcgi.wheeltrip.controller.NaviterierController;
import cz.cvut.fel.dcgi.wheeltrip.model.Poi;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.ControllerActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.JoystickController;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.PlaceSearchActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.RouteSearchActivity;
import cz.cvut.fel.dcgi.wheeltrip.util.Constants;
import cz.cvut.fel.dcgi.wheeltrip.util.SettingsUtils;
import roboguice.inject.InjectView;

/**
 * Route search screen.
 *
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 30. 12. 2015
 */
public class RouteSearchFragment extends BaseFragment {

    @Inject private DirectionsController mDirectionsController;
    @Inject private NaviterierController mNaviterierController;

    @InjectView(R.id.from_button) private Button fromButton;
    @InjectView(R.id.to_button) private Button toButton;
    @InjectView(R.id.find_route_button) private AppCompatButton findRoute;

    private LatLng from;
    private LatLng to;

    private static final int SET_FROM_REQUEST = 1;
    private static final int SET_TO_REQUEST = 2;

    /**
     * Controller callback.
     */
    private JoystickController controller = new JoystickController() {

        @Override
        public void onConfirmEvent() {

        }

        @Override
        public void onBackEvent() {
            getActivity().onBackPressed();
        }

        @Override
        public void onUpEvent() {

        }

        @Override
        public void onDownEvent() {
            findRoute.performClick();
        }

        @Override
        public void onLeftEvent() {
            fromButton.performClick();
        }

        @Override
        public void onRightEvent() {
            toButton.performClick();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        super.onCreateView(inflater, container, savedState);
        return inflater.inflate(R.layout.fragment_route_search, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((ControllerActivity) getActivity()).setCallback(controller);

        fromButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PlaceSearchActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.TARGET, sessionData.getOrigin() != null ? sessionData.getOrigin() : sessionData.getLastLocation());
                intent.putExtras(bundle);
                startActivityForResult(intent, SET_FROM_REQUEST);
            }
        });

        toButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PlaceSearchActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.TARGET, sessionData.getDestination() != null ? sessionData.getDestination() : sessionData.getLastLocation());
                intent.putExtras(bundle);
                startActivityForResult(intent, SET_TO_REQUEST);
            }
        });


        findRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (from == null) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.enter_origin), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (to == null) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.enter_destination), Toast.LENGTH_SHORT).show();
                    return;
                }

                sessionData.setFromTitle(fromButton.getText().toString());
                sessionData.setToTitle(toButton.getText().toString());

                if (SettingsUtils.isNaviterierDataEnabled(getActivity())) {
                    mNaviterierController.getRouteData(fromButton.getText().toString(), toButton.getText().toString(), getActivity());
                } else {
                    mDirectionsController.getDirections(from, to, getActivity());
                }


            }
        });


        if (sessionData.getLastLocation() != null) {
            from = sessionData.getLastLocation();
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RouteSearchActivity.RESULT_OK) {

            if (data == null || data.getSerializableExtra(Constants.POI) == null) {
                return;
            }

            if (requestCode == SET_FROM_REQUEST) {
                Poi poi = (Poi) data.getSerializableExtra(Constants.POI);
                fromButton.setText(poi.getName());
                from = new LatLng(poi.getLatitude(), poi.getLongitude());
                sessionData.setOrigin(from);
            }

            if (requestCode == SET_TO_REQUEST) {
                Poi poi = (Poi) data.getSerializableExtra(Constants.POI);
                toButton.setText(poi.getName());
                to = new LatLng(poi.getLatitude(), poi.getLongitude());
                sessionData.setDestination(to);
            }

        }


    }

}
