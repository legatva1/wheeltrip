package cz.cvut.fel.dcgi.wheeltrip.service.activityrecognition;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

import roboguice.util.Ln;

/**
 * Service that detects moving activity.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 10. 4. 2016
 */
public class ActivityRecognizedService extends IntentService {

    private boolean stop;

    public ActivityRecognizedService() {
        super("ActivityRecognizedService");
    }

    public ActivityRecognizedService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            handleDetectedActivities( result.getProbableActivities() );
        }
    }

    private void handleDetectedActivities(List<DetectedActivity> probableActivities) {
        for( DetectedActivity activity : probableActivities ) {
            if(activity.getConfidence()>90){
                Ln.d("send detected activity");
                Intent intentUpdate = new Intent();
                intentUpdate.setAction("cz.cvut.fel.dcgi.wheeltrip.ACTIVITY_RECOGNIZED");
                intentUpdate.putExtra("detected", activity.getType());
                sendBroadcast(intentUpdate);
            }
            switch( activity.getType() ) {
                case DetectedActivity.IN_VEHICLE: {
                    Ln.d("In Vehicle: %s",activity.getConfidence());
                    break;
                }
                case DetectedActivity.ON_BICYCLE: {
                    Ln.d("On Bicycle: %s", activity.getConfidence() );
                    break;
                }
                case DetectedActivity.ON_FOOT: {
                    Ln.d("On Foot: %s", activity.getConfidence() );
                    break;
                }
                case DetectedActivity.RUNNING: {
                    Ln.d("Running: %s",activity.getConfidence() );
                    break;
                }
                case DetectedActivity.STILL: {
                    Ln.d("Still: %s",activity.getConfidence() );
                    break;
                }
                case DetectedActivity.TILTING: {
                    Ln.d("Tilting: %s",activity.getConfidence() );
                    break;
                }
                case DetectedActivity.WALKING: {
                    Ln.d("Walking: %s",activity.getConfidence() );
                    break;
                }
                case DetectedActivity.UNKNOWN: {
                    Ln.d( "Unknown: %s",activity.getConfidence());
                    break;
                }
            }
        }
    }
}