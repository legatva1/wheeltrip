
package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RouteShape {

    @SerializedName("Points")
    @Expose
    private List<Point> points = new ArrayList<Point>();

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "RouteShape{" +
                "points=" + points +
                '}';
    }

    public List<LatLng> getLatLngs(){
        List<LatLng>latLngs = new ArrayList<>();
        if(points!=null){
            for(Point p:points){
                latLngs.add(new LatLng(p.getLatitude(),p.getLongitude()));
            }
        }
        return latLngs;
    }
}
