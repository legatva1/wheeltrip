package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RouteRequest {

    @SerializedName("SourceAddressId")
    @Expose
    private String originAddressId;
    @SerializedName("SourcePoiId")
    @Expose
    private String originPoiId;
    @SerializedName("SourceTextInput")
    @Expose
    private String originTextInput;
    @SerializedName("TargetAddressId")
    @Expose
    private String destinationAddressId;
    @SerializedName("TargetPoiId")
    @Expose
    private String destinationPoiId;
    @SerializedName("TargetTextInput")
    @Expose
    private String destinationTextInput;

    public String getOriginAddressId() {
        return originAddressId;
    }

    public void setOriginAddressId(String originAddressId) {
        this.originAddressId = originAddressId;
    }

    public String getOriginPoiId() {
        return originPoiId;
    }

    public void setOriginPoiId(String originPoiId) {
        this.originPoiId = originPoiId;
    }

    public String getOriginTextInput() {
        return originTextInput;
    }

    public void setOriginTextInput(String originTextInput) {
        this.originTextInput = originTextInput;
    }

    public String getDestinationAddressId() {
        return destinationAddressId;
    }

    public void setDestinationAddressId(String destinationAddressId) {
        this.destinationAddressId = destinationAddressId;
    }

    public String getDestinationPoiId() {
        return destinationPoiId;
    }

    public void setDestinationPoiId(String destinationPoiId) {
        this.destinationPoiId = destinationPoiId;
    }

    public String getDestinationTextInput() {
        return destinationTextInput;
    }

    public void setDestinationTextInput(String destinationTextInput) {
        this.destinationTextInput = destinationTextInput;
    }

    @Override
    public String toString() {
        return "RouteRequest{" +
                "originAddressId='" + originAddressId + '\'' +
                ", originPoiId='" + originPoiId + '\'' +
                ", originTextInput='" + originTextInput + '\'' +
                ", destinationAddressId='" + destinationAddressId + '\'' +
                ", destinationPoiId='" + destinationPoiId + '\'' +
                ", destinationTextInput='" + destinationTextInput + '\'' +
                '}';
    }
}