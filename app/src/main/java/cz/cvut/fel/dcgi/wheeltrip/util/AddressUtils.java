package cz.cvut.fel.dcgi.wheeltrip.util;

import android.location.Address;

import org.apache.commons.lang3.StringUtils;

import cz.cvut.fel.dcgi.wheeltrip.model.Poi;

/**
 * Address utils.
 *
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 4. 3. 2016
 */
public class AddressUtils {

    /**
     * Returns maximum available address info. Index 0 is reserved for VozejkMapPoi title.
     *
     * @param address address obtained from geocoding process
     * @return address description
     */
    public static String getMaxAddress(Address address) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < address.getMaxAddressLineIndex(); i++) {
            if (i < address.getMaxAddressLineIndex() - 1) {
                sb.append(address.getAddressLine(i));
                sb.append(", ");
            } else {
                sb.append(address.getAddressLine(i));
            }

        }

        return sb.toString();
    }

    /**
     * Creates address from poi.
     *
     * @param poi poi.
     * @return string representation of address
     */
    public static String getAddressFromPoi(Poi poi) {
        if (StringUtils.isEmpty(poi.getStreet())) {
            return "";
        }
        StringBuilder sb = new StringBuilder();

        sb.append(poi.getStreet());
        if (!StringUtils.isEmpty(poi.getStreetNumber())) {
            sb.append(" ");
            sb.append(poi.getStreetNumber());
        }

        if (!StringUtils.isEmpty(poi.getCity())) {
            sb.append(", ");
            sb.append(poi.getCity());
        }

        return sb.toString();
    }


}
