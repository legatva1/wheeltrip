package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;


import android.app.Activity;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.inject.Inject;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Timer;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.SessionData;
import cz.cvut.fel.dcgi.wheeltrip.controller.NaviterierController;
import cz.cvut.fel.dcgi.wheeltrip.model.GeneralPoi;
import cz.cvut.fel.dcgi.wheeltrip.model.Poi;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.Address;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.ControllerActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.MapChooserActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.PlaceSearchActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.adapter.NaviterierAddressAdapter;
import cz.cvut.fel.dcgi.wheeltrip.ui.adapter.NaviterierPoiAdapter;
import cz.cvut.fel.dcgi.wheeltrip.ui.adapter.PlaceAutocompleteAdapter;
import cz.cvut.fel.dcgi.wheeltrip.ui.adapter.VozejkMapPoiAdapter;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.JoystickController;
import cz.cvut.fel.dcgi.wheeltrip.controller.MapController;
import cz.cvut.fel.dcgi.wheeltrip.util.Constants;
import cz.cvut.fel.dcgi.wheeltrip.util.ItemClickSupport;
import cz.cvut.fel.dcgi.wheeltrip.util.SettingsUtils;
import cz.cvut.fel.dcgi.wheeltrip.controller.VozejkMapController;
import roboguice.inject.InjectView;
import roboguice.util.Ln;

/**
 * Fragment used to search for places.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 22. 12. 2015
 */
public class PlaceSearchFragment extends SpeechRecognitionFragment implements LoaderManager.LoaderCallbacks<Cursor>, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @Inject private VozejkMapController vozejkMapController;
    @Inject private NaviterierController naviterierController;
    @Inject private MapController mapController;
    @Inject private SessionData sessionData;

    @InjectView(R.id.edittext_places) private EditText mAutocompleteView;
    @InjectView(R.id.from_map) private AppCompatButton fromMapButton;
    @InjectView(R.id.microphone) private Button mic;
    @InjectView(R.id.place_recycler_view) private RecyclerView mRecyclerView;

    private VozejkMapPoiAdapter mAdapter;
    private RecyclerView.Adapter adapter;
    private NaviterierAddressAdapter naviterierAddressAdapter;
    private PlaceAutocompleteAdapter mPlacesAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    boolean canClick;
    protected GoogleApiClient mGoogleApiClient;
    private LatLng target;

    /**
     * Controller callback.
     */
    private JoystickController controller = new JoystickController() {

        @Override
        public void onConfirmEvent() {

            if (SettingsUtils.isVozejkmapDataEnabled(getActivity())) {
                selectPlace(mAdapter.getSelected());
            } else if (SettingsUtils.isNaviterierDataEnabled(getActivity())) {
                selectPlace(naviterierAddressAdapter.getSelected());
            } else {
                selectPlace(mPlacesAdapter.getSelected());
            }

        }

        @Override
        public void onBackEvent() {
            getActivity().onBackPressed();
        }

        @Override
        public void onUpEvent() {


            if (SettingsUtils.isVozejkmapDataEnabled(getActivity())) {
                mAdapter.setSelected(-1, mRecyclerView);
            } else if (SettingsUtils.isNaviterierDataEnabled(getActivity())) {
                naviterierAddressAdapter.setSelected(-1, mRecyclerView);
            } else {
                mPlacesAdapter.setSelected(-1, mRecyclerView);
            }


        }

        @Override
        public void onDownEvent() {

            if (SettingsUtils.isVozejkmapDataEnabled(getActivity())) {
                mAdapter.setSelected(1, mRecyclerView);
            } else if (SettingsUtils.isNaviterierDataEnabled(getActivity())) {
                naviterierAddressAdapter.setSelected(1, mRecyclerView);
            } else {
                mPlacesAdapter.setSelected(1, mRecyclerView);
            }
        }

        @Override
        public void onLeftEvent() {
            fromMapButton.performClick();
        }

        @Override
        public void onRightEvent() {
            mic.performClick();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createGoogleClient();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        super.onCreateView(inflater, container, savedState);
        return inflater.inflate(R.layout.fragment_place_search, container, false);
    }


    protected void createGoogleClient() {
        if (mGoogleApiClient == null) {
            Ln.d("createGoogleClient");
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(Places.GEO_DATA_API)
                    .build();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            Ln.d("connect");
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mGoogleApiClient != null) {
            Ln.d("onConnected(bundle=%s)", bundle);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Ln.d("disconect google client");
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((ControllerActivity) getActivity()).setCallback(controller);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        if (getActivity().getIntent().getParcelableExtra(Constants.TARGET) != null) {
            target = getActivity().getIntent().getParcelableExtra(Constants.TARGET);
        }

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new VozejkMapPoiAdapter(getActivity(), sessionData.getLastLocation());
        mPlacesAdapter = new PlaceAutocompleteAdapter(
                getActivity(),
                mGoogleApiClient,
                mapController.convertCenterAndRadiusToBounds(sessionData.getLastLocation(), 5000),
                null,
                sessionData.getLastLocation());

        naviterierAddressAdapter = new NaviterierAddressAdapter(getActivity(), sessionData.getLastLocation());

        getLoaderManager().initLoader(0, null, this);

        mAutocompleteView.addTextChangedListener(new TextWatcher() {

            private Timer timer;
            private final long SEARCH_TRIGGER_DELAY_IN_MS = 800;


            private Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {

                    if (!StringUtils.isEmpty(msg.getData().getString(Constants.QUERY))) {
                        if (SettingsUtils.isVozejkmapDataEnabled(getActivity())) {
                            mAdapter.getFilter().filter(msg.getData().getString(Constants.QUERY));
                        } else if (SettingsUtils.isNaviterierDataEnabled(getActivity())) {
                            naviterierAddressAdapter.getFilter().filter(msg.getData().getString(Constants.QUERY));
                        } else {
                            mPlacesAdapter.getFilter().filter(msg.getData().getString(Constants.QUERY));
                        }

                    } else {
                        restartLoader();
                    }

                }
            };

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(final Editable s) {
                Message message = new Message();
                Bundle bundle = new Bundle();
                bundle.putCharSequence(Constants.QUERY, s.toString());
                message.setData(bundle);
                handler.sendMessageDelayed(message, SEARCH_TRIGGER_DELAY_IN_MS);
            }
        });


        if (SettingsUtils.isVozejkmapDataEnabled(getActivity())) {
            mRecyclerView.setAdapter(mAdapter);
        } else if (SettingsUtils.isNaviterierDataEnabled(getActivity())) {
            mRecyclerView.setAdapter(naviterierAddressAdapter);
        } else {
            mRecyclerView.setAdapter(mPlacesAdapter);
        }

        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).build());


        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                selectPlace(position);

            }
        });

        mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput(Constants.REQ_CODE_SPEECH_INPUT);
            }
        });

        fromMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapChooserActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.TARGET, target != null ? target : sessionData.getLastLocation());
                intent.putExtras(bundle);
                startActivityForResult(intent, Constants.MAP_REQUEST);
            }
        });
    }

    private void selectPlace(int position) {

        if (SettingsUtils.isVozejkmapDataEnabled(getActivity()) && canClick) {
            canClick = false;
            Poi poi = mAdapter.getItem(position);
            mAdapter.setSelectedPosition(position, mRecyclerView);

            Intent intent = new Intent();
            Ln.d("place data intent %s", intent);
            intent.putExtra(Constants.POI, poi);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        } else if (SettingsUtils.isNaviterierDataEnabled(getActivity())) {

            Address address = naviterierAddressAdapter.getItem(position);

            Poi poi = new GeneralPoi();
            poi.setName(address.getStreet() + " " + address.getLandRegistryNumber());
            poi.setLatitude(address.getPoint().getLatitude());
            poi.setLongitude(address.getPoint().getLongitude());
            poi.setStreet("");

            Intent intent = new Intent();
            Ln.d("place data intent %s", intent);
            intent.putExtra(Constants.POI, poi);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();

        } else {
            final AutocompletePrediction item = mPlacesAdapter.getItem(position);
            final String placeId = item.getPlaceId();

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
        }
    }

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            Poi poi = new GeneralPoi();
            poi.setName(place.getName().toString());
            poi.setLatitude(place.getLatLng().latitude);
            poi.setLongitude(place.getLatLng().longitude);
            poi.setStreet(place.getAddress().toString());
            places.release();
            Intent intent = new Intent();
            Ln.d("place data intent %s", intent);
            intent.putExtra(Constants.POI, poi);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();

        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constants.REQ_CODE_SPEECH_INPUT: {
                if (resultCode == PlaceSearchActivity.RESULT_OK) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    mAutocompleteView.setText(result.get(0));

                }
                break;
            }
            case Constants.MAP_REQUEST:
                if (data != null && data.getSerializableExtra(Constants.POI) != null) {
                    Poi poi = (Poi) data.getSerializableExtra(Constants.POI);
                    Intent intent = new Intent();
                    intent.putExtra(Constants.POI, poi);
                    getActivity().setResult(Activity.RESULT_OK, intent);
                    getActivity().finish();
                }
                break;
            default:
                break;

        }
    }

    /**
     * Restarts cursor loader.
     */
    private void restartLoader() {
        canClick = false;
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Ln.d("loader created");
        return vozejkMapController.onCreateLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (mAdapter != null) {
            mAdapter.updateList(vozejkMapController.getPoisFromCursor(cursor, sessionData.getLastLocation()));
            canClick = true;
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


}
