package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import cz.cvut.fel.dcgi.wheeltrip.R;

/**
 * WheelTrip preferences screen.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 30. 1. 2016
 */
public class SettingsFragment extends PreferenceFragment{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

    }
}
