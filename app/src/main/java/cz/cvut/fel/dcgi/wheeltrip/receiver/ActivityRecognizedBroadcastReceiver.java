package cz.cvut.fel.dcgi.wheeltrip.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;

import roboguice.util.Ln;


/**
 * Receives detected activity. Currently not used.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 10. 4. 2016
 */
public class ActivityRecognizedBroadcastReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("cz.cvut.fel.dcgi.wheeltrip.ACTIVITY_RECOGNIZED")) {
            Ln.d("received detected activity");
            int detectedActivity = intent.getIntExtra("detected", 0);
            switch (detectedActivity) {
                case DetectedActivity.IN_VEHICLE: {
                    break;
                }
                case DetectedActivity.ON_BICYCLE: {
                    break;
                }
                case DetectedActivity.ON_FOOT: {
                    break;
                }
                case DetectedActivity.RUNNING: {
                    break;
                }
                case DetectedActivity.STILL: {
                    break;
                }
                case DetectedActivity.TILTING: {
                    break;
                }
                case DetectedActivity.WALKING: {
                    break;
                }
                case DetectedActivity.UNKNOWN: {
                    break;
                }
            }
        }
    }
}
