package cz.cvut.fel.dcgi.wheeltrip.bus.event;

import com.google.android.gms.maps.model.LatLng;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 23. 12. 2015
 */
public class PlaceSelectedEvent {

    private LatLng location;

    public PlaceSelectedEvent(LatLng location) {
        this.location = location;
    }

    public LatLng getLocation() {
        return location;
    }
}
