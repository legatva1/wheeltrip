package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import android.view.KeyEvent;
import android.view.MotionEvent;

/**
 * Activity that dispatches key events.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 12. 1. 2016
 */
public abstract class ControllerActivity extends BaseActivity {


    protected JoystickController mCallback;

    public void setCallback(JoystickController callback) {
        mCallback = callback;
    }


    @Override
    public boolean dispatchGenericMotionEvent(MotionEvent event) {
        return super.dispatchGenericMotionEvent(event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {

        if (mCallback == null) {
            return super.dispatchKeyEvent(event);
        }

        if (event.getAction() == KeyEvent.ACTION_DOWN) {

            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_ENTER:
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    mCallback.onConfirmEvent();
                    break;
                case KeyEvent.KEYCODE_BACK:
                    mCallback.onBackEvent();
                    break;
                case KeyEvent.KEYCODE_DPAD_UP:
                    mCallback.onUpEvent();
                    break;
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    mCallback.onDownEvent();
                    break;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    mCallback.onLeftEvent();
                    break;
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    mCallback.onRightEvent();
                    break;
            }
        }

        return super.dispatchKeyEvent(event);
    }
}


