package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.inject.Inject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.controller.MapController;
import cz.cvut.fel.dcgi.wheeltrip.controller.NaviterierController;
import cz.cvut.fel.dcgi.wheeltrip.model.GeneralPoi;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.ControllerActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.JoystickController;
import cz.cvut.fel.dcgi.wheeltrip.util.AddressUtils;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
import roboguice.inject.InjectView;
import roboguice.util.Ln;

/**
 * Fragment that is used to choose place from the map.
 *
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */
public class MapChooserFragment extends BaseMapFragment implements GoogleMap.OnMapLongClickListener {

    @Inject private MapController mMapController;
    @Inject private NaviterierController mNaviterierController;

    @InjectView(R.id.move_buttons) private RelativeLayout moveButtons;
    @InjectView(R.id.up) private ImageButton buttonUp;
    @InjectView(R.id.down) private ImageButton buttonDown;
    @InjectView(R.id.left) private ImageButton buttonLeft;
    @InjectView(R.id.right) private ImageButton buttonRight;

    private static final int SCROLL_DISTANCE = 100;
    private static final int SCROLL_LONG_DISTANCE = 500;
    private Marker placeMarker;
    private LatLng firstTarget;

    /**
     * Controller callback.
     */
    private JoystickController controller = new JoystickController() {

        @Override
        public void onConfirmEvent() {
            new GeocodingAsyncTask().execute(mMap.getCameraPosition().target);
            addMarker(mMap.getCameraPosition().target);
        }

        @Override
        public void onBackEvent() {
            getActivity().onBackPressed();
        }

        @Override
        public void onUpEvent() {
            buttonUp.performClick();
        }

        @Override
        public void onDownEvent() {
            buttonDown.performClick();
        }

        @Override
        public void onLeftEvent() {
            buttonLeft.performClick();
        }


        @Override
        public void onRightEvent() {
            buttonRight.performClick();
        }

    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        super.onCreateView(inflater, container, savedState);
        return inflater.inflate(R.layout.fragment_map_chooser, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((ControllerActivity) getActivity()).setCallback(controller);

        firstTarget = getActivity().getIntent().getParcelableExtra("target");
        buttonUp.setOnClickListener(new OnDirectionButtonClickListener(0));
        buttonDown.setOnClickListener(new OnDirectionButtonClickListener(1));
        buttonLeft.setOnClickListener(new OnDirectionButtonClickListener(2));
        buttonRight.setOnClickListener(new OnDirectionButtonClickListener(3));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        super.onMapReady(googleMap);
        mMap.setOnMapLongClickListener(this);
        mMapController.animateCameraToPosition(firstTarget, googleMap);
    }

    /**
     * Adds marker of selected location on the map.
     *
     * @param position place location
     */
    private void addMarker(LatLng position) {
        if (placeMarker == null) {
            placeMarker = mMapController.drawMarker(position, mMap);
        } else {
            placeMarker.remove();
            placeMarker = mMapController.drawMarker(position, mMap);
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        new GeocodingAsyncTask().execute(mMap.getCameraPosition().target);
        addMarker(mMap.getCameraPosition().target);
    }

    /**
     * Listener that listens for button clicks to move the map.
     */
    private class OnDirectionButtonClickListener implements View.OnClickListener {

        private int directionCode;

        public OnDirectionButtonClickListener(int directionCode) {
            this.directionCode = directionCode;
        }

        @Override
        public void onClick(View v) {

            mMap.stopAnimation();

            switch (directionCode) {
                case 0:
                    mMap.animateCamera(CameraUpdateFactory.scrollBy(0, -SCROLL_DISTANCE));
                    break;
                case 1:
                    mMap.animateCamera(CameraUpdateFactory.scrollBy(0, SCROLL_DISTANCE));
                    break;
                case 2:
                    mMap.animateCamera(CameraUpdateFactory.scrollBy(-SCROLL_DISTANCE, 0));
                    break;
                case 3:
                    mMap.animateCamera(CameraUpdateFactory.scrollBy(SCROLL_DISTANCE, 0));
                    break;
                default:
                    break;
            }

        }
    }

    /**
     * Task that performs reverse geocoding to obtain places address.
     */
    private class GeocodingAsyncTask extends AsyncTask<LatLng, Address, Address> {

        @Override
        protected Address doInBackground(LatLng... positions) {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            Address address = null;
            try {
                List<Address> addressList = geocoder.getFromLocation(positions[0].latitude, positions[0].longitude, 1);
                if (addressList != null && addressList.size() > 0) {
                    address = addressList.get(0);
                }
            } catch (IOException e) {
                Ln.d("Unable connect to Geocoder: %s", e);
            }
            return address;
        }

        @Override
        protected void onPostExecute(Address address) {
            super.onPostExecute(address);

            if (address == null) {
                return;
            }

            GeneralPoi poi = new GeneralPoi();
            poi.setLatitude(address.getLatitude());
            poi.setLongitude(address.getLongitude());
            poi.setName(address.getAddressLine(0));
            poi.setStreet(AddressUtils.getMaxAddress(address));

            Intent intent = new Intent();
            Ln.d("place data intent %s", intent);
            intent.putExtra("poi", poi);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();

        }
    }
}
