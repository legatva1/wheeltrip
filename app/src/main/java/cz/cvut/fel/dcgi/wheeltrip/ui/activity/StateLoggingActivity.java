package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import roboguice.activity.RoboActivity;
import roboguice.util.Ln;

/**
 * Activity that logs activity lifecycle states.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 19. 12. 2015
 */
public abstract class StateLoggingActivity extends RoboActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        log("onCreate(savedInstanceState=%s)", savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        log("onStart()");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        log("onRestart()");
        super.onRestart();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        log("onRestoreInstanceState(savedInstanceState=%s)", savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        log("onActivityResult(requestCode=%d, resultCode=%d, data=%s", requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        log("onNewIntent(intent=%s", intent);
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        log("onResume()");
        super.onResume();
    }

    @Override
    protected void onPostResume() {
        log("onPostResume()");
        super.onPostResume();
    }

    @Override
    protected void onPause() {
        log("onPause()");
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        log("onSaveInstanceState(outState=%s)", outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        log("onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        log("onDestroy()");
        super.onDestroy();
    }

    private void log(String format, Object... args) {
        String activityName = ((Object) this).getClass().getSimpleName();
        Ln.v(activityName + "#" + format, args);
        Crashlytics.log(Log.VERBOSE, activityName, String.format(format, args) );
    }
}
