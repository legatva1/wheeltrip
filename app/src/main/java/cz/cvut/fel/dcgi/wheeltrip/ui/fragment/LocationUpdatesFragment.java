package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import cz.cvut.fel.dcgi.wheeltrip.BuildConfig;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.LocationFoundEvent;
import cz.cvut.fel.dcgi.wheeltrip.service.activityrecognition.ActivityRecognizedService;
import cz.cvut.fel.dcgi.wheeltrip.util.SettingsUtils;
import roboguice.util.Ln;

/**
 * Fragment that provides location updates.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 12. 2015
 */
public abstract class LocationUpdatesFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LatLng currentLocation;
    protected boolean mRequestLocationUpdates = true;
    public static final int LOCATION_PERMISSION_REQUEST = 0;
    PendingIntent activityIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createGoogleClient();
    }

    /**
     * Sets mock mode.
     */
    protected void setMockMode() {
        if (BuildConfig.DEBUG && SettingsUtils.isMockingEnabled(getActivity())) {
            try {
                LocationServices.FusedLocationApi.setMockMode(mGoogleApiClient, true);
            } catch (SecurityException ex) {
                Ln.d("Location permissions not granted: %s", ex.getMessage());
            }
        }
    }

    /**
     * Creates Google API Client.
     */
    protected void createGoogleClient() {
        if (mGoogleApiClient == null) {
            Ln.d("createGoogleClient");
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(ActivityRecognition.API)
                    .build();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            Ln.d("connect");
            mGoogleApiClient.connect();

        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mGoogleApiClient != null && mRequestLocationUpdates) {
            Ln.d("onConnected(bundle=%s)", bundle);
            createLocationRequest();
            startLocationUpdates();
            setMockMode();
        }
        if (mGoogleApiClient != null) {
            Ln.d("start activity recognition");
            Intent intent = new Intent(getActivity(), ActivityRecognizedService.class);
            activityIntent = PendingIntent.getService(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(mGoogleApiClient, 3000, activityIntent);

        }


    }

    /**
     * Creates location request. Request updates every 5 seconds.
     */
    protected void createLocationRequest() {
        Ln.d("createLocationRequest");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    /***
     * Starts location updates.
     */
    protected void startLocationUpdates() {
        Location loc;
        try {
            loc = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            if (loc != null) {
                currentLocation = new LatLng(loc.getLatitude(), loc.getLongitude());
                sessionData.setLastLocation(currentLocation);
            }
            Ln.d("start location requests %s %s %s", mGoogleApiClient, mLocationRequest, this);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException ex) {
            Ln.d("Location permissions not granted: %s", ex.getMessage());
        }

    }

    @Override
    public void onPause() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }

        super.onPause();

    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Ln.d("disconect google client");
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected() && mRequestLocationUpdates) {
            startLocationUpdates();
        }
    }

    /**
     * Stops location updates.
     */
    protected void stopLocationUpdates() {
        Ln.d("stop location updates");
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
        ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(mGoogleApiClient, activityIntent);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
        sessionData.setLastLocation(currentLocation);
        mAppBus.post(new LocationFoundEvent(currentLocation));
        mAppBus.post(location);
        Ln.d("New location %s", location);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                } else {
                    Toast.makeText(getActivity(), "Aplikace vyžaduje GPS.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
}
