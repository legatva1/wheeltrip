package cz.cvut.fel.dcgi.wheeltrip.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import java.util.Comparator;

import cz.cvut.fel.dcgi.wheeltrip.db.naviterier.NaviterierPoiContract;
import cz.cvut.fel.dcgi.wheeltrip.model.Poi;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.NaviterierPoi;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 22. 12. 2015
 */
public class NaviterierPoiAdapter extends BasePoiAdapter<NaviterierPoi> {

    public NaviterierPoiAdapter(Context context, LatLng location) {
        super(context, location);
    }

    @Override
    protected String getItemLabel(int position) {
        return mItems.get(position).getName();
    }

    @Override
    protected double getDistance(int position) {
        return mItems.get(position).getDistance();
    }

    @Override
    protected Comparator<NaviterierPoi> getComparator() {
        return new Comparator<NaviterierPoi>() {
            @Override
            public int compare(NaviterierPoi lhs, NaviterierPoi rhs) {
                if (lhs.getDistance() > rhs.getDistance()) {
                    return 1;
                } else if (lhs.getDistance() < rhs.getDistance()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
    }

    @Override
    protected NaviterierPoi loadDataFromCursor(Cursor cursor, NaviterierPoi item) {
        item = new NaviterierPoi();
        item.setId(cursor.getString(NaviterierPoiContract.NaviterierPoiTable.POI_ID));
        item.setName(cursor.getString(NaviterierPoiContract.NaviterierPoiTable.NAME));
        item.setLatitude(cursor.getDouble(NaviterierPoiContract.NaviterierPoiTable.LAT));
        item.setLongitude(cursor.getDouble(NaviterierPoiContract.NaviterierPoiTable.LNG));
        item.setStreet(cursor.getString(NaviterierPoiContract.NaviterierPoiTable.STREET));
        item.setStreetNumber(cursor.getString(NaviterierPoiContract.NaviterierPoiTable.LAND_REGISTRY_NUMBER));
        item.setCity(cursor.getString(NaviterierPoiContract.NaviterierPoiTable.CITY));
        item.setDistance(SphericalUtil.computeDistanceBetween(mLocation, new LatLng(item.getLatitude(), item.getLongitude())));
        return item;
    }

    @Override
    protected Uri getUri() {
        return NaviterierPoiContract.PoiEntry.CONTENT_URI;
    }

    @Override
    protected String[] getProjection() {
        return NaviterierPoiContract.PoiEntry.POI_COLUMNS;
    }

    @Override
    protected String getAppendQuery() {
        return null;
    }
}
