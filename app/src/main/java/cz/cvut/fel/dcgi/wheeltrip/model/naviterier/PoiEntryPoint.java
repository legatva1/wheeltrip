
package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PoiEntryPoint {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("PoiId")
    @Expose
    private String poiId;
    @SerializedName("DistanceFromFirstPoint")
    @Expose
    private Double distanceFromFirstPoint;
    @SerializedName("RoadId")
    @Expose
    private String roadId;
    @SerializedName("DistanceFromPoi")
    @Expose
    private Double distanceFromPoi;
    @SerializedName("Shape")
    @Expose
    private Point point;

    private LatLng latLng;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoiId() {
        return poiId;
    }

    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

    public Double getDistanceFromFirstPoint() {
        return distanceFromFirstPoint;
    }

    public void setDistanceFromFirstPoint(Double distanceFromFirstPoint) {
        this.distanceFromFirstPoint = distanceFromFirstPoint;
    }

    public String getRoadId() {
        return roadId;
    }

    public void setRoadId(String roadId) {
        this.roadId = roadId;
    }

    public Double getDistanceFromPoi() {
        return distanceFromPoi;
    }

    public void setDistanceFromPoi(Double distanceFromPoi) {
        this.distanceFromPoi = distanceFromPoi;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public LatLng getLatLng() {
        if(latLng == null){
            return new LatLng(point.getLatitude(),point.getLongitude());
        }
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    @Override
    public String toString() {
        return "PoiEntryPoint{" +
                "id='" + id + '\'' +
                ", poiId='" + poiId + '\'' +
                ", distanceFromFirstPoint=" + distanceFromFirstPoint +
                ", roadId='" + roadId + '\'' +
                ", distanceFromPoi=" + distanceFromPoi +
                ", point=" + point +
                ", latLng=" + latLng +
                '}';
    }
}
