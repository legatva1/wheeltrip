package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import cz.cvut.fel.dcgi.wheeltrip.ui.fragment.MapFragment;

/**
 * Activity that contains MapFragment.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */
public final class MapsActivity extends UiActivity<MapFragment> {

}
