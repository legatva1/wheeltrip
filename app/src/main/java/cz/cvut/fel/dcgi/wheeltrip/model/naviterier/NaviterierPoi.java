package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import cz.cvut.fel.dcgi.wheeltrip.model.PoiImpl;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */

public class NaviterierPoi extends PoiImpl {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Shape")
    @Expose
    private Point point;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public double getLatitude() {
        return point.getLatitude();
    }

    @Override
    public double getLongitude() {
        return point.getLongitude();
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void setLatitude(double latitude) {
        if (point == null) {
            point = new Point();
        }
        point.setLatitude(latitude);
    }

    @Override
    public void setLongitude(double longitude) {
        if (point == null) {
            point = new Point();
        }
        point.setLongitude(longitude);
    }
}
