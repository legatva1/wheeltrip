package cz.cvut.fel.dcgi.wheeltrip.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cz.cvut.fel.dcgi.wheeltrip.db.naviterier.NaviterierAddressContract;
import cz.cvut.fel.dcgi.wheeltrip.db.naviterier.NaviterierPoiContract;
import cz.cvut.fel.dcgi.wheeltrip.db.vozejkmap.VozejkMapContract;
import roboguice.util.Ln;

/**
 * SQL helper for WheelTrip database.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 20. 12. 2015
 */
public final class WheelTripOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "wheeltrip.db";
    public static final int DATABASE_VERSION = 1;

    final StringBuilder vozejkmapTable = new StringBuilder()
            .append("CREATE TABLE ").append(VozejkMapContract.PoiEntry.TABLE_NAME).append(" ( ") //
            .append(VozejkMapContract.PoiEntry._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
            .append(VozejkMapContract.PoiEntry.TITLE).append(" TEXT NOT NULL, ")
            .append(VozejkMapContract.PoiEntry.LOCATION_TYPE).append(" INTEGER NOT NULL, ")
            .append(VozejkMapContract.PoiEntry.DESCRIPTION).append(" TEXT, ")
            .append(VozejkMapContract.PoiEntry.LAT).append(" REAL NOT NULL, ")
            .append(VozejkMapContract.PoiEntry.LNG).append(" REAL NOT NULL, ")
            .append(VozejkMapContract.PoiEntry.ATTR1).append(" INTEGER, ")
            .append(VozejkMapContract.PoiEntry.ATTR2).append(" TEXT, ")
            .append(VozejkMapContract.PoiEntry.ATTR3).append(" TEXT, ")
            .append(VozejkMapContract.PoiEntry.SEARCH_TITLE).append(" TEXT NOT NULL, ")
            .append(VozejkMapContract.PoiEntry.STREET).append(" TEXT, ")
            .append(VozejkMapContract.PoiEntry.STREET_NUMBER).append(" TEXT, ")
            .append(VozejkMapContract.PoiEntry.CITY).append(" TEXT);");

    final StringBuilder naviterierAddressTable = new StringBuilder()
            .append("CREATE TABLE ").append(NaviterierAddressContract.AddressEntry.TABLE_NAME).append(" ( ") //
            .append(NaviterierAddressContract.AddressEntry._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
            .append(NaviterierAddressContract.AddressEntry.ADDRESS_ID).append(" TEXT, ")
            .append(NaviterierAddressContract.AddressEntry.LAT).append(" REAL NOT NULL, ")
            .append(NaviterierAddressContract.AddressEntry.LNG).append(" REAL NOT NULL, ")
            .append(NaviterierAddressContract.AddressEntry.STREET).append(" TEXT, ")
            .append(NaviterierAddressContract.AddressEntry.HOUSE_NUMBER).append(" TEXT, ")
            .append(NaviterierAddressContract.AddressEntry.LAND_REGISTRY_NUMBER).append(" TEXT, ")
            .append(NaviterierAddressContract.AddressEntry.CITY).append(" TEXT, ")
            .append(NaviterierAddressContract.AddressEntry.SEARCH_TITLE).append(" TEXT NOT NULL);");

    final StringBuilder naviterierPoiTable = new StringBuilder()
            .append("CREATE TABLE ").append(NaviterierPoiContract.PoiEntry.TABLE_NAME).append(" ( ") //
            .append(NaviterierPoiContract.PoiEntry._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
            .append(NaviterierPoiContract.PoiEntry.POI_ID).append(" TEXT NOT NULL, ")
            .append(NaviterierPoiContract.PoiEntry.NAME).append(" TEXT NOT NULL, ")
            .append(NaviterierPoiContract.PoiEntry.LAT).append(" REAL NOT NULL, ")
            .append(NaviterierPoiContract.PoiEntry.LNG).append(" REAL NOT NULL, ")
            .append(NaviterierPoiContract.PoiEntry.STREET).append(" TEXT, ")
            .append(NaviterierPoiContract.PoiEntry.LAND_REGISTRY_NUMBER).append(" TEXT, ")
            .append(NaviterierPoiContract.PoiEntry.CITY).append(" TEXT, ")
            .append(NaviterierPoiContract.PoiEntry.SEARCH_TITLE).append(" TEXT NOT NULL);");


    public WheelTripOpenHelper(Context context) {
        super(context, WheelTripOpenHelper.DATABASE_NAME, null, WheelTripOpenHelper.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        Ln.v("Creating vozejkmap database with version %d", DATABASE_VERSION);

        db.execSQL(vozejkmapTable.toString());
        Ln.v(vozejkmapTable.toString());
        db.execSQL(naviterierAddressTable.toString());
        Ln.v(naviterierAddressTable.toString());
        db.execSQL(naviterierPoiTable.toString());
        Ln.v(naviterierPoiTable.toString());

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Ln.v("Upgrading vozejkmap database from version %d to version %d", oldVersion, newVersion);
        db.execSQL("DROP TABLE IF IT EXISTS " + VozejkMapContract.PoiEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF IT EXISTS " + NaviterierAddressContract.AddressEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF IT EXISTS " + NaviterierPoiContract.PoiEntry.TABLE_NAME);
        onCreate(db);
    }


}