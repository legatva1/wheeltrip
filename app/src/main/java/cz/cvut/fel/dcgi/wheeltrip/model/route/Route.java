
package cz.cvut.fel.dcgi.wheeltrip.model.route;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Route {

    @SerializedName("length")
    @Expose
    private double length;
    @SerializedName("difficulty")
    @Expose
    private Integer difficulty;
    @SerializedName("latLngs")
    @Expose
    private List<LatLng> latLngs = new ArrayList<>();
    @SerializedName("steps")
    @Expose
    private List<Step> steps = new ArrayList<>();
    @SerializedName("difficulties")
    @Expose
    private List<Difficulty> difficulties = new ArrayList<>();

    private String generalDescription;


    private RouteLine routeLine;
    /**
     * 
     * @return
     *     The length
     */
    public double getLength() {
        return length;
    }

    /**
     * 
     * @param length
     *     The length
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * 
     * @return
     *     The difficulty
     */
    public Integer getDifficulty() {
        return difficulty;
    }

    /**
     * 
     * @param difficulty
     *     The difficulty
     */
    public void setDifficulty(Integer difficulty) {
        this.difficulty = difficulty;
    }

    /**
     * 
     * @return
     *     The latLngs
     */
    public List<LatLng> getLatLngs() {
        return latLngs;
    }

    /**
     * 
     * @param latLngs
     *     The latLngs
     */
    public void setLatLngs(List<LatLng> latLngs) {
        this.latLngs = latLngs;
    }

    /**
     * 
     * @return
     *     The steps
     */
    public List<Step> getSteps() {
        return steps;
    }

    /**
     * 
     * @param steps
     *     The steps
     */
    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    /**
     * 
     * @return
     *     The difficulties
     */
    public List<Difficulty> getDifficulties() {
        return difficulties;
    }

    /**
     * 
     * @param difficulties
     *     The difficulties
     */
    public void setDifficulties(List<Difficulty> difficulties) {
        this.difficulties = difficulties;
    }

    public RouteLine getRouteLine() {
       return routeLine;
    }

    public void setRouteLine(RouteLine routeLine) {
        this.routeLine = routeLine;
    }

    public String getGeneralDescription() {
        return generalDescription;
    }

    public void setGeneralDescription(String generalDescription) {
        this.generalDescription = generalDescription;
    }

    @Override
    public String toString() {
        return "Route{" +
                "length=" + length +
                ", difficulty=" + difficulty +
                ", latLngs=" + latLngs +
                ", steps=" + steps +
                ", difficulties=" + difficulties +
                '}';
    }
}
