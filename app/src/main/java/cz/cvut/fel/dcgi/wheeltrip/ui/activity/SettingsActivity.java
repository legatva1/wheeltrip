package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import android.os.Bundle;

import cz.cvut.fel.dcgi.wheeltrip.ui.fragment.SettingsFragment;

/**
 * Activity that contains SettingsFragment.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */
public class SettingsActivity extends ControllerActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }


}
