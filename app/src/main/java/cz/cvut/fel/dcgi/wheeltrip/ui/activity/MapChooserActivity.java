package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import cz.cvut.fel.dcgi.wheeltrip.ui.fragment.MapChooserFragment;

/**
 * Activity that contains MapChooserFragment.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */
public class MapChooserActivity extends UiActivity<MapChooserFragment> {
}
