
package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Route {

    @SerializedName("Shape")
    @Expose
    private RouteShape routeShape;
    @SerializedName("Itinerary")
    @Expose
    private Itinerary itinerary;

    public RouteShape getRouteShape() {
        return routeShape;
    }

    public void setRouteShape(RouteShape routeShape) {
        this.routeShape = routeShape;
    }

    public Itinerary getItinerary() {
        return itinerary;
    }

    public void setItinerary(Itinerary itinerary) {
        this.itinerary = itinerary;
    }

    @Override
    public String toString() {
        return "Route{" +
                "routeShape=" + routeShape +
                ", itinerary=" + itinerary +
                '}';
    }
}
