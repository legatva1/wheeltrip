package cz.cvut.fel.dcgi.wheeltrip.ui.activity;


import cz.cvut.fel.dcgi.wheeltrip.ui.fragment.RouteSearchFragment;

/**
 * Activity that contains RouteSearchFragment.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */
public class RouteSearchActivity extends UiActivity<RouteSearchFragment> {

}
