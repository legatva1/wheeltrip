package cz.cvut.fel.dcgi.wheeltrip.db.naviterier;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Contract for NaviTerier pois.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 20. 12. 2015
 */
public final class NaviterierPoiContract {

    public static final String CONTENT_AUTHORITY = "cz.cvut.fel.dcgi.wheeltrip";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_POI = "naviterier_poi";

    public static final class PoiEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_POI).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI;

        public static final String TABLE_NAME = "NAVITERIER_POI";

        public static final String POI_ID = "POI_ID";
        public static final String NAME = "NAME";
        public static final String LAT = "LAT";
        public static final String LNG = "LNG";
        public static final String STREET = "STREET";
        public static final String LAND_REGISTRY_NUMBER = "LAND_REGISTRY_NUMBER";
        public static final String CITY = "CITY";
        public static final String SEARCH_TITLE = "SEARCH_TITLE";

        public static final String [] POI_COLUMNS = {_ID,POI_ID,NAME,LAT,LNG,STREET,LAND_REGISTRY_NUMBER,CITY,SEARCH_TITLE};

        public static Uri buildPoiUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }


    public static class NaviterierPoiTable {
        public static final int _ID = 0;
        public static final int POI_ID = 1;
        public static final int NAME = 2;
        public static final int LAT = 3;
        public static final int LNG = 4;
        public static final int STREET = 5;
        public static final int LAND_REGISTRY_NUMBER = 6;
        public static final int CITY = 7;
        public static final int SEARCH_TITLE = 8;

    }
}
