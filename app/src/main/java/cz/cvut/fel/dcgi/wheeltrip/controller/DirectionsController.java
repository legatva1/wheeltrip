package cz.cvut.fel.dcgi.wheeltrip.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.maps.android.SphericalUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.SessionData;
import cz.cvut.fel.dcgi.wheeltrip.model.directions.Direction;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Destination;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Difficulty;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Origin;
import cz.cvut.fel.dcgi.wheeltrip.model.route.RouteData;
import cz.cvut.fel.dcgi.wheeltrip.model.route.RouteLine;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Step;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.RouteActivity;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import roboguice.util.Ln;

/**
 * Controller that gets routes from Google Directions API without navigation instructions.
 * .
 *
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 6. 2. 2016
 */
@Singleton
public class DirectionsController {

    /**
     * Base url of Directions API.
     */
    private static final String BASE_URL = "https://maps.googleapis.com";

    @Inject
    private SessionData mSessionData;

    @Inject
    private MapController mMapController;

    /**
     * Creates route from Directions API call.
     *
     * @param from    origin
     * @param to      destination
     * @param context application context
     */
    public void getDirections(final LatLng from, final LatLng to, final Activity context) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        DirectionsServiceInterface service = retrofit.create(DirectionsServiceInterface.class);

        final String origin = from.latitude + "," + from.longitude;
        final String destination = to.latitude + "," + to.longitude;

        Call<Direction> call = service.getDirections(origin, destination);
        call.enqueue(new Callback<Direction>() {
            @Override
            public void onResponse(Response<Direction> response, Retrofit retrofit) {

                Ln.d("Directions API: %s", response.body().getRoutes());

                if (response.body() == null || response.body().getRoutes().isEmpty()) {
                    Toast.makeText(context, context.getResources().getString(R.string.route_not_found), Toast.LENGTH_SHORT).show();
                    return;
                }

                mSessionData.setDirection(response.body());
                mSessionData.setRoutesDrawn(false);
                mSessionData.setSelectedRouteIndex(0);
                mSessionData.setSelectedRoute(null);

                Intent intent = new Intent(context, RouteActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("from", mSessionData.getFromTitle());
                bundle.putString("to", mSessionData.getToTitle());
                intent.putExtras(bundle);

                Gson gson = new Gson();
                RouteData routeData;
                try {
                    //read test data from file
                    JsonReader reader = new JsonReader(new InputStreamReader(context.getAssets().open("mock/route1.json")));
                    routeData = gson.fromJson(reader, RouteData.class);
                    Ln.d("route data %s", routeData);


                    if (routeData != null) {

                        Ln.d("%s %s", routeData.getRoutes().size(), response.body().getRoutes().size());

                        for (int i = routeData.getRoutes().size() - 1; i > 0; i--) {
                            if (i >= response.body().getRoutes().size()) {
                                routeData.getRoutes().remove(i);
                            }
                        }


                        for (int i = 0; i < response.body().getRoutes().size(); i++) {
                            routeData.getRoutes().get(i).setLatLngs(decodePoly(response.body().getRoutes().get(i).getOverviewPolyline().getPoints()));
                            routeData.getRoutes().get(i).setLength(SphericalUtil.computeLength(routeData.getRoutes().get(i).getLatLngs()));

                            RouteLine line = new RouteLine()
                                    .setBackWidth(20)
                                    .setFrontWidth(10)
                                    .setPoints(routeData.getRoutes().get(i).getLatLngs());
                            if (i == 0) {
                                line.setFront(true);
                            }
                            routeData.getRoutes().get(i).setRouteLine(line);
                            Ln.d("set line %s", line);

                            routeData.getRoutes().get(i).setSteps(new ArrayList<Step>());
                            routeData.getRoutes().get(i).setDifficulties(new ArrayList<Difficulty>());

                            /*for (int j = 0; j < routeData.getRoutes().get(i).getSteps().size(); j++) {
                                Step step = routeData.getRoutes().get(i).getSteps().get(j);
                                if (step.getPointRef() >= routeData.getRoutes().get(i).getLatLngs().size()) {
                                    break;
                                }
                                step.setBounds(mMapController.convertCenterAndRadiusToBounds(routeData.getRoutes().get(i).getLatLngs().get(step.getPointRef()), 10));
                                step.setPosition(routeData.getRoutes().get(i).getLatLngs().get(step.getPointRef()));
                            }

                            for (int j = 0; j < routeData.getRoutes().get(i).getDifficulties().size(); j++) {
                                Difficulty difficulty = routeData.getRoutes().get(i).getDifficulties().get(j);
                                if (difficulty.getPointRef() >= routeData.getRoutes().get(i).getLatLngs().size()) {
                                    break;
                                }
                                difficulty.setBounds(mMapController.convertCenterAndRadiusToBounds(routeData.getRoutes().get(i).getLatLngs().get(difficulty.getPointRef()), 10));
                                difficulty.setPosition(routeData.getRoutes().get(i).getLatLngs().get(difficulty.getPointRef()));
                            }*/

                        }
                        Origin o = new Origin();
                        o.setTitle(mSessionData.getFromTitle());
                        o.setLatLng(routeData.getRoutes().get(0).getLatLngs().get(0));

                        Destination d = new Destination();
                        d.setTitle(mSessionData.getToTitle());
                        d.setLatLng(routeData.getRoutes().get(0).getLatLngs().get(routeData.getRoutes().get(0).getLatLngs().size() - 1));

                        routeData.setOrigin(o);
                        routeData.setDestination(d);
                        Ln.d("route data %s", routeData);


                        mSessionData.setRouteData(routeData);
                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                context.startActivity(intent);
                context.finish();

            }

            @Override
            public void onFailure(Throwable t) {
                Ln.d("Directions API failed. %s", t.getMessage());
            }
        });
    }

    /**
     * Decodes encoded polyline from Directions API.
     *
     * @param encoded encoded polyline
     * @return set of LatLng values that define a route
     */
    public List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;

    }

}
