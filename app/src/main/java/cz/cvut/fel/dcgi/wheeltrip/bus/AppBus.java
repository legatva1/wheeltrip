package cz.cvut.fel.dcgi.wheeltrip.bus;


import com.google.inject.Singleton;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 21. 12. 2015
 */
@Singleton
public class AppBus extends ThreadyBus {

    public AppBus() {
        super("appBus");
    }

}
