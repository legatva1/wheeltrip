package cz.cvut.fel.dcgi.wheeltrip.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import cz.cvut.fel.dcgi.wheeltrip.util.AddressUtils;

/**
 * Abstract class with common poi properties.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 9. 4. 2016
 */
public abstract class PoiImpl implements Poi{

    @SerializedName(value="title", alternate={"Name"})
    @Expose
    protected String name;
    @SerializedName("lat")
    @Expose
    protected double latitude;
    @SerializedName("lng")
    @Expose
    protected double longitude;
    @SerializedName(value="street", alternate={"Street"})
    @Expose
    protected String street;
    @SerializedName(value="street_number", alternate={"LandRegistryNumber"})
    @Expose
    protected String streetNumber;
    @SerializedName(value="city", alternate={"City"})
    @Expose
    protected String city;

    protected double distance;

    @Override
    public String getId() {
        return "";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getType() {
        return -1;
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public int getAccessibilityType() {
        return -1;
    }

    @Override
    public String getParkingvailable() {
        return "no";
    }

    @Override
    public String getRestroomAvailable() {
        return "no";
    }

    @Override
    public String getStreet() {
        return street;
    }

    @Override
    public String getStreetNumber() {
        return streetNumber;
    }

    @Override
    public String getCity() {
        return city;
    }

    @Override
    public String getAddress() {
        return AddressUtils.getAddressFromPoi(this);
    }

    @Override
    public double getDistance() {
        return distance;
    }

    @Override
    public void setId(String id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public void setType(int type){
        throw new UnsupportedOperationException();
    };
    @Override
    public void setDescription(String description){
        throw new UnsupportedOperationException();
    };
    @Override
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    @Override
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    @Override
    public void setAccessibilityType(int accessibilityType){
        throw new UnsupportedOperationException();
    };
    @Override
    public void setRestroomAvailable(String restroomAvailable){
        throw new UnsupportedOperationException();
    };
    @Override
    public void setParkingAvailable(String parkingAvailable){
        throw new UnsupportedOperationException();
    };
    @Override
    public void setStreet(String street) {
        this.street = street;
    }
    @Override
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }
    @Override
    public void setCity(String city) {
        this.city = city;
    }
    @Override
    public void setDistance(double distance) {
        this.distance = distance;
    }
}
