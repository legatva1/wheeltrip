package cz.cvut.fel.dcgi.wheeltrip.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import java.util.Comparator;

import cz.cvut.fel.dcgi.wheeltrip.db.naviterier.NaviterierAddressContract;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.Address;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.Point;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 22. 12. 2015
 */
public class NaviterierAddressAdapter extends BasePoiAdapter<Address> {

    public NaviterierAddressAdapter(Context context, LatLng location) {
        super(context, location);
    }

    @Override
    protected String getItemLabel(int position) {
        return mItems.get(position).getStreet() + " " + mItems.get(position).getLandRegistryNumber();
    }

    @Override
    protected double getDistance(int position) {
        return mItems.get(position).getDistance();
    }

    @Override
    protected Comparator<Address> getComparator() {
        return new Comparator<Address>() {
            @Override
            public int compare(Address lhs, Address rhs) {
                if (lhs.getDistance() > rhs.getDistance()) {
                    return 1;
                } else if (lhs.getDistance() < rhs.getDistance()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
    }

    @Override
    protected Address loadDataFromCursor(Cursor cursor, Address item) {
        item = new Address();
        item.setId(cursor.getString(NaviterierAddressContract.NaviterierAddressTable.POI_ID));
        Point point = new Point();
        point.setLatitude(cursor.getDouble(NaviterierAddressContract.NaviterierAddressTable.LAT));
        point.setLongitude(cursor.getDouble(NaviterierAddressContract.NaviterierAddressTable.LNG));
        item.setPoint(point);
        item.setStreet(cursor.getString(NaviterierAddressContract.NaviterierAddressTable.STREET));
        item.setLandRegistryNumber(cursor.getString(NaviterierAddressContract.NaviterierAddressTable.LAND_REGISTRY_NUMBER));
        item.setHouseNumber(cursor.getString(NaviterierAddressContract.NaviterierAddressTable.HOUSE_NUMBER));
        item.setCity(cursor.getString(NaviterierAddressContract.NaviterierAddressTable.CITY));
        item.setDistance(SphericalUtil.computeDistanceBetween(mLocation, new LatLng(item.getPoint().getLatitude(), item.getPoint().getLongitude())));
        return item;
    }

    @Override
    protected Uri getUri() {
        return NaviterierAddressContract.AddressEntry.CONTENT_URI;
    }

    @Override
    protected String[] getProjection() {
        return NaviterierAddressContract.AddressEntry.ADDRESS_COLUMNS;
    }

    @Override
    protected String getAppendQuery() {
        return null;
    }
}
