package cz.cvut.fel.dcgi.wheeltrip.bus;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Helper class - posts from any thread are received on the main thread.
 * @link http://stackoverflow.com/questions/15431768/how-to-send-event-from-service-to-activity-with-otto-event-bus
 * @since 21. 12. 2015
 */
public class ThreadyBus extends Bus {


        private final Handler mainThread = new Handler(Looper.getMainLooper());

        public ThreadyBus() {
        }

        public ThreadyBus(String identifier) {
            super(identifier);
        }

        public ThreadyBus(ThreadEnforcer enforcer) {
            super(enforcer);
        }

        public ThreadyBus(ThreadEnforcer enforcer, String identifier) {
            super(enforcer, identifier);
        }

        @Override
        public void post(final Object event) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                ThreadyBus.super.post(event);
            } else {
                mainThread.post(new Runnable() {
                    @Override
                    public void run() {
                        ThreadyBus.super.post(event);
                    }
                });
            }
        }

}
