package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.widget.Toast;

import java.util.Locale;

/**
 * Speech recognition fragment.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 24. 2. 2016
 */
public class SpeechRecognitionFragment extends BaseFragment {

    protected void promptSpeechInput(int code) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Mluvte");
        try {
            startActivityForResult(intent, code);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(),
                    "speach recognition is not supported",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
