package cz.cvut.fel.dcgi.wheeltrip.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.model.filter.FilterItem;
import cz.cvut.fel.dcgi.wheeltrip.util.IconUtils;

/**
 * Adapter used for poi type filter.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 29. 12. 2015
 */
public class PoiTypeFilterAdapter extends RecyclerView.Adapter<PoiTypeFilterAdapter.ViewHolder> {

    private List<FilterItem> mFilterItems = Collections.emptyList();
    private int selected;

    /**
     * Holder of filter item.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView icon;
        public CheckBox checkbox;


        public ViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.filter_poi_title);
            icon = (ImageView) v.findViewById(R.id.filter_poi_type);
            checkbox = (CheckBox) v.findViewById(R.id.filter_checkbox);

        }

    }

    /**
     * Gets selected position.
     * @return selected position
     */
    public int getSelected() {
        return selected;
    }

    public PoiTypeFilterAdapter(List<FilterItem> filterItems) {
        this.mFilterItems = filterItems;
    }

    /**
     * Checks list item checkbox.
     * @param position item position
     */
    public void setChecked(int position) {
        if (mFilterItems.get(position).isChecked()) {
            mFilterItems.get(position).setChecked(false);

        } else {
            mFilterItems.get(position).setChecked(true);
        }
        notifyDataSetChanged();
    }

    /**
     * Checks all items.
     */
    public void selectAll() {
        for (FilterItem item : mFilterItems) {
            item.setChecked(true);
        }
        notifyDataSetChanged();

    }

    /**
     * Unchecks all items.
     */
    public void unselectAll() {
        for (FilterItem item : mFilterItems) {
            item.setChecked(false);
        }
        notifyDataSetChanged();

    }

    /**
     * Selects next or previous item in a list.
     * @param direction direction to move to (-1 up, 1 down)
     * @param recyclerView recycler view
     */
    public void setSelected(int direction, RecyclerView recyclerView) {

        int moveTo = selected + direction;
        if (moveTo < 0 || moveTo > mFilterItems.size() - 1) {
            return;
        }
        selected = moveTo;

        recyclerView.scrollToPosition(moveTo);
        notifyDataSetChanged();
    }

    /**
     * Selects custom list item.
     * @param position position of item to select
     * @param recyclerView recycler view
     */
    public void setSelectedPosition(int position, RecyclerView recyclerView) {

        int moveTo = position;
        if (moveTo < 0 || moveTo > mFilterItems.size() - 1) {
            return;
        }
        selected = moveTo;

        recyclerView.scrollToPosition(moveTo);
        notifyDataSetChanged();
    }


    @Override
    public PoiTypeFilterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.filter_row, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.title.setText(mFilterItems.get(position).getTitle());
        holder.icon.setImageResource(IconUtils.getPoiIcon(mFilterItems.get(position).getLocationType()));
        holder.checkbox.setChecked(mFilterItems.get(position).isChecked());
        holder.itemView.setSelected(selected == position);
    }


    @Override
    public int getItemCount() {
        if (mFilterItems == null) {
            return 0;
        }
        return mFilterItems.size();
    }


    /**
     * Gets filter item.
     * @param position item position
     * @return filter item.
     */
    public FilterItem getItem(int position) {
        if (mFilterItems == null || mFilterItems.size() <= position) {
            return null;
        }
        return mFilterItems.get(position);
    }
}
