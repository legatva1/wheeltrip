package cz.cvut.fel.dcgi.wheeltrip.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.util.DistanceUtils;
import roboguice.util.Ln;

/**
 * Base adapter for POI objects.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 15. 5. 2016
 */
public abstract class BasePoiAdapter<T> extends RecyclerView.Adapter<BasePoiAdapter.ViewHolder> implements Filterable {

    protected Context mContext;
    protected LatLng mLocation;
    protected int mSelected;

    protected List<T> mItems = Collections.emptyList();

    public BasePoiAdapter(Context context, LatLng location) {
        this.mContext = context;
        mLocation = location;
    }


    @Override
    public BasePoiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.poi_text_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BasePoiAdapter.ViewHolder holder, int position) {
        holder.mTextView.setText(getItemLabel(position));
        holder.mImageView.setVisibility(View.GONE);
        holder.mDistanceView.setText(DistanceUtils.getStringDistance(getDistance(position)));
        holder.itemView.setSelected(mSelected == position);
    }

    @Override
    public int getItemCount() {
        if (mItems == null) {
            return 0;
        }
        return mItems.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null) {
                    mItems = getAutocomplete(constraint);
                    if (mItems != null) {
                        results.values = mItems;
                        results.count = mItems.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    /**
     * Gets list of POI objects.
     * @param constraint search query
     * @return list of POI objects
     */
    protected List<T> getAutocomplete(CharSequence constraint){

        String query = StringUtils.stripAccents(constraint.toString()).toLowerCase();

        String selection = "SEARCH_TITLE LIKE '" + query + "%' OR SEARCH_TITLE LIKE '% " + query + "%'";

        if(!StringUtils.isEmpty(getAppendQuery())){
            selection += " AND "+getAppendQuery();
        }

        Cursor cursor = mContext.getContentResolver().query(getUri(), getProjection(), selection, null, null);

        List<T> items = new ArrayList<>();
        T item = null;

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    items.add(loadDataFromCursor(cursor, item));

                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        Collections.sort(items,getComparator());

        return items;

    }

    /**
     * Gets label of list item.
     * @param position list item position
     * @return item label
     */
    protected abstract String getItemLabel(int position);

    /**
     * Gets distance to poi from current location.
     * @param position list item position
     * @return distance to poi
     */
    protected abstract double getDistance(int position);

    /**
     * Return distance comparator.
     * @return distance comparator
     */
    protected abstract Comparator<T>getComparator();

    /**
     * Loads data from cursor into object.
     * @param cursor db cursor
     * @param item poi object
     */
    protected abstract T loadDataFromCursor(Cursor cursor, T item);

    /**
     * Gets Uri of poi data
     * @return uri
     */
    protected abstract Uri getUri();

    /**
     * Gets array of db columns to return in query.
     * @return poi object columns
     */
    protected abstract String[] getProjection();

    /**
     * Optional append query.
     * @return append query
     */
    protected abstract String getAppendQuery();

    /**
     * Holder for list item.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public ImageView mImageView;
        public TextView mDistanceView;

        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.info_text);
            mImageView = (ImageView) v.findViewById(R.id.poi_icon);
            mDistanceView = (TextView) v.findViewById(R.id.poi_distance);
        }
    }

    /**
     * Selects next or previous item in a list.
     * @param direction direction to move to (-1 up, 1 down)
     * @param recyclerView recycler view
     */
    public void setSelected(int direction, RecyclerView recyclerView) {
        Ln.d("Setting selected position %d", mSelected);
        int moveTo = mSelected + direction;
        if (moveTo < 0 || moveTo > mItems.size() - 1) {
            return;
        }
        mSelected = moveTo;

        recyclerView.scrollToPosition(moveTo);
        notifyDataSetChanged();
    }

    /**
     * Selects custom list item.
     * @param position position of item to select
     * @param recyclerView recycler view
     */
    public void setSelectedPosition(int position, RecyclerView recyclerView) {
        Ln.d("Setting selected position %d", mSelected);
        int moveTo = position;
        if (moveTo < 0 || moveTo > mItems.size() - 1) {
            return;
        }
        mSelected = moveTo;

        recyclerView.scrollToPosition(moveTo);
        notifyDataSetChanged();
    }

    /**
     * Updates data with new list of items.
     * @param newPois updated values
     */
    public void updateList(List<T> newPois) {
        mItems = newPois;
        notifyDataSetChanged();
    }

    /**
     * Gets list item.
     * @param position position of item in the list.
     * @return list item on defined position
     */
    public T getItem(int position) {
        if (mItems == null || mItems.size() <= position) {
            return null;
        }
        return mItems.get(position);
    }

    /**
     * Gets currently selected position.
     * @return selected position
     */
    public int getSelected() {
        return mSelected;
    }

}
