package cz.cvut.fel.dcgi.wheeltrip.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Difficulty;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Obstacle;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Route;
import cz.cvut.fel.dcgi.wheeltrip.util.DistanceUtils;
import cz.cvut.fel.dcgi.wheeltrip.util.IconUtils;

/**
 * Routes pager adapter.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 29. 1. 2016
 */
public class RoutePagerAdapter extends PagerAdapter {

    private Context mContext;
    private List<Route> routes;

    public RoutePagerAdapter(Context context, List<Route> routes) {
        mContext = context;
        this.routes = routes;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.route_item, collection, false);
        TextView textView = (TextView) layout.findViewById(R.id.route_length);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ic_route_difficulty);

        ImageView stairs = (ImageView) layout.findViewById(R.id.difficulty_stairs);
        ImageView badSurface = (ImageView) layout.findViewById(R.id.difficulty_bad_surface);
        ImageView steepHillUp = (ImageView) layout.findViewById(R.id.difficulty_stepp_hill_up);
        ImageView steepHillDown = (ImageView) layout.findViewById(R.id.difficulty_steep_hill_down);
        ImageView narrowRoad = (ImageView) layout.findViewById(R.id.difficulty_narrow_road);

        for (int i = 0; i < routes.get(position).getDifficulties().size(); i++) {
            Difficulty difficulty = routes.get(position).getDifficulties().get(i);
            if(difficulty.getType().equals(Obstacle.STAIRS.getType())){
                stairs.setVisibility(View.VISIBLE);
            }
            if(difficulty.getType().equals(Obstacle.BAD_SURFACE.getType())){
                badSurface.setVisibility(View.VISIBLE);
            }
            if(difficulty.getType().equals(Obstacle.STEEP_HILL_UP.getType())){
                steepHillUp.setVisibility(View.VISIBLE);
            }
            if(difficulty.getType().equals(Obstacle.STEEP_HILL_DOWN.getType())){
                steepHillDown.setVisibility(View.VISIBLE);
            }
            if(difficulty.getType().equals(Obstacle.NARROW_ROAD.getType())){
                narrowRoad.setVisibility(View.VISIBLE);
            }
        }

        textView.setText(DistanceUtils.getStringDistance(routes.get(position).getLength()));
        imageView.setImageDrawable(mContext.getResources().getDrawable(IconUtils.getRouteDifficultyIcon(routes.get(position).getDifficulty())));
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return routes.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}
