package cz.cvut.fel.dcgi.wheeltrip.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import cz.cvut.fel.dcgi.wheeltrip.db.naviterier.NaviterierAddressContract;
import cz.cvut.fel.dcgi.wheeltrip.db.naviterier.NaviterierPoiContract;
import cz.cvut.fel.dcgi.wheeltrip.db.vozejkmap.VozejkMapContract;

/**
 * Content provider.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 21. 12. 2015
 */
public class WheelTripProvider extends ContentProvider {

    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private WheelTripOpenHelper mOpenHelper;

    public static final String CONTENT_AUTHORITY = "cz.cvut.fel.dcgi.wheeltrip";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    static final int VOZEJKMAP_POIS = 100;
    static final int VOZEJKMAP_POI = 101;

    static final int NAVITERIER_ADDRESSES = 200;
    static final int NAVITERIER_ADDRESS = 201;

    static final int NAVITERIER_POIS = 300;
    static final int NAVITERIER_POI = 301;

    static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = CONTENT_AUTHORITY;

        matcher.addURI(authority, VozejkMapContract.PATH_POI, VOZEJKMAP_POIS);
        matcher.addURI(authority, VozejkMapContract.PATH_POI + "/#", VOZEJKMAP_POI);

        matcher.addURI(authority, NaviterierAddressContract.PATH_ADDRESS, NAVITERIER_ADDRESSES);
        matcher.addURI(authority, NaviterierAddressContract.PATH_ADDRESS + "/#", NAVITERIER_ADDRESS);

        matcher.addURI(authority, NaviterierPoiContract.PATH_POI, NAVITERIER_POIS);
        matcher.addURI(authority, NaviterierPoiContract.PATH_POI + "/#", NAVITERIER_POI);
        return matcher;
    }


    @Override
    public boolean onCreate() {
        mOpenHelper = new WheelTripOpenHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();


        int uriType = sUriMatcher.match(uri);
        switch (uriType) {
            case VOZEJKMAP_POIS:
                queryBuilder.setTables(VozejkMapContract.PoiEntry.TABLE_NAME);
                break;
            case VOZEJKMAP_POI:
                queryBuilder.setTables(VozejkMapContract.PoiEntry.TABLE_NAME);
                queryBuilder.appendWhere(VozejkMapContract.PoiEntry._ID + "="
                        + uri.getLastPathSegment());
                break;
            case NAVITERIER_ADDRESSES:
                queryBuilder.setTables(NaviterierAddressContract.AddressEntry.TABLE_NAME);
                break;
            case NAVITERIER_ADDRESS:
                queryBuilder.setTables(NaviterierAddressContract.AddressEntry.TABLE_NAME);
                queryBuilder.appendWhere(NaviterierAddressContract.AddressEntry._ID + "="
                        + uri.getLastPathSegment());
                break;
            case NAVITERIER_POIS:
                queryBuilder.setTables(NaviterierPoiContract.PoiEntry.TABLE_NAME);
                break;
            case NAVITERIER_POI:
                queryBuilder.setTables(NaviterierPoiContract.PoiEntry.TABLE_NAME);
                queryBuilder.appendWhere(NaviterierPoiContract.PoiEntry._ID + "="
                        + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        if(getContext() != null){
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case VOZEJKMAP_POI:
                return VozejkMapContract.PoiEntry.CONTENT_ITEM_TYPE;
            case VOZEJKMAP_POIS:
                return VozejkMapContract.PoiEntry.CONTENT_TYPE;
            case NAVITERIER_ADDRESS:
                return NaviterierAddressContract.AddressEntry.CONTENT_ITEM_TYPE;
            case NAVITERIER_ADDRESSES:
                return NaviterierAddressContract.AddressEntry.CONTENT_TYPE;
            case NAVITERIER_POI:
                return NaviterierPoiContract.PoiEntry.CONTENT_ITEM_TYPE;
            case NAVITERIER_POIS:
                return NaviterierPoiContract.PoiEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case VOZEJKMAP_POIS: {
                long _id = db.insert(VozejkMapContract.PoiEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = VozejkMapContract.PoiEntry.buildPoiUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case NAVITERIER_ADDRESSES: {
                long _id = db.insert(NaviterierAddressContract.AddressEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = NaviterierAddressContract.AddressEntry.buildPoiUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case NAVITERIER_POIS: {
                long _id = db.insert(NaviterierPoiContract.PoiEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = NaviterierPoiContract.PoiEntry.buildPoiUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        // this makes delete all rows return the number of rows deleted
        if ( null == selection ) selection = "1";
        switch (match) {
            case VOZEJKMAP_POIS:
                rowsDeleted = db.delete(
                        VozejkMapContract.PoiEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case NAVITERIER_ADDRESSES:
                rowsDeleted = db.delete(
                        NaviterierAddressContract.AddressEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case NAVITERIER_POIS:
                rowsDeleted = db.delete(
                        NaviterierPoiContract.PoiEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if (rowsDeleted != 0) {
            if(getContext()!=null){
                getContext().getContentResolver().notifyChange(uri, null);
            }
        }
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case VOZEJKMAP_POIS:
                rowsUpdated = db.update(VozejkMapContract.PoiEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case NAVITERIER_ADDRESSES:
                rowsUpdated = db.update(NaviterierAddressContract.AddressEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case NAVITERIER_POIS:
                rowsUpdated = db.update(NaviterierPoiContract.PoiEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            if(getContext()!=null){
                getContext().getContentResolver().notifyChange(uri, null);
            }
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case VOZEJKMAP_POIS:
                return performBulkInsert(uri,VozejkMapContract.PoiEntry.TABLE_NAME,values,db);
            case NAVITERIER_ADDRESSES:
                return performBulkInsert(uri,NaviterierAddressContract.AddressEntry.TABLE_NAME,values,db);
            case NAVITERIER_POIS:
                return performBulkInsert(uri,NaviterierPoiContract.PoiEntry.TABLE_NAME,values,db);
            default:
                return super.bulkInsert(uri, values);
        }
    }

    private int performBulkInsert(Uri uri, String tableName, ContentValues[] values, SQLiteDatabase db){
        db.beginTransaction();
        int returnCount = 0;
        try {
            for (ContentValues value : values) {
                long _id = db.insert(tableName, null, value);
                if (_id != -1) {
                    returnCount++;
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        if(getContext()!=null){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return returnCount;
    }
}
