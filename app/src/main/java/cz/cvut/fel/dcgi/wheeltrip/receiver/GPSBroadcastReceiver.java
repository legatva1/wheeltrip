package cz.cvut.fel.dcgi.wheeltrip.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.widget.Toast;

/**
 * Receives GPS state changes.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 11. 3. 2016
 */
public class GPSBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.location.PROVIDERS_CHANGED")) {
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            boolean gps = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if(gps){
                Toast.makeText(context,"GPS enabled",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(context,"GPS disabled",Toast.LENGTH_SHORT).show();
            }
        }

    }
}
