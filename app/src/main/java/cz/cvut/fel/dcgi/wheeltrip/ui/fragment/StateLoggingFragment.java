package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import roboguice.util.Ln;

/**
 * Fragment that logs fragment lifecycle states.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 19. 12. 2015
 */
public abstract class StateLoggingFragment extends roboguice.fragment.provided.RoboFragment{

    @Override
    public void onAttach(Context context) {
        log("onAttach(activity=%s)", context);
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        log("onCreate(savedInstanceState=%s)", savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        log("onCreateView(inflater=%s, container=%s, savedInstanceState=%s)", inflater, container, savedState);
        return super.onCreateView(inflater, container, savedState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        log("onViewCreated(view=%s, savedInstanceState=%s)", view, savedInstanceState);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        log("onActivityCreated(savedInstanceState=%s)", savedInstanceState);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        log("onStart()");
        super.onStart();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        log("onActivityResult(requestCode=%d, resultCode=%d, data=%s", requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        log("onResume()");
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        log("onSaveInstanceState(outState=%s)", outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        log("onPause()");
        super.onPause();
    }

    @Override
    public void onStop() {
        log("onStop()");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        log("onDestroyView()");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        log("onDestroy()");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        log("onDetach()");
        super.onDetach();
    }

    private void log(String format, Object... args) {
        String activityName = ((Object) this).getClass().getSimpleName();
        Ln.v(activityName + "#" + format, args);
    }
}
