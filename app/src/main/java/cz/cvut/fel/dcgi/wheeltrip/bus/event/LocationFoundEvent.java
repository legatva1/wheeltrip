package cz.cvut.fel.dcgi.wheeltrip.bus.event;

import com.google.android.gms.maps.model.LatLng;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 27. 12. 2015
 */
public class LocationFoundEvent {

    private LatLng location;

    public LocationFoundEvent(LatLng location) {
        this.location = location;
    }

    public LatLng getLocation() {
        return location;
    }
}
