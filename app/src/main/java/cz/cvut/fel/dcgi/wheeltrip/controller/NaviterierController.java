package cz.cvut.fel.dcgi.wheeltrip.controller;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.maps.android.SphericalUtil;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.SessionData;
import cz.cvut.fel.dcgi.wheeltrip.WheelTripApplication;
import cz.cvut.fel.dcgi.wheeltrip.bus.AppBus;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.SourceDataFound;
import cz.cvut.fel.dcgi.wheeltrip.db.WheelTripOpenHelper;
import cz.cvut.fel.dcgi.wheeltrip.db.naviterier.NaviterierAddressContract;
import cz.cvut.fel.dcgi.wheeltrip.db.naviterier.NaviterierPoiContract;
import cz.cvut.fel.dcgi.wheeltrip.model.Poi;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.Address;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.Addresses;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.EmptyRequest;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.NaviterierPoi;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.Point;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.Pois;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.RouteData;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.RouteRequest;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.SourceData;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.SourceDataRequest;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Destination;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Origin;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Route;
import cz.cvut.fel.dcgi.wheeltrip.model.route.RouteLine;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Step;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.RouteActivity;
import cz.cvut.fel.dcgi.wheeltrip.util.IconUtils;
import cz.cvut.fel.dcgi.wheeltrip.util.PreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import roboguice.util.Ln;

/**
 * Controller that works with NaviTerier system.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 23. 3. 2016
 */
@Singleton
public class NaviterierController {

    /**
     * Base url for NatiTerier endpoint.
     */
    private static final String BASE_URL = "http://147.32.81.71/NaviTerier.ProcessingService/json/reply";

    @Inject
    private SessionData mSessionData;

    @Inject
    private AppBus mAppBus;

    @Inject
    private MapController mMapController;


    /**
     * Creates route data from NaviTerier response.
     * @param originId street name of origin
     * @param destinationId street name of destination
     * @param context application context
     */
    public void getRouteData(final String originId, final String destinationId, final Activity context) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NaviterierServiceInterface service = retrofit.create(NaviterierServiceInterface.class);
        RouteRequest request = new RouteRequest();
        request.setOriginTextInput(originId);
        request.setDestinationTextInput(destinationId);

        Gson gson = new Gson();
        Ln.d("routejson %s",gson.toJson(request));

        Call<RouteData> call = service.getFindRoutes(request);
        Ln.d("call: %s", call);
        call.enqueue(new Callback<RouteData>() {
            @Override
            public void onResponse(Response<RouteData> response, Retrofit retrofit) {

                if(response.body() == null || response.body().getRoutes().size() == 0){
                    Toast.makeText(context, context.getResources().getString(R.string.route_not_found), Toast.LENGTH_SHORT).show();
                    return;
                }

                mSessionData.setRoutesDrawn(false);
                mSessionData.setSelectedRouteIndex(0);
                mSessionData.setSelectedRoute(null);

                Intent intent = new Intent(context, RouteActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("from", mSessionData.getFromTitle());
                bundle.putString("to", mSessionData.getToTitle());
                intent.putExtras(bundle);

                cz.cvut.fel.dcgi.wheeltrip.model.route.RouteData routeData = new cz.cvut.fel.dcgi.wheeltrip.model.route.RouteData();
                List<Route>routes=new ArrayList<Route>();

                for (int i = 0; i < response.body().getRoutes().size(); i++) {
                    Route route = new Route();
                    route.setDifficulty(0);
                    cz.cvut.fel.dcgi.wheeltrip.model.naviterier.Route respRoute = response.body().getRoutes().get(i);
                    route.setLatLngs(respRoute.getRouteShape().getLatLngs());
                    route.setLength(SphericalUtil.computeLength(respRoute.getRouteShape().getLatLngs()));

                    RouteLine line = new RouteLine()
                            .setBackWidth(20)
                            .setFrontWidth(10)
                            .setPoints(respRoute.getRouteShape().getLatLngs());
                    if (i == 0) {
                        line.setFront(true);
                    }
                    route.setRouteLine(line);
                    Ln.d("set line %s", line);

                    Origin o = new Origin();
                    o.setTitle(mSessionData.getFromTitle());
                    o.setLatLng(respRoute.getRouteShape().getLatLngs().get(0));

                    Destination d = new Destination();
                    d.setTitle(mSessionData.getToTitle());
                    d.setLatLng(respRoute.getRouteShape().getLatLngs().get(respRoute.getRouteShape().getLatLngs().size() - 1));

                    routeData.setOrigin(o);
                    routeData.setDestination(d);

                    List<Step>steps = new ArrayList<>();

                    for (int j = 0; j < respRoute.getItinerary().getStages().size(); j++) {
                        Step step = new Step();
                        step.setInstruction(respRoute.getItinerary().getStages().get(j));
                        step.setPointRef(j);
                        step.setBounds(mMapController.convertCenterAndRadiusToBounds(respRoute.getRouteShape().getLatLngs().get(step.getPointRef()), 10));
                        step.setPosition(respRoute.getRouteShape().getLatLngs().get(step.getPointRef()));
                        step.setType(IconUtils.getDirectionArrowForStage(step.getInstruction()).getType());
                        steps.add(step);
                    }

                    route.setGeneralDescription(respRoute.getItinerary().getGeneralDescription());
                    route.setSteps(steps);
                    routes.add(route);

                }

                routeData.setRoutes(routes);
                Ln.d("route data %s", routeData);
                mSessionData.setRouteData(routeData);
                context.startActivity(intent);
                context.finish();

            }

            @Override
            public void onFailure(Throwable t) {
                Ln.d("NaviTerier API failed. %s", t.getMessage());
            }
        });
    }

    /**
     * Asynchronous task to insert naviterier pois into database.
     */
    private class InsertNaviterierPoisTask extends AsyncTask<List<NaviterierPoi>, String, Void> {

        @Override
        protected Void doInBackground(List<NaviterierPoi>... poisArray) {
            insertAllNaviterierPois(poisArray[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            super.onPostExecute(v);
            PreferencesUtils.putBoolean(PreferencesUtils.NAVITERIER_POI_DOWNLOADED, true);
        }
    }

    /**
     * Performs bulk insert of naviterier pois.
     * @param pois pois to insert
     */
    private void insertAllNaviterierPois(List<NaviterierPoi> pois) {
        ContentValues[] values = new ContentValues[pois.size()];
        ContentValues contentValues;

        for (int i = 0; i < pois.size(); i++) {
            contentValues = new ContentValues();
            contentValues.put(NaviterierPoiContract.PoiEntry.POI_ID, pois.get(i).getId());
            contentValues.put(NaviterierPoiContract.PoiEntry.NAME, pois.get(i).getName());
            contentValues.put(NaviterierPoiContract.PoiEntry.LAT, pois.get(i).getLatitude());
            contentValues.put(NaviterierPoiContract.PoiEntry.LNG, pois.get(i).getLongitude());
            contentValues.put(NaviterierPoiContract.PoiEntry.STREET, pois.get(i).getStreet());
            contentValues.put(NaviterierPoiContract.PoiEntry.LAND_REGISTRY_NUMBER, pois.get(i).getStreetNumber());
            contentValues.put(NaviterierPoiContract.PoiEntry.CITY, pois.get(i).getCity());
            contentValues.put(NaviterierPoiContract.PoiEntry.SEARCH_TITLE, StringUtils.stripAccents(pois.get(i).getName()).toLowerCase());
            values[i] = contentValues;
        }

        WheelTripApplication.get().getContentResolver().bulkInsert(NaviterierPoiContract.PoiEntry.CONTENT_URI, values);
    }

    /**
     * Asynchronous task to insert naviterier addresses into database.
     */
    private class InsertNaviterierAddressesTask extends AsyncTask<List<Address>, String, Void> {

        @Override
        protected Void doInBackground(List<Address>... addressesArray) {
            insertAllNaviterierAddresses(addressesArray[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            super.onPostExecute(v);
            PreferencesUtils.putBoolean(PreferencesUtils.NAVITERIER_ADDRESS_DOWNLOADED, true);
        }
    }

    /**
     * Performs bulk insert of naviterier addresses.
     * @param addresses addresses to insert
     */
    private void insertAllNaviterierAddresses(List<Address> addresses) {
        ContentValues[] values = new ContentValues[addresses.size()];
        ContentValues contentValues;

        for (int i = 0; i < addresses.size(); i++) {
            contentValues = new ContentValues();
            contentValues.put(NaviterierAddressContract.AddressEntry.ADDRESS_ID, addresses.get(i).getId());
            contentValues.put(NaviterierAddressContract.AddressEntry.LAT, addresses.get(i).getPoint().getLatitude());
            contentValues.put(NaviterierAddressContract.AddressEntry.LNG, addresses.get(i).getPoint().getLongitude());
            contentValues.put(NaviterierAddressContract.AddressEntry.STREET, addresses.get(i).getStreet());
            contentValues.put(NaviterierAddressContract.AddressEntry.HOUSE_NUMBER, addresses.get(i).getHouseNumber());
            contentValues.put(NaviterierAddressContract.AddressEntry.LAND_REGISTRY_NUMBER, addresses.get(i).getLandRegistryNumber());
            contentValues.put(NaviterierAddressContract.AddressEntry.CITY, addresses.get(i).getCity());
            contentValues.put(NaviterierAddressContract.AddressEntry.SEARCH_TITLE, StringUtils.stripAccents(addresses.get(i).getStreet()).toLowerCase());
            values[i] = contentValues;
        }

        WheelTripApplication.get().getContentResolver().bulkInsert(NaviterierAddressContract.AddressEntry.CONTENT_URI, values);
    }

    /**
     * Calls NaviTerier endpoint to get pois.
     */
    public void downloadPois() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NaviterierServiceInterface service = retrofit.create(NaviterierServiceInterface.class);
        Gson gson = new Gson();
        gson.toJson(new Object());

        Call<Pois> call = service.getPois(new EmptyRequest());
        call.enqueue(new Callback<Pois>() {
            @Override
            public void onResponse(Response<Pois> response, Retrofit retrofit) {

                new InsertNaviterierPoisTask().execute(response.body().getPois());
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(WheelTripApplication.get(), "naviterier download failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Calls NaviTerier endpoint to get addresses.
     */
    public void downloadAddresses() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NaviterierServiceInterface service = retrofit.create(NaviterierServiceInterface.class);

        Call<Addresses> call = service.getAddresses(new EmptyRequest());
        call.enqueue(new Callback<Addresses>() {
            @Override
            public void onResponse(Response<Addresses> response, Retrofit retrofit) {

                new InsertNaviterierAddressesTask().execute(response.body().getAddresses());
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(WheelTripApplication.get(), "naviterier download failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Gets pois from database.
     * @return pois
     */
    public List<Poi> getPois() {

        List<Poi> pois;
        WheelTripOpenHelper helper = new WheelTripOpenHelper(WheelTripApplication.get());
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(NaviterierPoiContract.PoiEntry.TABLE_NAME, NaviterierPoiContract.PoiEntry.POI_COLUMNS, null,
                null, null, null, null, null);
        pois = getPoisFromCursor(cursor);
        Ln.d("pois size %s", pois.size());
        db.close();
        return pois;
    }

    /**
     * Gets addresses from database.
     * @return addresses
     */
    public List<Address> getAddresses() {
        List<Address> addresses;
        WheelTripOpenHelper helper = new WheelTripOpenHelper(WheelTripApplication.get());
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(NaviterierAddressContract.AddressEntry.TABLE_NAME, NaviterierAddressContract.AddressEntry.ADDRESS_COLUMNS, null,
                null, null, null, null, null);
        addresses = getAddressesFromCursor(cursor);
        Ln.d("addresses size %s", addresses.size());
        db.close();
        return addresses;

    }

    /**
     * Get pois from database cursor.
     * @param cursor cursor
     * @return list of naviterier pois
     */
    public List<Poi> getPoisFromCursor(Cursor cursor) {
        Poi poi;
        List<Poi> pois = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {

                poi = new NaviterierPoi();
                poi.setId(cursor.getString(NaviterierPoiContract.NaviterierPoiTable.POI_ID));
                poi.setName(cursor.getString(NaviterierPoiContract.NaviterierPoiTable.NAME));
                poi.setLatitude(cursor.getDouble(NaviterierPoiContract.NaviterierPoiTable.LAT));
                poi.setLongitude(cursor.getDouble(NaviterierPoiContract.NaviterierPoiTable.LNG));
                poi.setStreet(cursor.getString(NaviterierPoiContract.NaviterierPoiTable.STREET));
                poi.setStreetNumber(cursor.getString(NaviterierPoiContract.NaviterierPoiTable.LAND_REGISTRY_NUMBER));
                poi.setCity(cursor.getString(NaviterierPoiContract.NaviterierPoiTable.CITY));
                poi.setDistance(SphericalUtil.computeDistanceBetween(mSessionData.getLastLocation(), new LatLng(poi.getLatitude(), poi.getLongitude())));
                pois.add(poi);

            } while (cursor.moveToNext());
        }
        return pois;
    }

    /**
     * Get addresses from database cursor.
     * @param cursor cursor
     * @return list of naviterier addresses
     */
    public List<Address> getAddressesFromCursor(Cursor cursor) {
        Address address;
        List<Address> addresses = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {

                address = new Address();
                address.setId(cursor.getString(NaviterierAddressContract.NaviterierAddressTable.POI_ID));
                Point point = new Point();
                point.setLatitude(cursor.getDouble(NaviterierAddressContract.NaviterierAddressTable.LAT));
                point.setLongitude(cursor.getDouble(NaviterierAddressContract.NaviterierAddressTable.LNG));
                address.setPoint(point);
                address.setStreet(cursor.getString(NaviterierAddressContract.NaviterierAddressTable.STREET));
                address.setHouseNumber(cursor.getString(NaviterierAddressContract.NaviterierAddressTable.HOUSE_NUMBER));
                address.setLandRegistryNumber(cursor.getString(NaviterierAddressContract.NaviterierAddressTable.LAND_REGISTRY_NUMBER));
                address.setCity(cursor.getString(NaviterierAddressContract.NaviterierAddressTable.CITY));
                addresses.add(address);

            } while (cursor.moveToNext());
        }
        return addresses;
    }
}
