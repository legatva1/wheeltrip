package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */

public class Address {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Shape")
    @Expose
    private Point point;
    @SerializedName("Street")
    @Expose
    private String street;
    @SerializedName("HouseNumber")
    @Expose
    private String houseNumber;
    @SerializedName("LandRegistryNumber")
    @Expose
    private String landRegistryNumber;
    @SerializedName("City")
    @Expose
    private String city;

    private double distance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getLandRegistryNumber() {
        return landRegistryNumber;
    }

    public void setLandRegistryNumber(String landRegistryNumber) {
        this.landRegistryNumber = landRegistryNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id='" + id + '\'' +
                ", point=" + point +
                ", street='" + street + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", landRegistryNumber='" + landRegistryNumber + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
