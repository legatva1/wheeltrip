
package cz.cvut.fel.dcgi.wheeltrip.model.route;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Difficulty {

    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("point_ref")
    @Expose
    private Integer pointRef;
    @SerializedName("description")
    @Expose
    private String description;

    private LatLng position;

    private boolean isUsed;

    private LatLngBounds bounds;

    /**
     * 
     * @return
     *     The type
     */
    public Integer getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The pointRef
     */
    public Integer getPointRef() {
        return pointRef;
    }

    /**
     * 
     * @param pointRef
     *     The point_ref
     */
    public void setPointRef(Integer pointRef) {
        this.pointRef = pointRef;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    public LatLngBounds getBounds() {
        return bounds;
    }

    public void setBounds(LatLngBounds bounds) {
        this.bounds = bounds;
    }

    @Override
    public String toString() {
        return "Difficulty{" +
                "type=" + type +
                ", pointRef=" + pointRef +
                ", description='" + description + '\'' +
                '}';
    }
}
