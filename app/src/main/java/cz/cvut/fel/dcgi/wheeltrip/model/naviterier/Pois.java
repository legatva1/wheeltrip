package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.model.Poi;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 2. 4. 2016
 */
public class Pois {

    @SerializedName("Pois")
    @Expose
    private List<NaviterierPoi> pois;

    public List<NaviterierPoi> getPois() {
        return pois;
    }

    public void setPois(List<NaviterierPoi> pois) {
        this.pois = pois;
    }

    @Override
    public String toString() {
        return "Pois{" +
                "pois=" + pois +
                '}';
    }
}
