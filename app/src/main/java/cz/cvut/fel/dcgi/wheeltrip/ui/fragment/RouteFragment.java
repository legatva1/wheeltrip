package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.inject.Inject;
import com.squareup.otto.Subscribe;

import java.util.Date;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.RouteSelectedEvent;
import cz.cvut.fel.dcgi.wheeltrip.controller.DirectionsController;
import cz.cvut.fel.dcgi.wheeltrip.controller.MapController;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.ControllerActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.JoystickController;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.NavigationActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.adapter.RoutePagerAdapter;
import roboguice.inject.InjectView;
import roboguice.util.Ln;

/**
 * Fragment that displays routes.
 *
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 30. 12. 2015
 */
public class RouteFragment extends BaseMapFragment implements GoogleMap.OnCameraChangeListener {

    @Inject private MapController mMapController;
    @Inject private DirectionsController mDirectionsController;
    @InjectView(R.id.navigate_button) private AppCompatButton navigateButton;
    @InjectView(R.id.from) private TextView from;
    @InjectView(R.id.to) private TextView to;
    @InjectView(R.id.viewpager) private ViewPager pager;
    @InjectView(R.id.previous) private AppCompatButton previousButton;
    @InjectView(R.id.next) private AppCompatButton nextButton;
    @InjectView(R.id.route_title) private TextView routeTitle;
    @InjectView(R.id.routes_number) private TextView routesNumber;
    @InjectView(R.id.route_description) private AppCompatButton description;
    @InjectView(R.id.route_description_window) private ScrollView descriptionWindow;
    @InjectView(R.id.route_description_text) private TextView descriptionText;

    private boolean zoomedToBounds;
    private boolean routeDescriptionOpened;
    private long backPressed;

    /**
     * Controller callback.
     */
    private JoystickController controller = new JoystickController() {

        @Override
        public void onConfirmEvent() {

            if (routeDescriptionOpened) {
                descriptionWindow.setVisibility(View.GONE);
                routeDescriptionOpened = false;
            }

        }

        @Override
        public void onBackEvent() {
            getActivity().onBackPressed();
        }

        @Override
        public void onUpEvent() {

        }

        @Override
        public void onDownEvent() {
            navigateButton.performClick();
        }

        @Override
        public void onLeftEvent() {
            previousButton.performClick();
        }

        @Override
        public void onRightEvent() {
            nextButton.performClick();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        super.onCreateView(inflater, container, savedState);
        return inflater.inflate(R.layout.fragment_route, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((ControllerActivity) getActivity()).setCallback(controller);

        checkTTS();
        from.setText(sessionData.getRouteData().getOrigin().getTitle());
        to.setText(sessionData.getRouteData().getDestination().getTitle());

        navigateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), NavigationActivity.class));
            }
        });

        pager.setAdapter(new RoutePagerAdapter(getActivity(), sessionData.getRouteData().getRoutes()));

        previousButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem() - 1, true);
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem() + 1, true);

            }
        });

        routesNumber.setText(sessionData.getRouteData().getRoutes().size() == 1 ? sessionData.getRouteData().getRoutes().size() + " trasa" : sessionData.getRouteData().getRoutes().size() + " trasy");

        if (sessionData.getRouteData().getRoutes().size() < 2) {
            previousButton.setEnabled(false);
            nextButton.setEnabled(false);
        }

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                routeTitle.setText("Trasa " + (position + 1));
                if (position == 0) {
                    previousButton.setEnabled(false);
                } else {
                    previousButton.setEnabled(true);
                }

                if (position == sessionData.getRouteData().getRoutes().size() - 1) {
                    nextButton.setEnabled(false);
                } else {
                    nextButton.setEnabled(true);
                }

                mAppBus.post(new RouteSelectedEvent(position));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        sessionData.setSelectedRoute(sessionData.getRouteData().getRoutes().get(sessionData.getSelectedRouteIndex()));
        descriptionText.setText(sessionData.getSelectedRoute().getGeneralDescription());

        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descriptionWindow.setVisibility(View.VISIBLE);
                routeDescriptionOpened = true;
                if (ttsService != null) {
                    ttsService.speakMessageWithNotification(descriptionText.getText().toString());
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (sessionData.isRoutesDrawn()) {
            mAppBus.post(new RouteSelectedEvent(sessionData.getSelectedRouteIndex()));
        }
        if (mMap != null) {
            mMap.clear();
            mMapController.drawRoutes(sessionData.getRouteData().getRoutes(), mMap);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        super.onMapReady(googleMap);
        mMap.setOnCameraChangeListener(this);
        mMapController.drawRoutes(sessionData.getRouteData().getRoutes(), mMap);
        mMapController.drawDifficulties(sessionData.getRouteData().getRoutes().get(sessionData.getSelectedRouteIndex()), mMap);

    }

    @Subscribe
    public void onRouteSelected(RouteSelectedEvent event) {

        for (int i = 0; i < sessionData.getRouteData().getRoutes().size(); i++) {
            if (event.getPosition() == i) {
                sessionData.getRouteData().getRoutes().get(i).getRouteLine().setAsFront(getActivity());
                sessionData.setSelectedRoute(sessionData.getRouteData().getRoutes().get(event.getPosition()));
                sessionData.setSelectedRouteIndex(event.getPosition());
                descriptionText.setText(sessionData.getRouteData().getRoutes().get(i).getGeneralDescription());
            } else {
                sessionData.getRouteData().getRoutes().get(i).getRouteLine().setAsBack(getActivity());
            }
        }

        mMapController.drawDifficulties(sessionData.getRouteData().getRoutes().get(sessionData.getSelectedRouteIndex()), mMap);

    }


    @Override
    protected void createGoogleClient() {
        Ln.d("Do not create google client for RouteFragment.");
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        Ln.d("OnCameraChange(cameraPosition=%s)", cameraPosition.toString());
        if (!zoomedToBounds) {
            mMapController.animateToBounds(mMapController.calculateBounds(sessionData.getRouteData().getRoutes().get(sessionData.getSelectedRouteIndex()).getLatLngs()), mMap);
            zoomedToBounds = true;
        }

    }

    @Override
    public boolean onBackPressed() {
        if (Math.abs(backPressed - new Date().getTime()) < 1000 && backPressed != 0) {
            Ln.d("%s %s", backPressed, new Date().getTime());
            backPressed = new Date().getTime();
            return true;
        }

        backPressed = new Date().getTime();

        if (routeDescriptionOpened) {
            descriptionWindow.setVisibility(View.GONE);
            routeDescriptionOpened = false;
            if (ttsService != null) {
                ttsService.stopPlaying();
            }

            return true;
        }


        return false;
    }


}
