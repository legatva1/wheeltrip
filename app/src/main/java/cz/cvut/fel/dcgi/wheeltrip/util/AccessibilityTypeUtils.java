package cz.cvut.fel.dcgi.wheeltrip.util;

import cz.cvut.fel.dcgi.wheeltrip.R;

/**
 * Accessibility utils.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 27. 12. 2015
 */
public class AccessibilityTypeUtils {


    public static int getAccessibilityTypeLabel(int accessibilityType) {
        switch (accessibilityType) {
            case 1:
                return R.string.attr1_01;
            case 2:
                return R.string.attr1_02;
            case 3:
                return R.string.attr1_03;
            case 4:
                return R.string.attr1_04;
            case 5:
                return R.string.attr1_05;
            case 6:
                return R.string.attr1_06;
            case 7:
                return R.string.attr1_07;
            case 8:
                return R.string.attr1_08;
            case 9:
                return R.string.attr1_09;
            case 10:
                return R.string.attr1_10;
            case 11:
                return R.string.attr1_11;
            default:
                return R.string.attr1_12;
        }
    }
}
