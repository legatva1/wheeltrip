package cz.cvut.fel.dcgi.wheeltrip.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.Arrays;

import cz.cvut.fel.dcgi.wheeltrip.WheelTripApplication;
import roboguice.util.Ln;


/**
 * SharedPreferences utils.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 24. 12. 2015
 */

public class PreferencesUtils {

    private static final String PREFERENCES_NAME = "preference";

    public static final String VOZEJKMAP_DATA_DOWNLOADED = "VOZEJKMAP_DATA_DOWNLOADED";
    public static final String NAVITERIER_POI_DOWNLOADED = "NAVITERIER_POI_DOWNLOADED";
    public static final String NAVITERIER_ADDRESS_DOWNLOADED = "NAVITERIER_ADDRESS_DOWNLOADED";
    public static final String LAST_DATA_UPDATE = "LAST_DATA_UPDATE";

    public static String getString(String key) {
        SharedPreferences sharedPref = WheelTripApplication.get().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPref.getString(key, "");
    }

    public static void putString(String key, String value) {
        SharedPreferences sharedPref = WheelTripApplication.get().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static long getLong(String key) {
        SharedPreferences sharedPref = WheelTripApplication.get().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPref.getLong(key, 0);
    }

    public static void putLong(String key, long value) {
        SharedPreferences sharedPref = WheelTripApplication.get().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        SharedPreferences sharedPref = WheelTripApplication.get().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(key, defaultValue);
    }

    public static void putBoolean(String key, boolean value) {
        SharedPreferences sharedPref = WheelTripApplication.get().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }


    public static boolean[] getFilterSettings() {
        SharedPreferences sharedPref = WheelTripApplication.get().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        String preferences = sharedPref.getString("filterSettings", null);
        if (preferences != null) {
            Ln.d("filterSettings found");
            Gson gson = new Gson();
            return gson.fromJson(preferences, boolean[].class);

        } else {
            Ln.d("filterSettings not found");
            boolean[] filterSettings = new boolean[14];
            Arrays.fill(filterSettings, true);
            return filterSettings;
        }
    }

    public static void saveFilterSettings(boolean[] filterSettings) {
        if (filterSettings == null) {
            return;
        }

        SharedPreferences sharedPref = WheelTripApplication.get().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String settings = gson.toJson(filterSettings, boolean[].class);
        Ln.d("saving filterSettings %s", settings);
        sharedPref.edit().putString("filterSettings", settings).commit();
    }

}
