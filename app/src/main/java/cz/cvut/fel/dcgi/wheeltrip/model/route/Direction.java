package cz.cvut.fel.dcgi.wheeltrip.model.route;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 16. 2. 2016
 */
public enum Direction {
    STRAIGHT(0), LEFT(1), RIGHT(2), SLIGHT_LEFT(3), SLIGHT_RIGHT(4), TURN_AROUND(5),UNKNOWN(6);

    private int type;

    Direction(int type){
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static Direction getByType(int type){
        switch (type){
            case 0:
                return STRAIGHT;
            case 1:
                return LEFT;
            case 2:
                return RIGHT;
            case 3:
                return SLIGHT_LEFT;
            case 4:
                return SLIGHT_RIGHT;
            case 5:
                return TURN_AROUND;
            case 6:
                return UNKNOWN;
            default:
                throw new IllegalArgumentException("Unsupported direction type: "+type);
        }
    }
}
