package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

/**
 * Interface that defines available controller methods.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 12. 1. 2016
 */
public interface JoystickController {

    void onConfirmEvent();

    void onBackEvent();

    void onUpEvent();

    void onDownEvent();

    void onLeftEvent();

    void onRightEvent();

}
