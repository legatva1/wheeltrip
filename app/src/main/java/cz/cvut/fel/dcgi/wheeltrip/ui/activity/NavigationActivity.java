package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import cz.cvut.fel.dcgi.wheeltrip.ui.fragment.NavigationFragment;

/**
 * Activity that contains NavigationFragment.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */

public class NavigationActivity extends UiActivity<NavigationFragment> {

}
