package cz.cvut.fel.dcgi.wheeltrip.controller;

import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.Addresses;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.EmptyRequest;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.Pois;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.RouteData;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.RouteRequest;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.SourceData;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.SourceDataRequest;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Endpoint definitions for NaviTerier.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 23. 3. 2016
 */
public interface NaviterierServiceInterface {

    @Headers("Content-Type: application/json")
    @POST("http://147.32.81.71/NaviTerier.ProcessingService/json/reply/FindSourceData")
    Call<SourceData> getSourceData(@Body SourceDataRequest request);

    @Headers("Content-Type: application/json")
    @POST("http://147.32.81.71/NaviTerier.ProcessingService/json/reply/GetPois")
    Call<Pois> getPois(@Body EmptyRequest body);

    @Headers("Content-Type: application/json")
    @POST("http://147.32.81.71/NaviTerier.ProcessingService/json/reply/GetAddresses")
    Call<Addresses> getAddresses(@Body EmptyRequest body);

    @Headers("Content-Type: application/json")
    @POST("http://147.32.81.71/NaviTerier.ProcessingService/json/reply/FindRoutes")
    Call<RouteData> getFindRoutes(@Body RouteRequest body);
}
