package cz.cvut.fel.dcgi.wheeltrip.controller;

import com.google.inject.Singleton;

import cz.cvut.fel.dcgi.wheeltrip.model.notification.Notification;
import cz.cvut.fel.dcgi.wheeltrip.service.tts.TextToSpeechService;

/**
 * Notification controller.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 20. 4. 2016
 */
@Singleton
public class NotificationController {


    public void playNotification(TextToSpeechService textToSpeechService, Notification notification){
        textToSpeechService.speakMessageWithNotificationCustomPause(notification.getMessage(),500);
    }

}
