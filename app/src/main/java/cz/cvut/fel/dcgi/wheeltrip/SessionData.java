package cz.cvut.fel.dcgi.wheeltrip;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.inject.Singleton;

import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.model.directions.Direction;
import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.PoiEntryPoint;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Route;
import cz.cvut.fel.dcgi.wheeltrip.model.route.RouteData;

/**
 * Used to save users data.
 *
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 22. 1. 2016
 */
@Singleton
public class SessionData {

    private LatLng lastLocation;
    private LatLngBounds screenBounds;
    private boolean requestLocationUpdates;
    private boolean toCurrentPosition;
    private String fromTitle;
    private RouteData routeData;
    private Route selectedRoute;
    private boolean routesDrawn;
    private int selectedRouteIndex;
    private List<PoiEntryPoint> poiEntryPoints;

    private LatLng origin;
    private LatLng destination;

    public String getToTitle() {
        return toTitle;
    }

    public void setToTitle(String toTitle) {
        this.toTitle = toTitle;
    }

    public String getFromTitle() {
        return fromTitle;
    }

    public void setFromTitle(String fromTitle) {
        this.fromTitle = fromTitle;
    }

    private String toTitle;

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    private Direction direction;

    public SessionData() {
        toCurrentPosition = true;
    }

    public LatLng getLastLocation() {
        if (lastLocation == null) {
            return new LatLng(0, 0);
        }
        return lastLocation;
    }

    public void setLastLocation(LatLng lastLocation) {
        this.lastLocation = lastLocation;
    }

    public LatLngBounds getScreenBounds() {
        return screenBounds;
    }

    public void setScreenBounds(LatLngBounds screenBounds) {
        this.screenBounds = screenBounds;
    }

    public boolean isRequestLocationUpdates() {
        return requestLocationUpdates;
    }

    public void setRequestLocationUpdates(boolean requestLocationUpdates) {
        this.requestLocationUpdates = requestLocationUpdates;
    }

    public boolean isToCurrentPosition() {
        return toCurrentPosition;
    }

    public void setToCurrentPosition(boolean toCurrentPosition) {
        this.toCurrentPosition = toCurrentPosition;
    }

    public RouteData getRouteData() {
        return routeData;
    }

    public void setRouteData(RouteData routeData) {
        this.routeData = routeData;
    }

    public Route getSelectedRoute() {
        return selectedRoute;
    }

    public void setSelectedRoute(Route selectedRoute) {
        this.selectedRoute = selectedRoute;
    }

    public boolean isRoutesDrawn() {
        return routesDrawn;
    }

    public void setRoutesDrawn(boolean routesDrawn) {
        this.routesDrawn = routesDrawn;
    }

    public int getSelectedRouteIndex() {
        return selectedRouteIndex;
    }

    public void setSelectedRouteIndex(int selectedRouteIndex) {
        this.selectedRouteIndex = selectedRouteIndex;
    }

    public List<PoiEntryPoint> getPoiEntryPoints() {
        return poiEntryPoints;
    }

    public void setPoiEntryPoints(List<PoiEntryPoint> poiEntryPoints) {
        this.poiEntryPoints = poiEntryPoints;
    }

    public LatLng getOrigin() {
        return origin;
    }

    public void setOrigin(LatLng origin) {
        this.origin = origin;
    }

    public LatLng getDestination() {
        return destination;
    }

    public void setDestination(LatLng destination) {
        this.destination = destination;
    }
}
