package cz.cvut.fel.dcgi.wheeltrip.model.route;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 16. 2. 2016
 */
public enum Obstacle {
    STAIRS(0), BAD_SURFACE(1), STEEP_HILL_UP(2),  STEEP_HILL_DOWN(3), NARROW_ROAD(4);


    private int type;

    Obstacle(int type){
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static Obstacle getByType(int type){
        switch (type){
            case 0:
                return STAIRS;
            case 1:
                return BAD_SURFACE;
            case 2:
                return STEEP_HILL_UP;
            case 3:
                return STEEP_HILL_DOWN;
            case 4:
                return NARROW_ROAD;
            default:
                throw new IllegalArgumentException("Unsupported obstacle type: "+type);
        }
    }
}
