package cz.cvut.fel.dcgi.wheeltrip.service.compass;

import android.content.Context;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.squareup.otto.Subscribe;

import cz.cvut.fel.dcgi.wheeltrip.bus.AppBus;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.BearingEvent;
import roboguice.util.Ln;

/**
 * CompassService is used to compute bearing. Bearing is used to rotate GoogleMap.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 29. 1. 2016
 */
@Singleton
public class CompassService implements SensorEventListener{

    private float[] gravity = new float[3];
    private float[] geomagnetic = new float[3];
    private float[] rotation = new float[9];
    // orientation (azimuth, pitch, roll)
    private float[] orientation = new float[3];

    private float[] smoothed = new float[3];

    private SensorManager sensorManager;

    private Sensor sensorGravity;
    private Sensor sensorMagnetic;

    private GeomagneticField geomagneticField;
    private double bearing = 0;

    @Inject
    private AppBus mAppBus;

    /**
     * Starts listening for sensor changes.
     * @param context application context
     */
    public void startCompass(Context context){
        mAppBus.register(this);
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensorGravity = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMagnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        sensorManager.registerListener(this, sensorGravity,
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorMagnetic,
                SensorManager.SENSOR_DELAY_NORMAL);


    }

    /**
     * Stops listening for sensor changes.
     * @param context application context
     */
    public void stopCompass(Context context){
        sensorManager.unregisterListener(this, sensorGravity);
        sensorManager.unregisterListener(this, sensorMagnetic);
        mAppBus.unregister(this);
    }


    /**
     * Handles sensor changes.
     * @param event new sensor data
     */
    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            smoothed = LowPassFilter.filter(event.values.clone(), gravity);
            gravity[0] = smoothed[0];
            gravity[1] = smoothed[1];
            gravity[2] = smoothed[2];

        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            smoothed = LowPassFilter.filter(event.values.clone(), geomagnetic);
            geomagnetic[0] = smoothed[0];
            geomagnetic[1] = smoothed[1];
            geomagnetic[2] = smoothed[2];
        }

        // get rotation matrix to get gravity and magnetic data
        SensorManager.getRotationMatrix(rotation, null, gravity, geomagnetic);
        // get bearing to target
        SensorManager.getOrientation(rotation, orientation);

        mAppBus.post(new BearingEvent(computeBearingInDegrees(orientation[0])));
    }

    /**
     * Computes bearing in degrees.
     * @param bearing bearing in double format
     * @return bearing in degrees format
     */
    private double computeBearingInDegrees(double bearing){

        // convert from radians to degrees
        bearing = Math.toDegrees(bearing);

        // fix difference between true North and magnetical North
        if (geomagneticField != null) {
            bearing += geomagneticField.getDeclination();
        }

        // bearing must be in 0-360
        if (bearing < 0) {
            bearing += 360;
        }

        return bearing;
    }

    /**
     * Listens for location changes event.
     * @param location new location
     */
    @Subscribe
    public void onLocationFound(Location location) {
        Ln.d("Received Location");

        geomagneticField = new GeomagneticField(
                (float) location.getLatitude(),
                (float) location.getLongitude(),
                (float) location.getAltitude(),
                System.currentTimeMillis());

    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD
                && accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
            Ln.d("Unrealiable data received from %s",sensor.getName());
        }
    }
}
