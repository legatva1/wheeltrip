package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.ControllerActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.adapter.PoiTypeFilterAdapter;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.JoystickController;
import cz.cvut.fel.dcgi.wheeltrip.model.filter.FilterItem;
import cz.cvut.fel.dcgi.wheeltrip.util.ItemClickSupport;
import cz.cvut.fel.dcgi.wheeltrip.util.PreferencesUtils;
import roboguice.inject.InjectView;

/**
 * Filter fragment.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 29. 12. 2015
 */
public class PoiFilterFragment extends BaseFragment {


    @InjectView(R.id.filter_recycler_view) private RecyclerView mRecyclerView;
    @InjectView(R.id.select_all) private AppCompatButton selectAll;
    @InjectView(R.id.unselect_all) private AppCompatButton unselectAll;

    private PoiTypeFilterAdapter mAdapter;
    private boolean[] filterSettings;

    /**
     * Controller callback.
     */
    private JoystickController mCallback = new JoystickController() {

        @Override
        public void onConfirmEvent() {
            clickItem(mAdapter.getSelected());
        }

        @Override
        public void onBackEvent() {
            getActivity().onBackPressed();
        }

        @Override
        public void onUpEvent() {
            mAdapter.setSelected(-1, mRecyclerView);
        }

        @Override
        public void onDownEvent() {
            mAdapter.setSelected(1,mRecyclerView);
        }

        @Override
        public void onLeftEvent() {
            unselectAll.performClick();
        }

        @Override
        public void onRightEvent() {
            selectAll.performClick();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        super.onCreateView(inflater, container, savedState);
        return inflater.inflate(R.layout.fragment_poi_filter, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((ControllerActivity) getActivity()).setCallback(mCallback);

        filterSettings = PreferencesUtils.getFilterSettings();

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);

        List<FilterItem> items = new ArrayList<>();

        String[] titles = getResources().getStringArray(R.array.filter_items);

        for (int i = 0; i < filterSettings.length; i++) {
            FilterItem filterItem = new FilterItem();
            filterItem.setTitle(titles[i]);
            filterItem.setLocationType(i + 1);
            filterItem.setChecked(filterSettings[i]);
            items.add(filterItem);

        }

        mAdapter = new PoiTypeFilterAdapter(items);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).build());

        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
               clickItem(position);

            }
        });

        selectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.selectAll();
                Arrays.fill(filterSettings, true);
            }
        });

        unselectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.unselectAll();
                Arrays.fill(filterSettings, false);
            }
        });

    }

    /**
     * Performs select action on filter item.
     * @param position item position
     */
    private void clickItem(int position){
        mAdapter.setChecked(position);
        mAdapter.setSelectedPosition(position,mRecyclerView);
        filterSettings[position] = !filterSettings[position];
    }

    @Override
    public void onPause() {
        super.onPause();
        PreferencesUtils.saveFilterSettings(filterSettings);
    }

}
