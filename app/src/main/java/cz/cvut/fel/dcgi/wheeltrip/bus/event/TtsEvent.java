package cz.cvut.fel.dcgi.wheeltrip.bus.event;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 27. 4. 2016
 */
public class TtsEvent {

    private String message;

    public TtsEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
