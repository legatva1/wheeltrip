package cz.cvut.fel.dcgi.wheeltrip.model.route;

import com.google.android.gms.maps.GoogleMap;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 16. 2. 2016
 */
public interface RoutePart {

    void drawOnMap(GoogleMap map);

}
