package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import android.content.Intent;

import cz.cvut.fel.dcgi.wheeltrip.ui.fragment.PoiFilterFragment;

/**
 * Activity that contains PoiFilterFragment.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */
public class PoiFilterActivity extends UiActivity<PoiFilterFragment>{

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK, new Intent());
        super.onBackPressed();
    }
}
