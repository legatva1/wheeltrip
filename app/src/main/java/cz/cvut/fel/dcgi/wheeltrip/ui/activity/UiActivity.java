package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import android.os.Bundle;

import java.lang.reflect.ParameterizedType;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.ui.fragment.BaseFragment;
import roboguice.inject.ContentView;

/**
 * Base activity that defines all activities with UI.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 30. 1. 2016
 */

@ContentView(R.layout.activity_base)
public class UiActivity<F extends BaseFragment>
        extends ControllerActivity {

    protected BaseFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragment = createFragmet();
        fragment.setArguments(getIntent().getExtras());
        getFragmentManager().beginTransaction().replace(
                R.id.activity_frame, fragment, fragment.getTag()).commit();
    }

    protected F createFragmet() {
        ParameterizedType superClass =
                (ParameterizedType) getClass().getGenericSuperclass();
        Class<F> type =
                (Class<F>) superClass.getActualTypeArguments()[0];
        try {
            return type.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onBackPressed() {
        if (fragment != null && fragment.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }
}
