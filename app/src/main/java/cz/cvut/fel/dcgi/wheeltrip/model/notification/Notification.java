package cz.cvut.fel.dcgi.wheeltrip.model.notification;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 20. 4. 2016
 */
public enum Notification {

    GO_TO_START("Dostavte se na start trasy."),
    DANGER_OF_FALL("Pozor, nebezpečné naklonění vozíku."),
    DIVERSION_FROM_ROUTE("Nacházíte se mimo trasu."),
    IN_FINISH("Jste v cíli."),
    LOW_BATERY("Nízký stav dobití baterie.");


    private String message;

    Notification(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
