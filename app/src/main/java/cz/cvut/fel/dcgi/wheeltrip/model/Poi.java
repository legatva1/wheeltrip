package cz.cvut.fel.dcgi.wheeltrip.model;

import java.io.Serializable;

/**
 * Poi interface.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 8. 4. 2016
 */
public interface Poi extends Serializable{

    String getId();
    String getName();
    int getType();
    String getDescription();
    double getLatitude();
    double getLongitude();
    int getAccessibilityType();
    String getParkingvailable();
    String getRestroomAvailable();
    String getStreet();
    String getStreetNumber();
    String getCity();
    String getAddress();
    double getDistance();

    void setId(String id);
    void setName(String name);
    void setType(int type);
    void setDescription(String description);
    void setLatitude(double latitude);
    void setLongitude(double longitude);
    void setAccessibilityType(int accessibilityType);
    void setParkingAvailable(String parkingAvailable);
    void setRestroomAvailable(String restroomAvailable);
    void setStreet(String street);
    void setStreetNumber(String streetNumber);
    void setCity(String city);
    void setDistance(double distance);

}
