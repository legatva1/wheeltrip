package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import cz.cvut.fel.dcgi.wheeltrip.ui.fragment.PlaceSearchFragment;

/**
 * Activity that contains PlaceSearchFragment.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */
public class PlaceSearchActivity extends UiActivity<PlaceSearchFragment> {

}
