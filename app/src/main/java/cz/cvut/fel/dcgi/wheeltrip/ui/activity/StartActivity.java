package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.google.inject.Inject;

import cz.cvut.fel.dcgi.wheeltrip.controller.NaviterierController;
import cz.cvut.fel.dcgi.wheeltrip.controller.VozejkMapController;
import cz.cvut.fel.dcgi.wheeltrip.util.PreferencesUtils;
import roboguice.util.Ln;

/**
 * Initial activity that starts data download.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 12. 2015
 */
public final class StartActivity extends BaseActivity {

    @Inject private VozejkMapController mVozejkMapController;
    @Inject private NaviterierController naviterierController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

        if (isWiFi) {
            if (!PreferencesUtils.getBoolean(PreferencesUtils.VOZEJKMAP_DATA_DOWNLOADED, false)) {
                Ln.d("Downloading VozejkMap data.");
                mVozejkMapController.getVozejkMapData();
            }
            if (!PreferencesUtils.getBoolean(PreferencesUtils.NAVITERIER_POI_DOWNLOADED, false)) {
                Ln.d("Downloading Naviterier pois.");
                naviterierController.downloadPois();
            }
            if (!PreferencesUtils.getBoolean(PreferencesUtils.NAVITERIER_ADDRESS_DOWNLOADED, false)) {
                Ln.d("Downloading Naviterier address.");
                naviterierController.downloadAddresses();
            }
        }


        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
        finish();
    }


}
