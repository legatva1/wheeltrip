package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 2. 4. 2016
 */
public class Addresses {

    @SerializedName("Addresses")
    @Expose
    private List<Address> addresses;

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "Addresses{" +
                "addresses=" + addresses +
                '}';
    }
}
