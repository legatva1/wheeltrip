package cz.cvut.fel.dcgi.wheeltrip.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.db.vozejkmap.VozejkMapContract;
import cz.cvut.fel.dcgi.wheeltrip.model.Poi;
import cz.cvut.fel.dcgi.wheeltrip.util.PreferencesUtils;
import cz.cvut.fel.dcgi.wheeltrip.model.vozejkmap.VozejkMapPOI;
import cz.cvut.fel.dcgi.wheeltrip.util.DistanceUtils;
import cz.cvut.fel.dcgi.wheeltrip.util.IconUtils;
import roboguice.util.Ln;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 22. 12. 2015
 */
public class VozejkMapPoiAdapter extends BasePoiAdapter<VozejkMapPOI> {

    public VozejkMapPoiAdapter(Context context, LatLng location) {
        super(context,location);
    }

    private List<Integer> getLocationTypes() {
        List<Integer> filterItems = new ArrayList<>();
        boolean[] filterSettings = PreferencesUtils.getFilterSettings();
        for (int i = 0; i < filterSettings.length; i++) {
            if (filterSettings[i]) {
                filterItems.add(i + 1);
            }
        }
        return filterItems;
    }

    /**
     * Appends location types filter to where selection clause
     *
     * @return where selection clause with location types
     */
    public String appendLocationTypeFilterQuery() {
        StringBuilder selectionBuilder = new StringBuilder();

        List<Integer> filterItems = getLocationTypes();

        if (filterItems.isEmpty()) {
            selectionBuilder.append(VozejkMapContract.PoiEntry.LOCATION_TYPE)
                    .append(" = ")
                    .append(0);
        }

        for (int i = 0; i < filterItems.size(); i++) {
            if (i == 0) {
                selectionBuilder.append("(");
            }
            if (i == filterItems.size() - 1) {
                selectionBuilder.append(VozejkMapContract.PoiEntry.LOCATION_TYPE).append(" = ").append(filterItems.get(i)).append(")");
                break;
            }
            selectionBuilder.append(VozejkMapContract.PoiEntry.LOCATION_TYPE).append(" = ").append(filterItems.get(i)).append(" OR ");
        }

        return selectionBuilder.toString();
    }


    @Override
    public void onBindViewHolder(BasePoiAdapter.ViewHolder holder, int position) {
        super.onBindViewHolder(holder,position);
        holder.mImageView.setVisibility(View.VISIBLE);
        holder.mImageView.setImageResource(IconUtils.getPoiIcon(mItems.get(position).getType()));
    }

    @Override
    protected String getItemLabel(int position) {
        return mItems.get(position).getName();
    }

    @Override
    protected double getDistance(int position) {
        return mItems.get(position).getDistance();
    }

    @Override
    protected Comparator<VozejkMapPOI> getComparator() {
        return new Comparator<VozejkMapPOI>() {
            @Override
            public int compare(VozejkMapPOI lhs, VozejkMapPOI rhs) {
                if (lhs.getDistance() > rhs.getDistance()) {
                    return 1;
                } else if (lhs.getDistance() < rhs.getDistance()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
    }

    @Override
    protected VozejkMapPOI loadDataFromCursor(Cursor cursor, VozejkMapPOI item) {
        item = new VozejkMapPOI();
        item.setName(cursor.getString(VozejkMapContract.VozejkMapTable.TITLE));
        item.setType(cursor.getInt(VozejkMapContract.VozejkMapTable.LOCATION_TYPE));
        item.setDescription(cursor.getString(VozejkMapContract.VozejkMapTable.DESCRIPTION));
        item.setLatitude(cursor.getDouble(VozejkMapContract.VozejkMapTable.LAT));
        item.setLongitude(cursor.getDouble(VozejkMapContract.VozejkMapTable.LNG));
        item.setAccessibilityType(cursor.getInt(VozejkMapContract.VozejkMapTable.ATTR1));
        item.setRestroomAvailable(cursor.getString(VozejkMapContract.VozejkMapTable.ATTR2));
        item.setParkingAvailable(cursor.getString(VozejkMapContract.VozejkMapTable.ATTR3));
        item.setDistance(SphericalUtil.computeDistanceBetween(mLocation, new LatLng(item.getLatitude(), item.getLongitude())));
        return item;
    }

    @Override
    protected Uri getUri() {
        return VozejkMapContract.PoiEntry.CONTENT_URI;
    }

    @Override
    protected String[] getProjection() {
        return VozejkMapContract.PoiEntry.POI_COLUMNS;
    }

    @Override
    protected String getAppendQuery() {
        return appendLocationTypeFilterQuery();
    }
}
