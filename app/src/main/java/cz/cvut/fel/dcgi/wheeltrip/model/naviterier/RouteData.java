
package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RouteData {

    @SerializedName("Routes")
    @Expose
    private List<Route> routes = new ArrayList<Route>();
    @SerializedName("SourceAddress")
    @Expose
    private Address originAddress;
    @SerializedName("SourcePoi")
    @Expose
    private NaviterierPoi originPoi;
    @SerializedName("TargetAddress")
    @Expose
    private Address destinationAddress;
    @SerializedName("TargetPoi")
    @Expose
    private NaviterierPoi destinationPoi;
    @SerializedName("MapVersion")
    @Expose
    private String mapVersion;

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public Address getOriginAddress() {
        return originAddress;
    }

    public void setOriginAddress(Address originAddress) {
        this.originAddress = originAddress;
    }

    public NaviterierPoi getOriginPoi() {
        return originPoi;
    }

    public void setOriginPoi(NaviterierPoi originPoi) {
        this.originPoi = originPoi;
    }

    public Address getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(Address destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public NaviterierPoi getDestinationPoi() {
        return destinationPoi;
    }

    public void setDestinationPoi(NaviterierPoi destinationPoi) {
        this.destinationPoi = destinationPoi;
    }

    public String getMapVersion() {
        return mapVersion;
    }

    public void setMapVersion(String mapVersion) {
        this.mapVersion = mapVersion;
    }

    @Override
    public String toString() {
        return "RouteData{" +
                "routes=" + routes +
                ", originAddress=" + originAddress +
                ", originPoi=" + originPoi +
                ", destinationAddress=" + destinationAddress +
                ", destinationPoi=" + destinationPoi +
                ", mapVersion='" + mapVersion + '\'' +
                '}';
    }
}
