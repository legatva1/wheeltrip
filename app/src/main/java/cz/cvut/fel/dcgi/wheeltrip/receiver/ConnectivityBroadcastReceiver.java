package cz.cvut.fel.dcgi.wheeltrip.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Receives internet connection changes.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 11. 3. 2016
 */
public class ConnectivityBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")){
            ConnectivityManager cm =
                    (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if(activeNetwork ==null){
                return;
            }

            boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
            boolean isMobile = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;

            if(isWiFi||isMobile){
                Toast.makeText(context,"",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(context,"Chybí připojení k internetu.",Toast.LENGTH_SHORT).show();
            }

        }

    }
}
