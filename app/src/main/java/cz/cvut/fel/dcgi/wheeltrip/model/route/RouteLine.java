package cz.cvut.fel.dcgi.wheeltrip.model.route;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.WheelTripApplication;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 1. 2. 2016
 */
public class RouteLine implements RoutePart{


    private float backWidth;
    private float frontWidth;
    private int zIndex;
    private Polyline backLine;
    private Polyline frontLine;
    private String id;
    private boolean isFront;

    private List<LatLng> points;


    public String getId() {
        return id;
    }

    public float getBackWidth() {
        return backWidth;
    }

    public RouteLine setBackWidth(float backWidth) {
        this.backWidth = backWidth;
        return this;
    }

    public float getFrontWidth() {
        return frontWidth;
    }

    public RouteLine setFrontWidth(float frontWidth) {
        this.frontWidth = frontWidth;
        return this;
    }

    public int getBackColor(boolean isFront, Context context) {
        return isFront?context.getResources().getColor(R.color.route_color_bottom):context.getResources().getColor(R.color.route_color_bottom_disabled);
    }



    public int getFrontColor(boolean isFront, Context context) {
        return isFront?context.getResources().getColor(R.color.route_color):context.getResources().getColor(R.color.route_color_disabled);
    }



    public List<LatLng> getPoints() {
        return points;
    }

    public RouteLine setPoints(List<LatLng> points) {
        this.points = points;
        return this;
    }

    public int getzIndex() {
        return zIndex;
    }

    public RouteLine setzIndex(int zIndex) {
        this.zIndex = zIndex;
        return this;
    }

    public Polyline getBackLine() {
        return backLine;
    }

    public void setBackLine(Polyline backLine) {
        this.backLine = backLine;
    }

    public Polyline getFrontLine() {
        return frontLine;
    }

    public void setFrontLine(Polyline frontLine) {
        this.id=frontLine.getId();
        this.frontLine = frontLine;
    }

    public void setAsFront(Context context){
        frontLine.setColor(context.getResources().getColor(R.color.route_color));
        backLine.setColor(context.getResources().getColor(R.color.route_color_bottom));
        backLine.setZIndex(1);
        frontLine.setZIndex(1);
        isFront = true;

    }

    public void setAsBack(Context context){
        frontLine.setColor(context.getResources().getColor(R.color.route_color_disabled));
        backLine.setColor(context.getResources().getColor(R.color.route_color_bottom_disabled));
        backLine.setZIndex(0);
        frontLine.setZIndex(0);
        isFront = false;
    }

    public boolean isFront() {
        return isFront;
    }

    public void setFront(boolean front) {
        isFront = front;
    }

    @Override
    public void drawOnMap(GoogleMap map) {
        PolylineOptions lineBack = new PolylineOptions().addAll(points)
                .color(getBackColor(isFront, WheelTripApplication.get())).width(getBackWidth()).zIndex(isFront ? 1 : 0);

        PolylineOptions lineFront = new PolylineOptions().addAll(points)
                .color(getFrontColor(isFront, WheelTripApplication.get())).width(getFrontWidth()).zIndex(isFront ? 1 : 0);

        backLine = map.addPolyline(lineBack);
        frontLine = map.addPolyline(lineFront);
    }
}
