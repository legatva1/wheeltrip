package cz.cvut.fel.dcgi.wheeltrip.controller;

import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.model.vozejkmap.VozejkMapPOI;
import retrofit.Call;
import retrofit.http.GET;

/**
 * Endpoint definitions for VozejkMap.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 20. 12. 2015
 */
public interface VozejkMapServiceInterface {

    @GET("/opendata/locations.json?key=YmdvM3NuNTYydTggY29")
    Call<List<VozejkMapPOI>> getVozejkMapPOIs();
}
