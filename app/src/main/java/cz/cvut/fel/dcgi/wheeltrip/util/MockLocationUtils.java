package cz.cvut.fel.dcgi.wheeltrip.util;

import android.location.Location;
import android.os.Build;
import android.os.SystemClock;

import com.google.android.gms.maps.model.LatLng;
import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper for use of mock location.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 16. 2. 2016
 */
@Singleton
public class MockLocationUtils {

    private static final String PROVIDER = "flp";
    private static final float ACCURACY = 3.0f;
    private static List<LatLng> mockPositions;

    private static Location createLocation(LatLng position) {
        long elapsedTimeNanos = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            elapsedTimeNanos = SystemClock.elapsedRealtimeNanos();
        }
        Location newLocation = new Location(PROVIDER);
        newLocation.setLatitude(position.latitude);
        newLocation.setLongitude(position.longitude);
        newLocation.setAccuracy(ACCURACY);
        newLocation.setTime(System.currentTimeMillis());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            newLocation.setElapsedRealtimeNanos(elapsedTimeNanos);
        }
        return newLocation;
    }

    public static void fillMockPositions(List<LatLng> positions) {
        mockPositions = new ArrayList<>();
        for (LatLng position : positions) {
            mockPositions.add(position);
        }

    }

    public static Location getMockLocation() {
        return createLocation(mockPositions.remove(0));
    }

    public static boolean hasNext() {
        return !mockPositions.isEmpty();
    }


}
