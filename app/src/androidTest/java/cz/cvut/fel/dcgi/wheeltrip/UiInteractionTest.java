package cz.cvut.fel.dcgi.wheeltrip;

import android.support.test.InstrumentationRegistry;
import android.test.ActivityInstrumentationTestCase2;
import android.view.KeyEvent;

import org.junit.Before;
import org.junit.Test;

import cz.cvut.fel.dcgi.wheeltrip.ui.activity.MapsActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 3. 4. 2016
 * run with gradlew.bat createDebugCoverageReport
 */
public class UiInteractionTest
        extends ActivityInstrumentationTestCase2<MapsActivity> {

    private MapsActivity mActivity;

    public UiInteractionTest() {
        super(MapsActivity.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        mActivity = getActivity();
    }

    @Test
    public void test1ApplicationWalkthrough() throws InterruptedException {
        Thread.sleep(3000);
        //settings
        onView(withId(R.id.settings_button)).check(matches(isDisplayed()));
        sendKeys(KeyEvent.KEYCODE_DPAD_RIGHT);
        onView(withId(R.id.settings_button)).check(doesNotExist());
        sendKeys(KeyEvent.KEYCODE_BACK);
        onView(withId(R.id.poi_filter_button)).check(matches(isDisplayed()));
        //filter
        sendKeys(KeyEvent.KEYCODE_DPAD_UP);
        onView(withId(R.id.select_all)).check(matches(isDisplayed()));
        sendKeys(KeyEvent.KEYCODE_DPAD_LEFT);
        Thread.sleep(1000);
        sendKeys(KeyEvent.KEYCODE_DPAD_RIGHT);
        Thread.sleep(2000);
        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        Thread.sleep(1000);
        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        Thread.sleep(1000);
        sendKeys(KeyEvent.KEYCODE_BACK);
        onView(withId(R.id.route_button)).check(matches(isDisplayed()));
        //route button
        sendKeys(KeyEvent.KEYCODE_DPAD_LEFT);
        onView(withId(R.id.to_button)).check(matches(isDisplayed()));
        //set destination
        sendKeys(KeyEvent.KEYCODE_DPAD_RIGHT);
        Thread.sleep(3000);
        onView(withId(R.id.edittext_places)).perform(typeText("r"));
        onView(withId(R.id.edittext_places)).perform(typeText("e"));
        onView(withId(R.id.edittext_places)).perform(typeText("t"));
        onView(withId(R.id.edittext_places)).perform(typeText("r"));
        onView(withId(R.id.edittext_places)).perform(typeText("o"));
        Thread.sleep(2000);
        sendKeys(KeyEvent.KEYCODE_ENTER);
        Thread.sleep(1000);
        onView(withId(R.id.find_route_button)).check(matches(isDisplayed()));
        //find route
        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        Thread.sleep(5000);
        onView(withId(R.id.navigate_button)).check(matches(isDisplayed()));
        //navigate
        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        Thread.sleep(5000);
        //hide menu
        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        Thread.sleep(3000);
        //open menu
        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        Thread.sleep(3000);
        onView(withId(R.id.cannot_finish_step)).check(matches(isDisplayed()));
        sendKeys(KeyEvent.KEYCODE_DPAD_LEFT);
        Thread.sleep(1000);
        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        sendKeys(KeyEvent.KEYCODE_BACK);
        onView(withId(R.id.end_navigation)).check(matches(isDisplayed()));
        sendKeys(KeyEvent.KEYCODE_DPAD_RIGHT);
        Thread.sleep(3000);
        onView(withId(R.id.route_button)).check(matches(isDisplayed()));

    }

    @Test
    public void test2ApplicationWalkthrough() throws InterruptedException {
        Thread.sleep(3000);
        //hide menu
        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        Thread.sleep(3000);
        onView(withId(R.id.map_mode_button)).check(matches(isDisplayed()));
        //map mode
        sendKeys(KeyEvent.KEYCODE_DPAD_LEFT);
        Thread.sleep(3000);
        sendKeys(KeyEvent.KEYCODE_ENTER);
        Thread.sleep(3000);
        onView(withId(R.id.streetview)).check(matches(isDisplayed()));
        sendKeys(KeyEvent.KEYCODE_DPAD_LEFT);
        sendKeys(KeyEvent.KEYCODE_BACK);
        sendKeys(KeyEvent.KEYCODE_BACK);
        Thread.sleep(3000);
    }

    @Test
    public void test3ApplicationWalkthrough() throws InterruptedException {
        Thread.sleep(3000);
        //hide menu
        sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        Thread.sleep(3000);
        onView(withId(R.id.search_button)).check(matches(isDisplayed()));
        //map mode
        sendKeys(KeyEvent.KEYCODE_DPAD_RIGHT);
        Thread.sleep(3000);
        onView(withId(R.id.edittext_places)).perform(typeText("r"));
        onView(withId(R.id.edittext_places)).perform(typeText("e"));
        onView(withId(R.id.edittext_places)).perform(typeText("t"));
        onView(withId(R.id.edittext_places)).perform(typeText("r"));
        onView(withId(R.id.edittext_places)).perform(typeText("o"));
        Thread.sleep(2000);
        sendKeys(KeyEvent.KEYCODE_ENTER);
        Thread.sleep(1000);
        onView(withId(R.id.navigate)).check(matches(isDisplayed()));
        Thread.sleep(1000);
        sendKeys(KeyEvent.KEYCODE_DPAD_RIGHT);
        Thread.sleep(5000);
        onView(withId(R.id.navigate_button)).check(matches(isDisplayed()));
        sendKeys(KeyEvent.KEYCODE_DPAD_RIGHT);
        Thread.sleep(1000);
        sendKeys(KeyEvent.KEYCODE_DPAD_RIGHT);
        Thread.sleep(1000);
        sendKeys(KeyEvent.KEYCODE_DPAD_LEFT);
        Thread.sleep(1000);
        sendKeys(KeyEvent.KEYCODE_DPAD_LEFT);
    }
}
