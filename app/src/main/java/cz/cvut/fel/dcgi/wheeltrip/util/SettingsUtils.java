package cz.cvut.fel.dcgi.wheeltrip.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import cz.cvut.fel.dcgi.wheeltrip.R;

/**
 * App preferences utila.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 14. 2. 2016
 */
public class SettingsUtils {

    public static boolean isMockingEnabled(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(context.getString(R.string.mock_data), false);
    }

    public static boolean isVozejkmapDataEnabled(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(context.getString(R.string.vozejkmap_data), true);
    }

    public static boolean isNaviterierDataEnabled(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(context.getString(R.string.naviterier_data), false);
    }

    public static boolean isPlacesDataEnabled(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(context.getString(R.string.places_data), false);
    }

    public static boolean isTtsEnabled(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(context.getString(R.string.tts_enabled), true);
    }


    public static boolean isUSBJoystickEnabled(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(context.getString(R.string.usb_joystick), false);
    }

    public static int getWheelchairMaxSpped(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(context.getString(R.string.wheelchair_max_speed), 2);
    }

    public static int getWheelchairAcceleration(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(context.getString(R.string.wheelchair_acceleration), 1);
    }

}
