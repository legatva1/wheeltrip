
package cz.cvut.fel.dcgi.wheeltrip.model.route;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Destination {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("latLng")
    @Expose
    private LatLng latLng;

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The latLng
     */
    public LatLng getLatLng() {
        return latLng;
    }

    /**
     * 
     * @param latLng
     *     The latLng
     */
    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    @Override
    public String toString() {
        return "Destination{" +
                "title='" + title + '\'' +
                ", latLng=" + latLng +
                '}';
    }
}
