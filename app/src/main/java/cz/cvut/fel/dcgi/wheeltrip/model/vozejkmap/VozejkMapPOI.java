package cz.cvut.fel.dcgi.wheeltrip.model.vozejkmap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import cz.cvut.fel.dcgi.wheeltrip.model.Poi;
import cz.cvut.fel.dcgi.wheeltrip.model.PoiImpl;
import cz.cvut.fel.dcgi.wheeltrip.util.AddressUtils;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 20. 12. 2015
 */

public class VozejkMapPOI extends PoiImpl{

    @SerializedName("location_type")
    @Expose
    private int type;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("attr1")
    @Expose
    private int accessibilityType;
    @SerializedName("attr2")
    @Expose
    private String restroomAvailable;
    @SerializedName("attr3")
    @Expose
    private String parkingAvailable;

    @Override
    public int getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public int getAccessibilityType() {
        return accessibilityType;
    }

    @Override
    public String getParkingvailable() {
        return parkingAvailable;
    }

    @Override
    public String getRestroomAvailable() {
        return restroomAvailable;
    }

    @Override
    public void setType(int type) {
        this.type = type;
    }
    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void setAccessibilityType(int accessibilityType) {
        this.accessibilityType = accessibilityType;
    }
    @Override
    public void setRestroomAvailable(String restroomAvailable) {
        this.restroomAvailable = restroomAvailable;
    }
    @Override
    public void setParkingAvailable(String parkingAvailable) {
        this.parkingAvailable = parkingAvailable;
    }
}