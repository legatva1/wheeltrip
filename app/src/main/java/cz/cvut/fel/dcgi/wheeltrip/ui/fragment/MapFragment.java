package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.inject.Inject;
import com.squareup.otto.Subscribe;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.AddMarkersEvent;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.LocationFoundEvent;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.PlaceSelectedEvent;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.VozejkMapDataDownloadedEvent;
import cz.cvut.fel.dcgi.wheeltrip.controller.DirectionsController;
import cz.cvut.fel.dcgi.wheeltrip.controller.MapController;
import cz.cvut.fel.dcgi.wheeltrip.controller.VozejkMapController;
import cz.cvut.fel.dcgi.wheeltrip.model.Poi;
import cz.cvut.fel.dcgi.wheeltrip.model.vozejkmap.VozejkMapPOI;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.ControllerActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.JoystickController;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.MapChooserActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.MapsActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.PlaceSearchActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.PoiFilterActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.RouteSearchActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.SettingsActivity;
import cz.cvut.fel.dcgi.wheeltrip.ui.activity.StreetViewActivity;
import cz.cvut.fel.dcgi.wheeltrip.util.AccessibilityTypeUtils;
import cz.cvut.fel.dcgi.wheeltrip.util.AddressUtils;
import cz.cvut.fel.dcgi.wheeltrip.util.Constants;
import cz.cvut.fel.dcgi.wheeltrip.util.IconUtils;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
import roboguice.inject.InjectView;
import roboguice.util.Ln;

/**
 * Map fragment. Main screen of the WheelTrip app.
 *
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 22. 12. 2015
 */
public class MapFragment extends BaseMapFragment implements
        GoogleMap.OnCameraChangeListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    @Inject private VozejkMapController mVozejkMapController;
    @Inject private MapController mMapController;
    @Inject private DirectionsController mDirectionsController;

    @InjectView(R.id.poi_detail) private CardView poiDetail;
    @InjectView(R.id.detail_title) private TextView detailTitle;
    @InjectView(R.id.detail_icon) private ImageView detailIcon;
    @InjectView(R.id.detail_accessibility) private TextView detailAccessibility;
    @InjectView(R.id.detail_street) private TextView detailStreet;
    @InjectView(R.id.detail_restroom) private ImageView detailRestroom;
    @InjectView(R.id.detail_parking) private ImageView detailParking;
    @InjectView(R.id.restroom_title) private TextView restroomTitle;
    @InjectView(R.id.parking_title) private TextView parkingTitle;
    @InjectView(R.id.poi_filter_button) private AppCompatButton poiFilter;
    @InjectView(R.id.settings_button) private AppCompatButton settingsButton;
    @InjectView(R.id.route_button) private AppCompatButton routeSearch;
    @InjectView(R.id.search_button) private Button searchButton;
    @InjectView(R.id.map_mode_button) private Button mapButton;
    @InjectView(R.id.streetview) private AppCompatButton streetViewButton;
    @InjectView(R.id.navigate) private AppCompatButton navigate;
    @InjectView(R.id.menuContainer) private CardView menu;
    @InjectView(R.id.detail_description) private AppCompatButton description;
    @InjectView(R.id.description_window) private ScrollView descriptionWindow;
    @InjectView(R.id.description_text) private TextView descriptionText;

    private long backPressed;
    private boolean poiDetailOpened;
    private LatLng selectedPoi;
    private LatLng lastPosition;
    private boolean isMenuOpened;
    private boolean isMenuAnimationInProgress;
    private boolean poiDescriptionOpened;
    private Marker placeMarker;
    private boolean poiDetailAnimation;


    /**
     * Controller callback.
     */
    private JoystickController controller = new JoystickController() {

        @Override
        public void onConfirmEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }
            if (poiDescriptionOpened) {
                descriptionWindow.setVisibility(View.GONE);
                poiDescriptionOpened = false;
                if (ttsService != null) {
                    ttsService.stopPlaying();
                }
                return;
            }
        }

        @Override
        public void onBackEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }

            getActivity().onBackPressed();
        }

        @Override
        public void onUpEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }

            if (poiDetailOpened) {
                description.performClick();
                return;
            }

            if (isMenuOpened) {
                poiFilter.performClick();
            }
        }

        @Override
        public void onDownEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }

            menu.performClick();
        }

        @Override
        public void onLeftEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }
            if (poiDetailOpened) {
                streetViewButton.performClick();
                return;
            }

            if (isMenuOpened) {
                routeSearch.performClick();
            } else {
                mapButton.performClick();
            }

        }

        @Override
        public void onRightEvent() {
            if (isMenuAnimationInProgress) {
                return;
            }

            if (poiDetailOpened) {
                navigate.performClick();
                return;
            }

            if (isMenuOpened) {
                settingsButton.performClick();
            } else {
                searchButton.performClick();
            }
        }
    };

    /**
     * Adds marker to the map.
     *
     * @param position location of marker
     */
    private void addMarker(LatLng position) {
        if (placeMarker == null) {
            placeMarker = mMapController.drawMarker(position, mMap);
        } else {
            placeMarker.remove();
            placeMarker = mMapController.drawMarker(position, mMap);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((ControllerActivity) getActivity()).setCallback(controller);
        checkTTS();

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), PlaceSearchActivity.class), Constants.POI_REQUEST);
            }
        });

        poiFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), PoiFilterActivity.class), Constants.FILTER_REQUEST);
            }
        });

        routeSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), RouteSearchActivity.class));
            }
        });

        streetViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), StreetViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(StreetViewActivity.STREETVIEW_POSITION, selectedPoi);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapChooserActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.TARGET, mMap.getCameraPosition().target);
                intent.putExtras(bundle);

                startActivityForResult(intent, Constants.MAP_REQUEST);

            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isMenuOpened) {
                    disableDrawableArrows();
                } else {
                    enableDrawableArrows();
                }

                if (!isMenuAnimationInProgress) {
                    createMenuAnimation().start();
                }


            }
        });

        isMenuOpened = true;
        disableDrawableArrows();

        navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionData.getLastLocation() == null) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.enter_origin), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (selectedPoi == null) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.enter_destination), Toast.LENGTH_SHORT).show();
                    return;
                }

                sessionData.setFromTitle(getActivity().getResources().getString(R.string.from_default_title));
                sessionData.setToTitle(detailTitle.getText().toString());
                mDirectionsController.getDirections(sessionData.getLastLocation(), selectedPoi, getActivity());
            }
        });

        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descriptionWindow.setVisibility(View.VISIBLE);
                poiDescriptionOpened = true;

                if (ttsService != null) {
                    ttsService.speakMessageWithNotification(descriptionText.getText().toString());
                }
            }
        });

    }

    /**
     * Draw arrows as disabled.
     */
    private void disableDrawableArrows() {
        searchButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getActivity().getResources().getDrawable(R.drawable.ic_right_arrow_disabled), null);
        mapButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getActivity().getResources().getDrawable(R.drawable.ic_left_arrow_disabled), null);
    }

    /**
     * Draw arrows as enabled.
     */
    private void enableDrawableArrows() {
        searchButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getActivity().getResources().getDrawable(R.drawable.ic_right_arrow_dark), null);
        mapButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getActivity().getResources().getDrawable(R.drawable.ic_left_arrow_dark), null);
    }


    /**
     * Creates menu animation.
     *
     * @return animator object
     */
    private ObjectAnimator createMenuAnimation() {
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, r.getDisplayMetrics());
        ObjectAnimator menuAnimation = ObjectAnimator.ofFloat(
                menu,
                View.TRANSLATION_X,
                isMenuOpened ? 0f : -menu.getWidth() + px,
                isMenuOpened ? -menu.getWidth() + px : 0f);

        menuAnimation.setDuration(1000);
        menuAnimation.setInterpolator(new DecelerateInterpolator(1.5f));
        menuAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                isMenuAnimationInProgress = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {

                isMenuOpened = !isMenuOpened;
                isMenuAnimationInProgress = false;

            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        return menuAnimation;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == MapsActivity.RESULT_OK) {
            if (requestCode == Constants.POI_REQUEST || requestCode == Constants.MAP_REQUEST) {
                Poi poi = (Poi) data.getSerializableExtra(Constants.POI);
                selectedPoi = new LatLng(poi.getLatitude(), poi.getLongitude());
                lastPosition = null;
                Ln.d("place selected result %s", selectedPoi);
                setDetailData(poi);
                sessionData.setToCurrentPosition(false);
                addMarker(selectedPoi);
                createPoiDetailAnimation().start();
            }

            if (requestCode == Constants.FILTER_REQUEST) {
                mAppBus.post(new AddMarkersEvent());
            }

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        super.onMapReady(googleMap);
        Ln.d("onMapReady");
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
        onResume();
        mAppBus.post(new AddMarkersEvent());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap != null) {
            if (lastPosition != null) {
                mMapController.animateCameraToPosition(lastPosition, mMap);
                return;
            }
            if (selectedPoi == null) {
                Ln.d("move to current position");
                mMapController.animateCameraToPosition(sessionData.getLastLocation(), mMap);
            } else {
                Ln.d("move to selected poi");
                mMapController.animateCameraToPosition(selectedPoi, mMap);
            }
        } else {
            Ln.d("mMap is null");
        }
    }


    @Override
    public boolean onBackPressed() {

        if (Math.abs(backPressed - new Date().getTime()) < 1000 && backPressed != 0) {
            Ln.d("%s %s", backPressed, new Date().getTime());
            backPressed = new Date().getTime();
            return true;
        }

        backPressed = new Date().getTime();

        if (poiDescriptionOpened) {
            descriptionWindow.setVisibility(View.GONE);
            poiDescriptionOpened = false;
            if (ttsService != null) {
                ttsService.stopPlaying();
            }
            return true;
        }

        if (poiDetailAnimation) {
            return true;
        }

        if (poiDetailOpened) {
            hidePoiDetail();
            return true;
        }


        return false;
    }


    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        Ln.d("OnCameraChange(cameraPosition=%s)", cameraPosition.toString());
        LatLngBounds mScreenBounds = mMapController.getVisibleAreaBounds(mMap);

        if (cameraPosition.zoom < 17) {
            mScreenBounds = mMapController.convertCenterAndRadiusToBounds(sessionData.getLastLocation(), 1000);
            sessionData.setScreenBounds(mScreenBounds);
        } else {
            sessionData.setScreenBounds(mScreenBounds);
            mAppBus.post(new AddMarkersEvent());
        }

        lastPosition = cameraPosition.target;

    }

    /**
     * Sets poi detail data.
     *
     * @param poi poi with data to set
     */
    private void setDetailData(Poi poi) {
        Ln.d("setting detail %s", poi);
        detailTitle.setText(poi.getName());
        detailIcon.setImageResource(IconUtils.getPoiIcon(poi.getType()));
        detailAccessibility.setText(getString(AccessibilityTypeUtils.getAccessibilityTypeLabel(poi.getAccessibilityType())));
        detailRestroom.setImageResource(poi.getRestroomAvailable().equals(Constants.YES) ? R.drawable.ic_check_ok : R.drawable.ic_cross);
        detailParking.setImageResource(poi.getParkingvailable().equals(Constants.YES) ? R.drawable.ic_check_ok : R.drawable.ic_cross);
        detailStreet.setText(AddressUtils.getAddressFromPoi(poi));
        detailStreet.setVisibility(StringUtils.isEmpty(detailStreet.getText()) ? View.GONE : View.VISIBLE);

        if (poi.getType() == -1) {
            detailIcon.setVisibility(View.GONE);
            detailRestroom.setVisibility(View.GONE);
            detailParking.setVisibility(View.GONE);
            restroomTitle.setVisibility(View.GONE);
            parkingTitle.setVisibility(View.GONE);
            detailAccessibility.setVisibility(View.GONE);
            description.setVisibility(View.GONE);
        } else {
            detailIcon.setVisibility(View.VISIBLE);
            detailRestroom.setVisibility(View.VISIBLE);
            detailParking.setVisibility(View.VISIBLE);
            restroomTitle.setVisibility(View.VISIBLE);
            parkingTitle.setVisibility(View.VISIBLE);
            detailAccessibility.setVisibility(View.VISIBLE);
            description.setVisibility(View.VISIBLE);
            descriptionText.setText(poi.getDescription());
        }
    }

    /**
     * Creates poi detail animation.
     *
     * @return animator object
     */
    private ObjectAnimator createPoiDetailAnimation() {

        ObjectAnimator anim = ObjectAnimator.ofFloat(poiDetail, View.TRANSLATION_Y, -500f, 0f);
        anim.setDuration(500);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                poiDetail.setVisibility(View.VISIBLE);
                poiDetailAnimation = true;

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                poiDetailOpened = true;
                poiDetailAnimation = false;
                if (isMenuOpened) {
                    createMenuAnimation().start();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        return anim;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (StringUtils.isEmpty(marker.getTitle())) {
            return true;
        }
        setDetailData(mVozejkMapController.loadDataFromCursor(marker));
        selectedPoi = marker.getPosition();
        createPoiDetailAnimation().start();
        mMapController.animateCameraToPosition(marker.getPosition(), mMap);
        return true;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (poiDetailOpened) {
            hidePoiDetail();
            return;
        }

        if (isMenuOpened) {
            createMenuAnimation().start();
        }
    }

    @Override
    protected void setMockMode() {
        Ln.d("Do not set mock mode in MapFragment");
    }

    /**
     * Hides poi detail.
     */
    private void hidePoiDetail() {
        ObjectAnimator anim = ObjectAnimator.ofFloat(poiDetail, View.TRANSLATION_Y, 0f, -500f);
        anim.setDuration(500);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                poiDetailOpened = false;
                poiDetail.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        anim.start();
    }

    /**
     * Task that adds VozejkMap poi markers on the map.
     */
    private class VozejMapPOIAsyncTask extends AsyncTask<Void, List<VozejkMapPOI>, List<VozejkMapPOI>> {

        @Override
        protected List<VozejkMapPOI> doInBackground(Void... strings) {
            return mVozejkMapController.getPOIs();
        }

        @Override
        protected void onPostExecute(List<VozejkMapPOI> pois) {
            super.onPostExecute(pois);
            mMapController.addMarkers(pois, mMap);
        }
    }


    //Subscribers
    @Subscribe
    public void onVozejkMapDataDownload(VozejkMapDataDownloadedEvent event) {
        Ln.d("Received VozejkMapDataDownloadedEvent");
        mAppBus.post(new AddMarkersEvent());
    }


    @Subscribe
    public void onPlaceSelected(PlaceSelectedEvent event) {
        Ln.d("Received PlaceSelectedEvent");

        if (event.getLocation() == null) {
            Ln.d("place select poi is null");
            return;
        }

        mRequestLocationUpdates = false;
        Ln.d("onCreate mRequestLocationUpdates false");

    }

    @Subscribe
    public void onLocationFound(LocationFoundEvent event) {
        Ln.d("Received LocationFoundEvent");

        if (sessionData.isToCurrentPosition()) {
            mMapController.moveCameraToPosition(currentLocation, mMap);
            sessionData.setToCurrentPosition(false);
        }

    }

    @Subscribe
    public void onAddMarkersEvent(AddMarkersEvent event) {
        new VozejMapPOIAsyncTask().execute();
    }
}
