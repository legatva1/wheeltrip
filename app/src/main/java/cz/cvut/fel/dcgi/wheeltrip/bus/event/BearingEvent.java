package cz.cvut.fel.dcgi.wheeltrip.bus.event;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 29. 1. 2016
 */
public class BearingEvent {

    private double bearing;

    public BearingEvent(double bearing) {
        this.bearing = bearing;
    }

    public double getBearing() {
        return bearing;
    }
}
