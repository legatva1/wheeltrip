package cz.cvut.fel.dcgi.wheeltrip;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * WheelTrip application class.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 19. 12. 2015
 */
public class WheelTripApplication extends Application {

    private static WheelTripApplication sApplicationInstance;

    public static WheelTripApplication get() {
        return sApplicationInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(getBaseContext());
        sApplicationInstance = this;
        Fabric.with(this, new Crashlytics());
    }

}
