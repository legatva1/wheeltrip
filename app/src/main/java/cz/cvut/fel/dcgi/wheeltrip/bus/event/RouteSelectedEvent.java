package cz.cvut.fel.dcgi.wheeltrip.bus.event;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 1. 2. 2016
 */
public class RouteSelectedEvent {

    private int position;

    public RouteSelectedEvent(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
