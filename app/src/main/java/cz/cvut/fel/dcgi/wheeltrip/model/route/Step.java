
package cz.cvut.fel.dcgi.wheeltrip.model.route;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Step {

    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("point_ref")
    @Expose
    private Integer pointRef;
    @SerializedName("instruction")
    @Expose
    private String instruction;

    private boolean isUsed;

    private LatLngBounds bounds;

    private LatLng position;

    /**
     * 
     * @return
     *     The type
     */
    public Integer getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The pointRef
     */
    public Integer getPointRef() {
        return pointRef;
    }

    /**
     * 
     * @param pointRef
     *     The point_ref
     */
    public void setPointRef(Integer pointRef) {
        this.pointRef = pointRef;
    }

    /**
     * 
     * @return
     *     The instruction
     */
    public String getInstruction() {
        return instruction;
    }

    /**
     * 
     * @param instruction
     *     The instruction
     */
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    public LatLngBounds getBounds() {
        return bounds;
    }

    public void setBounds(LatLngBounds bounds) {
        this.bounds = bounds;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Step{" +
                "type=" + type +
                ", pointRef=" + pointRef +
                ", instruction='" + instruction + '\'' +
                '}';
    }
}
