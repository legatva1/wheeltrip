package cz.cvut.fel.dcgi.wheeltrip.bus.event;

import java.util.ArrayList;

import cz.cvut.fel.dcgi.wheeltrip.model.naviterier.PoiEntryPoint;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 23. 3. 2016
 */
public class SourceDataFound {

    private ArrayList<PoiEntryPoint> points;

    public SourceDataFound(ArrayList<PoiEntryPoint> points) {
        this.points = points;
    }

    public ArrayList<PoiEntryPoint> getPoints() {
        return points;
    }

}
