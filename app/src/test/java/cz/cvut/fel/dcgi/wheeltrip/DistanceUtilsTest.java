package cz.cvut.fel.dcgi.wheeltrip;

import org.junit.Assert;
import org.junit.Test;

import cz.cvut.fel.dcgi.wheeltrip.util.DistanceUtils;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 13. 4. 2016
 */
public class DistanceUtilsTest {

    @Test
    public void testShortDistance(){
        String distance = DistanceUtils.getStringDistance(236);
        String expected = "236 m";
        Assert.assertEquals(expected,distance);
    }

    @Test
    public void testLongDistance(){
        String distance = DistanceUtils.getStringDistance(1236);
        String expected = "1.2 km";
        Assert.assertEquals(expected,distance);
    }

    @Test
    public void testDotZeroDistance(){
        String distance = DistanceUtils.getStringDistance(1000);
        String expected = "1 km";
        Assert.assertEquals(expected,distance);
    }
    @Test
    public void testLongRoundDistance(){
        String distance = DistanceUtils.getStringDistance(1286);
        String expected = "1.3 km";
        Assert.assertEquals(expected,distance);
    }

}
