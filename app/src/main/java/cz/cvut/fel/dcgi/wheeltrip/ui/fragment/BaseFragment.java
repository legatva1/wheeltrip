package cz.cvut.fel.dcgi.wheeltrip.ui.fragment;

import android.content.Intent;
import android.speech.tts.TextToSpeech;

import com.google.inject.Inject;

import cz.cvut.fel.dcgi.wheeltrip.SessionData;
import cz.cvut.fel.dcgi.wheeltrip.bus.AppBus;
import cz.cvut.fel.dcgi.wheeltrip.service.tts.TextToSpeechService;
import cz.cvut.fel.dcgi.wheeltrip.util.Constants;
import roboguice.util.Ln;

/**
 * Base fragment that registers event bus.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 29. 1. 2016
 */
public abstract class BaseFragment extends StateLoggingFragment{

    @Inject protected AppBus mAppBus;
    @Inject protected SessionData sessionData;

    protected TextToSpeechService ttsService;

    @Override
    public void onStart() {
        super.onStart();
        mAppBus.register(this);
    }

    @Override
    public void onStop() {
        mAppBus.unregister(this);
        super.onStop();
    }

    /**
     * Override this method if you want to custom logic in OnBackPress
     * @return true if back press has custom logic, false otherwise
     */
    public boolean onBackPressed(){
        return false;
    }

    protected void checkTTS() {
        Ln.d("checkTTS");
        Intent check = new Intent();
        check.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(check, Constants.TTS_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.TTS_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                ttsService = new TextToSpeechService(getActivity());
                Ln.d("init textToSpeechService");
            } else {
                Ln.d("init install data");
                Intent install = new Intent();
                install.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(install);
            }
        }
    }

    @Override
    public void onDestroy() {
        if (ttsService != null) {
            ttsService.destroy();
        }
        super.onDestroy();
    }
}
