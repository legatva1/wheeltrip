
package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SourceData {


    @SerializedName("PoiEntryPoints")
    @Expose
    private List<PoiEntryPoint> poiEntryPoints = new ArrayList<>();

    public List<PoiEntryPoint> getPoiEntryPoints() {
        return poiEntryPoints;
    }

    public void setPoiEntryPoints(List<PoiEntryPoint> poiEntryPoints) {
        this.poiEntryPoints = poiEntryPoints;
    }

    @Override
    public String toString() {
        return "SourceData{" +
                "poiEntryPoints=" + poiEntryPoints +
                '}';
    }
}
