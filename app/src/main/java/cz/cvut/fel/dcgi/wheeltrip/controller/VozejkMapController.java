package cz.cvut.fel.dcgi.wheeltrip.controller;

import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.maps.android.SphericalUtil;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.SessionData;
import cz.cvut.fel.dcgi.wheeltrip.WheelTripApplication;
import cz.cvut.fel.dcgi.wheeltrip.bus.AppBus;
import cz.cvut.fel.dcgi.wheeltrip.bus.event.VozejkMapDataDownloadedEvent;
import cz.cvut.fel.dcgi.wheeltrip.db.vozejkmap.VozejkMapContract;
import cz.cvut.fel.dcgi.wheeltrip.db.WheelTripOpenHelper;
import cz.cvut.fel.dcgi.wheeltrip.model.vozejkmap.VozejkMapPOI;
import cz.cvut.fel.dcgi.wheeltrip.util.PreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import roboguice.util.Ln;

/**
 * VozejkMap controller.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 20. 12. 2015
 */
@Singleton
public class VozejkMapController {

    private static final String BASE_URL = "http://www.vozejkmap.cz";

    @Inject
    private AppBus mAppBus;

    @Inject
    private SessionData mSessionData;

    /**
     * Calls VozejkMap endpoint to obtain data from locations.json file.
     */
    public void getVozejkMapData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        VozejkMapServiceInterface service = retrofit.create(VozejkMapServiceInterface.class);

        Call<List<VozejkMapPOI>> call = service.getVozejkMapPOIs();
        call.enqueue(new Callback<List<VozejkMapPOI>>() {
            @Override
            public void onResponse(Response<List<VozejkMapPOI>> response, Retrofit retrofit) {

                new InsertDataTask().execute(response.body());
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(WheelTripApplication.get(), R.string.vozejkmap_data_download_failure, Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Asynchronous task that inserts VozejkMap data into database.
     */
    private class InsertDataTask extends AsyncTask<List<VozejkMapPOI>, String, Void> {

        @Override
        protected Void doInBackground(List<VozejkMapPOI>... poisArray) {
            insertAllVozejkMapData(poisArray[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            super.onPostExecute(v);
            Toast.makeText(WheelTripApplication.get(), R.string.vozejkmap_data_download_success, Toast.LENGTH_SHORT).show();
            PreferencesUtils.putBoolean(PreferencesUtils.VOZEJKMAP_DATA_DOWNLOADED, true);
            mAppBus.post(new VozejkMapDataDownloadedEvent());
        }
    }

    /**
     * Performs bulk insert of vozejkmap pois.
     * @param pois pois
     */
    public void insertAllVozejkMapData(List<VozejkMapPOI> pois) {
        ContentValues[] values = new ContentValues[pois.size()];
        ContentValues contentValues;

        for (int i = 0; i < pois.size(); i++) {
            contentValues = new ContentValues();
            contentValues.put(VozejkMapContract.PoiEntry.TITLE, pois.get(i).getName());
            contentValues.put(VozejkMapContract.PoiEntry.LOCATION_TYPE, pois.get(i).getType());
            contentValues.put(VozejkMapContract.PoiEntry.DESCRIPTION, pois.get(i).getDescription());
            contentValues.put(VozejkMapContract.PoiEntry.LAT, pois.get(i).getLatitude());
            contentValues.put(VozejkMapContract.PoiEntry.LNG, pois.get(i).getLongitude());
            contentValues.put(VozejkMapContract.PoiEntry.ATTR1, pois.get(i).getAccessibilityType());
            contentValues.put(VozejkMapContract.PoiEntry.ATTR2, pois.get(i).getRestroomAvailable());
            contentValues.put(VozejkMapContract.PoiEntry.ATTR3, pois.get(i).getParkingvailable());
            contentValues.put(VozejkMapContract.PoiEntry.SEARCH_TITLE, StringUtils.stripAccents(pois.get(i).getName()).toLowerCase());
            contentValues.put(VozejkMapContract.PoiEntry.STREET, pois.get(i).getStreet());
            contentValues.put(VozejkMapContract.PoiEntry.STREET_NUMBER, pois.get(i).getStreetNumber());
            contentValues.put(VozejkMapContract.PoiEntry.CITY, pois.get(i).getCity());
            values[i] = contentValues;
        }

        WheelTripApplication.get().getContentResolver().bulkInsert(VozejkMapContract.PoiEntry.CONTENT_URI, values);
    }

    /**
     * Loads data for individual vozejkmap poi.
     * @param marker marker of poi to load data for
     * @return vozejkmap poi
     */
    public VozejkMapPOI loadDataFromCursor(Marker marker) {

        String selection = VozejkMapContract.PoiEntry.LAT + " = " + marker.getPosition().latitude + " AND " + VozejkMapContract.PoiEntry.LNG + " = " + marker.getPosition().longitude;

        Ln.d("where selection: %s", selection);
        Cursor cursor = WheelTripApplication.get().getContentResolver().query(
                VozejkMapContract.PoiEntry.CONTENT_URI,
                VozejkMapContract.PoiEntry.POI_COLUMNS,
                selection,
                null,
                null);

        VozejkMapPOI poi = new VozejkMapPOI();
        if (cursor != null && cursor.moveToFirst()) {
            Ln.d("cursor %s", cursor);
            poi.setName(cursor.getString(VozejkMapContract.VozejkMapTable.TITLE));
            poi.setType(cursor.getInt(VozejkMapContract.VozejkMapTable.LOCATION_TYPE));
            poi.setDescription(cursor.getString(VozejkMapContract.VozejkMapTable.DESCRIPTION));
            poi.setLatitude(cursor.getDouble(VozejkMapContract.VozejkMapTable.LAT));
            poi.setLongitude(cursor.getDouble(VozejkMapContract.VozejkMapTable.LNG));
            poi.setAccessibilityType(cursor.getInt(VozejkMapContract.VozejkMapTable.ATTR1));
            poi.setRestroomAvailable(cursor.getString(VozejkMapContract.VozejkMapTable.ATTR2));
            poi.setParkingAvailable(cursor.getString(VozejkMapContract.VozejkMapTable.ATTR3));
            poi.setStreet(cursor.getString(VozejkMapContract.VozejkMapTable.STREET));
            poi.setStreetNumber(cursor.getString(VozejkMapContract.VozejkMapTable.STREET_NUMBER));
            poi.setCity(cursor.getString(VozejkMapContract.VozejkMapTable.CITY));
            cursor.close();
        } else {
            Ln.d("marker cursor is null");
        }
        return poi;
    }

    /**
     * Converts cursor data into list of VozejkMapPOI objects.
     *
     * @param cursor cursor with VozejkMap data
     * @return list of VozejkMapPOI objects
     */
    public List<VozejkMapPOI> getPoisFromCursor(Cursor cursor, LatLng currentLocation) {
        VozejkMapPOI poi;
        List<VozejkMapPOI> pois = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {

                poi = new VozejkMapPOI();
                poi.setName(cursor.getString(VozejkMapContract.VozejkMapTable.TITLE));
                poi.setType(cursor.getInt(VozejkMapContract.VozejkMapTable.LOCATION_TYPE));
                poi.setDescription(cursor.getString(VozejkMapContract.VozejkMapTable.DESCRIPTION));
                poi.setLatitude(cursor.getDouble(VozejkMapContract.VozejkMapTable.LAT));
                poi.setLongitude(cursor.getDouble(VozejkMapContract.VozejkMapTable.LNG));
                poi.setAccessibilityType(cursor.getInt(VozejkMapContract.VozejkMapTable.ATTR1));
                poi.setRestroomAvailable(cursor.getString(VozejkMapContract.VozejkMapTable.ATTR2));
                poi.setParkingAvailable(cursor.getString(VozejkMapContract.VozejkMapTable.ATTR3));
                poi.setDistance(SphericalUtil.computeDistanceBetween(currentLocation, new LatLng(poi.getLatitude(), poi.getLongitude())));
                poi.setStreet(cursor.getString(VozejkMapContract.VozejkMapTable.STREET));
                poi.setStreetNumber(cursor.getString(VozejkMapContract.VozejkMapTable.STREET_NUMBER));
                poi.setCity(cursor.getString(VozejkMapContract.VozejkMapTable.CITY));
                pois.add(poi);

            } while (cursor.moveToNext());
        }

        sortPoiByDistance(pois);

        return pois;
    }

    /**
     * Sorts pois by distance to current location.
     *
     * @param pois pois to sort
     */
    private void sortPoiByDistance(List<VozejkMapPOI> pois) {
        Collections.sort(pois, new Comparator<VozejkMapPOI>() {
            @Override
            public int compare(VozejkMapPOI lhs, VozejkMapPOI rhs) {
                if (lhs.getDistance() > rhs.getDistance()) {
                    return 1;
                } else if (lhs.getDistance() < rhs.getDistance()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
    }

    /**
     * Creates cursor loader for search list.
     *
     * @param context application context
     * @return cursor loader with vozejkmap data
     */
    public Loader<Cursor> onCreateLoader(Context context) {
        String[] projection = VozejkMapContract.PoiEntry.POI_COLUMNS;
        return new CursorLoader(context,
                VozejkMapContract.PoiEntry.CONTENT_URI, projection, appendLocationTypeFilterQuery(), null, null);

    }

    /**
     * Gets list of vozejkmap pois inside screen bounds.
     *
     * @return list of vozejkmap pois
     */
    public List<VozejkMapPOI> getPOIs() {

        List<VozejkMapPOI> pois = new ArrayList<>();
        if (mSessionData.getScreenBounds() != null) {

            String selection = getScreenBoundsQuery(mSessionData.getScreenBounds());
            selection += " AND " + appendLocationTypeFilterQuery();
            WheelTripOpenHelper helper = new WheelTripOpenHelper(WheelTripApplication.get());
            SQLiteDatabase db = helper.getReadableDatabase();
            Cursor cursor = db.query(VozejkMapContract.PoiEntry.TABLE_NAME, VozejkMapContract.PoiEntry.POI_COLUMNS, selection,
                    null, null, null, null, null);

            pois = getPoisFromCursor(cursor, mSessionData.getLastLocation());
            Ln.d("pois size %s",pois.size());
            db.close();
        }

        return pois;

    }

    /**
     * Creates pois query based on visible screen bounds.
     *
     * @param bounds visible screen bounds
     * @return where selection clause
     */
    private String getScreenBoundsQuery(LatLngBounds bounds) {

        double swLat = bounds.southwest.latitude;
        double swLng = bounds.southwest.longitude;
        double neLat = bounds.northeast.latitude;
        double neLng = bounds.northeast.longitude;

        String selection = "" +
                "((" + swLat + " < " + neLat + " AND LAT BETWEEN " + swLat + " AND " + neLat + ") " +
                "OR " +
                "(" + neLat + " < " + swLat + " AND LAT BETWEEN " + neLat + " AND " + swLat + ")) " +
                "AND " +
                "((" + swLng + " < " + neLng + " AND LNG BETWEEN " + swLng + " AND " + neLng + ") " +
                "OR " +
                "(" + neLng + " < " + swLng + " AND LNG BETWEEN " + neLng + " AND " + swLng + "))";

        return selection;
    }

    /**
     * Gets selected location types ids.
     *
     * @return selected location types
     */
    private List<Integer> getLocationTypes() {
        List<Integer> filterItems = new ArrayList<>();
        boolean[] filterSettings = PreferencesUtils.getFilterSettings();
        for (int i = 0; i < filterSettings.length; i++) {
            if (filterSettings[i]) {
                filterItems.add(i + 1);
            }
        }
        return filterItems;
    }

    /**
     * Appends location types filter to where selection clause
     *
     * @return where selection clause with location types
     */
    public String appendLocationTypeFilterQuery() {
        StringBuilder selectionBuilder = new StringBuilder();

        List<Integer> filterItems = getLocationTypes();

        if (filterItems.isEmpty()) {
            selectionBuilder.append(VozejkMapContract.PoiEntry.LOCATION_TYPE)
                    .append(" = ")
                    .append(0);
        }

        for (int i = 0; i < filterItems.size(); i++) {
            if (i == 0) {
                selectionBuilder.append("(");
            }
            if (i == filterItems.size() - 1) {
                selectionBuilder.append(VozejkMapContract.PoiEntry.LOCATION_TYPE).append(" = ").append(filterItems.get(i)).append(")");
                break;
            }
            selectionBuilder.append(VozejkMapContract.PoiEntry.LOCATION_TYPE).append(" = ").append(filterItems.get(i)).append(" OR ");
        }

        return selectionBuilder.toString();
    }
}
