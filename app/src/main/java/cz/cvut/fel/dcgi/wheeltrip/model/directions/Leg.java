
package cz.cvut.fel.dcgi.wheeltrip.model.directions;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Leg {

    @SerializedName("steps")
    @Expose
    private List<Step> steps = new ArrayList<>();
    @SerializedName("duration")
    @Expose
    private Duration_ duration;
    @SerializedName("distance")
    @Expose
    private Distance_ distance;
    @SerializedName("start_location")
    @Expose
    private StartLocation_ startLocation;
    @SerializedName("end_location")
    @Expose
    private EndLocation_ endLocation;
    @SerializedName("start_address")
    @Expose
    private String startAddress;
    @SerializedName("end_address")
    @Expose
    private String endAddress;

    /**
     * @return The steps
     */
    public List<Step> getSteps() {
        return steps;
    }

    /**
     * @param steps The steps
     */
    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    /**
     * @return The duration
     */
    public Duration_ getDuration() {
        return duration;
    }

    /**
     * @param duration The duration
     */
    public void setDuration(Duration_ duration) {
        this.duration = duration;
    }

    /**
     * @return The distance
     */
    public Distance_ getDistance() {
        return distance;
    }

    /**
     * @param distance The distance
     */
    public void setDistance(Distance_ distance) {
        this.distance = distance;
    }

    /**
     * @return The startLocation
     */
    public StartLocation_ getStartLocation() {
        return startLocation;
    }

    /**
     * @param startLocation The start_location
     */
    public void setStartLocation(StartLocation_ startLocation) {
        this.startLocation = startLocation;
    }

    /**
     * @return The endLocation
     */
    public EndLocation_ getEndLocation() {
        return endLocation;
    }

    /**
     * @param endLocation The end_location
     */
    public void setEndLocation(EndLocation_ endLocation) {
        this.endLocation = endLocation;
    }

    /**
     * @return The startAddress
     */
    public String getStartAddress() {
        return startAddress;
    }

    /**
     * @param startAddress The start_address
     */
    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    /**
     * @return The endAddress
     */
    public String getEndAddress() {
        return endAddress;
    }

    /**
     * @param endAddress The end_address
     */
    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

}
