
package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Itinerary {

    @SerializedName("GeneralDescription")
    @Expose
    private String generalDescription;
    @SerializedName("Stages")
    @Expose
    private List<String> stages = new ArrayList<String>();

    public String getGeneralDescription() {
        return generalDescription;
    }

    public void setGeneralDescription(String generalDescription) {
        this.generalDescription = generalDescription;
    }

    public List<String> getStages() {
        return stages;
    }

    public void setStages(List<String> stages) {
        this.stages = stages;
    }

    @Override
    public String toString() {
        return "Itinerary{" +
                "generalDescription='" + generalDescription + '\'' +
                ", stages=" + stages +
                '}';
    }
}
