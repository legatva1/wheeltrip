package cz.cvut.fel.dcgi.wheeltrip.controller;


import cz.cvut.fel.dcgi.wheeltrip.model.directions.Direction;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Endpoint definitions for Directions API.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 6. 2. 2016
 */
public interface DirectionsServiceInterface {


    @GET("maps/api/directions/json?mode=walking&alternatives=true&key=AIzaSyB9xs9jFvg6jfnUh4YMPLaXjlrgn_TuZrg")
    Call<Direction> getDirections(@Query("origin")String origin, @Query("destination")String destination);
}
