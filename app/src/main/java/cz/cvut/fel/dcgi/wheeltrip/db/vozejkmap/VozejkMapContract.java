package cz.cvut.fel.dcgi.wheeltrip.db.vozejkmap;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 20. 12. 2015
 */
public final class VozejkMapContract {

    public static final String CONTENT_AUTHORITY = "cz.cvut.fel.dcgi.wheeltrip";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_POI = "poi";

    public static final class PoiEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_POI).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI;

        public static final String TABLE_NAME = "VOZEJKMAP_POI";

        public static final String TITLE = "TITLE";
        public static final String LOCATION_TYPE = "LOCATION_TYPE";
        public static final String DESCRIPTION = "DESCRIPTION";
        public static final String LAT = "LAT";
        public static final String LNG = "LNG";
        public static final String ATTR1 = "ATTR1";
        public static final String ATTR2 = "ATTR2";
        public static final String ATTR3 = "ATTR3";
        public static final String SEARCH_TITLE = "SEARCH_TITLE";
        public static final String STREET = "STREET";
        public static final String STREET_NUMBER = "STREET_NUMBER";
        public static final String CITY = "CITY";

        public static final String [] POI_COLUMNS = {_ID,TITLE,LOCATION_TYPE,DESCRIPTION,LAT,LNG,ATTR1,ATTR2,ATTR3,SEARCH_TITLE,STREET,STREET_NUMBER,CITY};

        public static Uri buildPoiUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }


    public static class VozejkMapTable {
        public static final int _ID = 0;
        public static final int TITLE = 1;
        public static final int LOCATION_TYPE = 2;
        public static final int DESCRIPTION = 3;
        public static final int LAT = 4;
        public static final int LNG = 5;
        public static final int ATTR1 = 6;
        public static final int ATTR2 = 7;
        public static final int ATTR3 = 8;
        public static final int SEARCH_TITLE = 9;
        public static final int STREET = 10;
        public static final int STREET_NUMBER = 11;
        public static final int CITY = 12;

    }
}
