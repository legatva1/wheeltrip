
package cz.cvut.fel.dcgi.wheeltrip.model.route;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RouteData {

    @SerializedName("origin")
    @Expose
    private Origin origin;
    @SerializedName("destination")
    @Expose
    private Destination destination;
    @SerializedName("routes")
    @Expose
    private List<Route> routes = new ArrayList<>();

    /**
     * 
     * @return
     *     The origin
     */
    public Origin getOrigin() {
        return origin;
    }

    /**
     * 
     * @param origin
     *     The origin
     */
    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    /**
     * 
     * @return
     *     The destination
     */
    public Destination getDestination() {
        return destination;
    }

    /**
     * 
     * @param destination
     *     The destination
     */
    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    /**
     * 
     * @return
     *     The routes
     */
    public List<Route> getRoutes() {
        return routes;
    }

    /**
     * 
     * @param routes
     *     The routes
     */
    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    @Override
    public String toString() {
        return "RouteData{" +
                "origin=" + origin +
                ", destination=" + destination +
                ", routes=" + routes +
                '}';
    }
}
