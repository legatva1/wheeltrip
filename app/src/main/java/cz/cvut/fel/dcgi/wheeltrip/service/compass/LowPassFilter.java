package cz.cvut.fel.dcgi.wheeltrip.service.compass;

/**
 * LowPassFilter filters out high frequency changes.
 * @link https://www.built.io/blog/2013/05/applying-low-pass-filter-to-android-sensors-readings/
 * @since 25. 1. 2016
 */
public class LowPassFilter {

    private final static float ALPHA = 0.2f;

    public static float[] filter(float[] input, float[] output) {
        if (output == null) {
            return input;
        }

        for (int i = 0; i < input.length; i++) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }

        return output;
    }
}
