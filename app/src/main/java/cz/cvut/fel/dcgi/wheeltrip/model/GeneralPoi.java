package cz.cvut.fel.dcgi.wheeltrip.model;

/**
 * Empty general poi.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 8. 4. 2016
 */
public class GeneralPoi extends PoiImpl {

}
