package cz.cvut.fel.dcgi.wheeltrip.model.naviterier;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 23. 3. 2016
 */
public class SourceDataRequest {

    @SerializedName("SearchOrigin")
    @Expose
    private Point point;

    @SerializedName("Radius")
    @Expose
    private int radius;

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "SourceDataRequest{" +
                "point=" + point +
                ", radius=" + radius +
                '}';
    }
}
