package cz.cvut.fel.dcgi.wheeltrip.ui.activity;

import cz.cvut.fel.dcgi.wheeltrip.ui.fragment.RouteFragment;

/**
 * Activity that contains RouteFragment.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 3. 2016
 */

public class RouteActivity extends UiActivity<RouteFragment> {

}
