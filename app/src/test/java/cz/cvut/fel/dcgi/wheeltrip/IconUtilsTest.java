package cz.cvut.fel.dcgi.wheeltrip;

import junit.framework.Assert;

import org.junit.Test;

import cz.cvut.fel.dcgi.wheeltrip.model.route.Direction;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Obstacle;
import cz.cvut.fel.dcgi.wheeltrip.util.IconUtils;

/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 13. 4. 2016
 */
public class IconUtilsTest {

    @Test
    public void getPoiIconTest() {
        Assert.assertEquals(R.drawable.ic_culture, IconUtils.getPoiIcon(1));
        Assert.assertEquals(R.drawable.ic_doctor, IconUtils.getPoiIcon(6));
        Assert.assertEquals(R.drawable.ic_shop, IconUtils.getPoiIcon(11));
        Assert.assertEquals(R.drawable.ic_unknown, IconUtils.getPoiIcon(20));
    }

    @Test
    public void getRouteDifficultyIconTest() {
        Assert.assertEquals(R.drawable.ic_smily_face, IconUtils.getRouteDifficultyIcon(0));
        Assert.assertEquals(R.drawable.ic_neutral_face, IconUtils.getRouteDifficultyIcon(1));
        Assert.assertEquals(R.drawable.ic_frowny_face, IconUtils.getRouteDifficultyIcon(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getBadRouteDifficultyIconTest() {
        IconUtils.getRouteDifficultyIcon(3);
    }

    @Test
    public void getDirectionArrowTest() {
        Assert.assertEquals(R.drawable.ic_nav_arrow_left, IconUtils.getDirectionArrow(Direction.LEFT));
        Assert.assertEquals(R.drawable.ic_nav_arrow_right, IconUtils.getDirectionArrow(Direction.RIGHT));
    }

    @Test
    public void getObstacleTest() {
        Assert.assertEquals(R.drawable.ic_stairs, IconUtils.getObstacle(Obstacle.STAIRS));
    }

    @Test
    public void getDirectionArrowForStageTest() {
        //Assert.assertEquals(Direction.STRAIGHT, IconUtils.getDirectionArrowForStage("vpřed"));
    }
}
