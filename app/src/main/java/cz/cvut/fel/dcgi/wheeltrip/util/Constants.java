package cz.cvut.fel.dcgi.wheeltrip.util;

/**
 * Request codes.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 15. 5. 2016
 */
public class Constants {


    //reguest codes
    public static final int TTS_CODE = 11;
    public static final int POI_REQUEST = 2;
    public static final int FILTER_REQUEST = 3;
    public static final int MAP_REQUEST = 4;
    public static final int REQ_CODE_SPEECH_INPUT = 1;

    //intent keys
    public static final String POI = "poi";
    public static final String TARGET = "target";
    public static final String YES = "yes";
    public static final String QUERY = "query";
}
