package cz.cvut.fel.dcgi.wheeltrip.util;

/**
 * Distance utils.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 28. 12. 2015
 */
public class DistanceUtils {

    /**
     * gets string representation of distance.
     * @param distance distance
     * @return string representation of distance
     */
    public static String getStringDistance(double distance) {
        if (distance < 1000) {
            return Math.round(distance) + " m";
        } else {
            String dis = Math.round(distance / 1000.0 * 10.0) / 10.0 + "";

            if (dis.contains(".0")) {
                dis = dis.substring(0, dis.length() - 2);
            }

            return dis + " km";
        }
    }

}
