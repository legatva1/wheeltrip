package cz.cvut.fel.dcgi.wheeltrip.controller;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.SessionData;
import cz.cvut.fel.dcgi.wheeltrip.WheelTripApplication;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Difficulty;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Obstacle;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Route;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Step;
import cz.cvut.fel.dcgi.wheeltrip.model.vozejkmap.VozejkMapPOI;
import cz.cvut.fel.dcgi.wheeltrip.util.IconUtils;
import cz.cvut.fel.dcgi.wheeltrip.util.PreferencesUtils;
import roboguice.util.Ln;

/**
 * GoogleMap controller.
 *
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 22. 12. 2015
 */
@Singleton
public class MapController {

    @Inject
    private SessionData mSessionData;
    private static List<Marker> markers = new ArrayList<>();
    private static List<Marker> difficulties = new ArrayList<>();
    private static final LatLng NEUTRAL_POSITION = new LatLng(0, 0);

    /**
     * Adds markers of VozejkMap POIs on the map.
     *
     * @param pois VozejkMap data
     * @param map  GoogleMap to add markers to
     */
    public void addMarkers(List<VozejkMapPOI> pois, GoogleMap map) {
        if (map == null) {
            return;
        }

        // remove markers for small zoom
        if (map.getCameraPosition().zoom < 17) {
            removeMarkers();
            return;
        }


        //remove out of bounds markers
        removeMarkers(mSessionData.getScreenBounds());
        //remove markers according to filter
        removeMarkersByFilter();

        for (VozejkMapPOI poi : pois) {
            LatLng pos = new LatLng(poi.getLatitude(), poi.getLongitude());
            MarkerOptions op = new MarkerOptions()
                    .position(pos)
                    .icon(BitmapDescriptorFactory.fromResource(IconUtils.getPoiIcon(poi.getType())))
                    .anchor(0.5f, 0.5f)
                    .snippet(poi.getType() + "")
                    .title(poi.getName());
            boolean containsMarker = false;
            for (int i = 0; i < markers.size(); i++) {
                if (markers.get(i).getPosition().equals(pos)) {
                    Ln.d("contains marker %s", op.getTitle());
                    containsMarker = true;
                    break;
                }
            }

            if (!containsMarker) {
                markers.add(map.addMarker(op));
            }
        }
    }

    /**
     * Removes markers that are outside screen visible bounds.
     *
     * @param bounds visible area
     */
    private void removeMarkers(LatLngBounds bounds) {
        int size = markers.size();
        for (int i = size - 1; i >= 0; i--) {
            if (!bounds.contains(markers.get(i).getPosition())) {
                Ln.d("removing marker %s", markers.get(i).getTitle());
                markers.get(i).remove();
                markers.remove(markers.get(i));
            }

        }
    }

    /**
     * Removes all markers.
     */
    private void removeMarkers() {
        for (Marker m : markers) {
            m.remove();
        }
        markers.clear();
    }

    /**
     * Revomes markers that are not selected on filter screen.
     */
    private void removeMarkersByFilter() {
        int size = markers.size();
        boolean[] filterSettings = PreferencesUtils.getFilterSettings();
        for (int i = size - 1; i >= 0; i--) {
            if (!filterSettings[Integer.valueOf(markers.get(i).getSnippet()) - 1]) {
                Ln.d("removing marker %s", markers.get(i).getTitle());
                markers.get(i).remove();
                markers.remove(markers.get(i));
            }

        }
    }

    /**
     * Converts visible device screen to geographic bounds.
     *
     * @param map GoogleMap
     * @return geographical bounds of the screen
     */
    public LatLngBounds getVisibleAreaBounds(GoogleMap map) {
        return map.getProjection().getVisibleRegion().latLngBounds;
    }

    /**
     * Animates camera to specified position.
     *
     * @param position target position
     * @param map      GoogleMap
     */
    public void animateCameraToPosition(LatLng position, GoogleMap map) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(position)
                .zoom(NEUTRAL_POSITION.equals(position) ? 1 : 17)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    /**
     * Animates camera to specified position.
     *
     * @param position target position
     * @param bearing  map bearing
     * @param map      GoogleMap
     */
    public void animateCameraToPosition(LatLng position, float bearing, GoogleMap map) {
        Ln.d("animate to %s", position);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(position)
                .zoom(19)
                .bearing(bearing)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    /**
     * Moves camera to target position without animation.
     *
     * @param position target position
     * @param map      GoogleMap
     */
    public void moveCameraToPosition(LatLng position, GoogleMap map) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(position)
                .zoom(NEUTRAL_POSITION.equals(position) ? 1 : 17)
                .build();
        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


    /**
     * Converts LatLng position and specified radius into geographical bounds.
     *
     * @param center position
     * @param radius radius in metres
     * @return geographical bounds
     */
    public LatLngBounds convertCenterAndRadiusToBounds(LatLng center, double radius) {
        LatLng southwest = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 225);
        LatLng northeast = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 45);
        return new LatLngBounds(southwest, northeast);
    }


    /**
     * Draws single route on the map.
     *
     * @param route route to draw
     * @param map   GoogleMap
     */
    public void drawRoute(Route route, GoogleMap map) {
        Ln.d("route %s", route.getRouteLine());
        route.getRouteLine().drawOnMap(map);
    }

    /**
     * Draw list of routes on the map.
     *
     * @param routes routes to draw
     * @param map    GoogleMap
     */
    public void drawRoutes(List<Route> routes, GoogleMap map) {
        for (Route route : routes) {
            drawRoute(route, map);
        }

        drawMarker(mSessionData.getRouteData().getOrigin().getLatLng(), map);
        drawMarker(mSessionData.getRouteData().getDestination().getLatLng(), map);
        mSessionData.setRoutesDrawn(true);
    }

    /**
     * Draws single marker.
     * @param position marker position
     * @param map GoogleMap
     * @return marker
     */
    public Marker drawMarker(LatLng position, GoogleMap map) {
        MarkerOptions op = new MarkerOptions()
                .position(position)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.route_end_marker))
                .anchor(0.5f, 0.5f);
        return map.addMarker(op);
    }


    /**
     * Draws difficulty on the map.
     * @param difficulty difficulty object
     * @param map GoogleMap
     * @return marker
     */
    public Marker drawDifficulty(Difficulty difficulty, GoogleMap map) {
        MarkerOptions op = new MarkerOptions()
                .position(difficulty.getPosition())
                .icon(BitmapDescriptorFactory.fromResource(IconUtils.getObstacle(Obstacle.getByType(difficulty.getType()))))
                .snippet(difficulty.getDescription())
                .anchor(0.5f, 0.5f);
        return map.addMarker(op);
    }

    /**
     * Removes all difficulties.
     */
    public void removeDifficulties() {
        for (Marker m : difficulties) {
            m.remove();
        }
        difficulties.clear();
    }

    /**
     * Draws route difficulties.
     * @param route route
     * @param map GoogleMap
     */
    public void drawDifficulties(Route route, GoogleMap map) {
        removeDifficulties();
        for (Difficulty difficulty : route.getDifficulties()) {
            difficulties.add(drawDifficulty(difficulty, map));
        }
    }

    /**
     * Animates camera to bounds.
     *
     * @param bounds map bounds
     * @param map    GoogleMap
     */
    public void animateToBounds(LatLngBounds bounds, GoogleMap map) {
        Resources r = WheelTripApplication.get().getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 96, r.getDisplayMetrics());
        int padding = (int) px;
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
    }

    /**
     * Calculates bounds from list of positions.
     *
     * @param points LatLng positions
     * @return calculated bounds
     */
    public LatLngBounds calculateBounds(List<LatLng> points) {
        LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
        for (LatLng point : points) {
            boundsBuilder.include(point);
        }

        return boundsBuilder.build();

    }

    /**
     * Decides if the distance between two points is smaller than specified minimum.
     *
     * @param source   source position
     * @param target   target position
     * @param distance minimum distance
     * @return true if two positions are close to each other, false otherwise
     */
    public boolean isNear(LatLng source, LatLng target, double distance) {
        return SphericalUtil.computeDistanceBetween(source, target) < distance;
    }

    /**
     * Draws checkpoint for navigation steps.
     * @param steps steps to draw checkpoints for
     * @param map GoogleMap
     * @param context application context
     */
    public void drawCheckpoints(List<Step> steps, GoogleMap map, Context context) {
        for (Step step : steps) {
            map.addCircle(new CircleOptions()
                    .center(step.getPosition())
                    .radius(10)
                    .strokeColor(context.getResources().getColor(R.color.checkpoint_stroke))
                    .fillColor(context.getResources().getColor(R.color.checkpoint_fill)));

        }
    }

}
