package cz.cvut.fel.dcgi.wheeltrip.model.filter;

/**
 * Filter item container.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 29. 12. 2015
 */
public class FilterItem {


    private String title;
    private int locationType;
    private boolean checked;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLocationType() {
        return locationType;
    }

    public void setLocationType(int locationType) {
        this.locationType = locationType;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
