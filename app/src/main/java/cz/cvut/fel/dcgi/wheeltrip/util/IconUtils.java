package cz.cvut.fel.dcgi.wheeltrip.util;

import cz.cvut.fel.dcgi.wheeltrip.R;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Direction;
import cz.cvut.fel.dcgi.wheeltrip.model.route.Obstacle;

/**
 * Icon utils.
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 23. 12. 2015
 */
public class IconUtils {


    public static int getPoiIcon(int locationId) {
        switch (locationId) {
            case 1:
                return R.drawable.ic_culture;
            case 2:
                return R.drawable.ic_sport;
            case 3:
                return R.drawable.ic_institution;
            case 4:
                return R.drawable.ic_food_and_drink;
            case 5:
                return R.drawable.ic_accomodation;
            case 6:
                return R.drawable.ic_doctor;
            case 7:
                return R.drawable.ic_unknown;
            case 8:
                return R.drawable.ic_traffic;
            case 9:
                return R.drawable.ic_restroom;
            case 10:
                return R.drawable.ic_gas_station;
            case 11:
                return R.drawable.ic_shop;
            case 12:
                return R.drawable.ic_bank;
            case 13:
                return R.drawable.ic_parking;
            default:
                return R.drawable.ic_unknown;
        }
    }

    public static int getRouteDifficultyIcon(int difficulty) {
        switch (difficulty) {
            case 0:
                return R.drawable.ic_smily_face;
            case 1:
                return R.drawable.ic_neutral_face;
            case 2:
                return R.drawable.ic_frowny_face;
            default:
                throw new IllegalArgumentException("Unsupported route difficulty: " + difficulty);
        }
    }


    public static int getDirectionArrow(Direction direction) {
        switch (direction) {
            case STRAIGHT:
                return R.drawable.ic_nav_arrow_straight;
            case LEFT:
                return R.drawable.ic_nav_arrow_left;
            case RIGHT:
                return R.drawable.ic_nav_arrow_right;
            case SLIGHT_LEFT:
                return R.drawable.ic_nav_arrow_slight_left;
            case SLIGHT_RIGHT:
                return R.drawable.ic_nav_arrow_slight_right;
            default:
                return R.drawable.ic_nav_arrow_straight;
            //throw new IllegalArgumentException("Unsupported direction arrow type: "+direction.getType());
        }
    }

    public static int getObstacle(Obstacle obstacle) {
        switch (obstacle) {
            case STAIRS:
                return R.drawable.ic_stairs;
            case BAD_SURFACE:
                return R.drawable.ic_bad_surface;
            case STEEP_HILL_UP:
                return R.drawable.ic_steep_hill_up;
            case STEEP_HILL_DOWN:
                return R.drawable.ic_steep_hill_down;
            case NARROW_ROAD:
                return R.drawable.ic_narrow_road;
            default:
                throw new IllegalArgumentException("Unsupported obstacle type: " + obstacle.getType());
        }
    }

    public static Direction getDirectionArrowForStage(String stage) {

        if (stage.contains("vpřed")) {
            return Direction.STRAIGHT;
        }
        if (stage.contains("mírně vlevo")) {
            return Direction.SLIGHT_LEFT;
        }
        if (stage.contains("mírně vpravo")) {
            return Direction.SLIGHT_RIGHT;
        }
        if (stage.contains("vlevo")) {
            return Direction.LEFT;
        }
        if (stage.contains("vpravo")) {
            return Direction.RIGHT;
        }


        return Direction.UNKNOWN;
    }
}
