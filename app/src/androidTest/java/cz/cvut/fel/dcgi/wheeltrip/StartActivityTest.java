package cz.cvut.fel.dcgi.wheeltrip;

import android.support.test.InstrumentationRegistry;
import android.test.ActivityInstrumentationTestCase2;

import org.junit.Before;
import org.junit.Test;

import cz.cvut.fel.dcgi.wheeltrip.ui.activity.StartActivity;


/**
 * @author Vaclav Legat (vaclav.legat@gmail.com)
 * @since 15. 4. 2016
 */
public class StartActivityTest extends ActivityInstrumentationTestCase2<StartActivity> {

    private StartActivity mActivity;

    public StartActivityTest() {
        super(StartActivity.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        mActivity = getActivity();
    }

    @Test
    public void testStartActivity() throws InterruptedException {

        Thread.sleep(5000);
    }
}
